c-----------------------------------------------------------------------
c     subprogram 6. poly_nodes_fg.
c     finds the n+1 location of nodes for 1D lagrange polynomials in the
c     domain 0<=x<=1.  
c
c     the distribution of nodes is set according to the module variable
c     node_dist.  The value 'uniform' gives uniform nodes, and 'gll'
c     gives a nonuniform distribution.  For the latter,
c     the location of nodes corresponds to the zeros of the 
c     Gauss-Lobatto polynomials, (1+x)(1-x) * d L_n(x)/dx, where
c     L_n is the n-th order Legendre polynomial.  [See "Spectral/hp
c     Element Methods for CFD," Karniadakis and Sherwin, for example.]
c-----------------------------------------------------------------------
      SUBROUTINE poly_nodes_fg(n,x)
      USE fg_local
      IMPLICIT NONE

      INTEGER(i4), INTENT(IN) :: n
      REAL(r8), DIMENSION(0:n), INTENT(OUT) :: x
      CHARACTER(7) :: node_dist='uniform'
c     CHARACTER(7) :: node_dist='gll'

      INTEGER(i4) :: i,j,it
      INTEGER(i4) :: itm=100
      REAL(r8) :: xtmp,x0,x1,f0,f1,dx

      REAL(r8), PARAMETER :: delta=1.e-6,tol=1.e-14

      INTEGER(i4), SAVE :: n_last=-1
      CHARACTER(7), SAVE :: dist_last='none'
      REAL(r8), DIMENSION(0:50), SAVE :: x_last

      REAL(r8), EXTERNAL :: legendre_poly_fg
c-----------------------------------------------------------------------
c     don't repeat computation if the nodes haven't changed.
c-----------------------------------------------------------------------
      IF (n==n_last.AND.node_dist==dist_last) THEN
        x=x_last(0:n)
      ELSE
        n_last=n
        dist_last=node_dist
c-----------------------------------------------------------------------
c       uniform distribution.
c-----------------------------------------------------------------------
        IF (node_dist=='uniform') THEN
          DO i=0,n
            x(i)=REAL(i,r8)/REAL(n,r8)
          ENDDO
        ELSE
c-----------------------------------------------------------------------
c       find node positions through Newton's method with polynomial
c       deflation.
c
c       work in the domain -1<=x<=1 for the Legendre polynomials, then
c       rescale the results afterwards.
c
c       we use the fact that (1+x)(1-x) * d L_n/dx = n*(x*L_n-L_n-1)
c-----------------------------------------------------------------------
          x(0)=-1._r8
          DO i=1,n/2
            xtmp=x(i-1)+100._r8*delta
            it=1
            DO
              x0=xtmp-delta
              x1=xtmp+delta
              f0=REAL(n,r8)*
     $           (x0*legendre_poly_fg(x0,n)-legendre_poly_fg(x0,n-1_i4))
              f1=REAL(n,r8)*
     $           (x1*legendre_poly_fg(x1,n)-legendre_poly_fg(x1,n-1_i4))
              DO j=0,i-1
                f0=f0/(x0-x(j))
                f1=f1/(x1-x(j))
              ENDDO
              dx=(f1+f0)*delta/(f0-f1)*MIN(1._r8,1.5_r8**(it-5_i4))
              dx=MAX(MIN(1._r8/REAL(n,r8),dx),-1._r8/REAL(n,r8))
              xtmp=xtmp+dx
              IF (ABS(dx)<=tol) THEN
                x(i)=xtmp
                EXIT
              ENDIF
              it=it+1
              IF (it==itm)CALL fg_stop("Nodes_gll: not finding nodes.")
            ENDDO
          ENDDO
          DO i=n/2+1,n
            x(i)=-x(n-i)
          ENDDO
          x=0.5_r8*x+0.5_r8
        ENDIF
        DO i=1,n
          IF (x(i)<=x(i-1))CALL fg_stop("Nodes_gll: node ordering.")
        ENDDO
        x_last(0:n)=x
      ENDIF

      END SUBROUTINE poly_nodes_fg
c-----------------------------------------------------------------------
c     subprogram 7. legendre_poly.
c     compute the value of a legendre polynomial of non-negative
c     integer order at a specified position.
c-----------------------------------------------------------------------
      FUNCTION legendre_poly_fg(x,n) RESULT(ln)
      USE fg_local
      IMPLICIT NONE

      REAL(r8) :: ln
      REAL(r8), INTENT(IN) :: x
      INTEGER(i4), INTENT(IN) :: n

      REAL(r8) :: lim1,lim2
      INTEGER(i4) :: i
c-----------------------------------------------------------------------
c     use standard recursion to generate the value of the desired
c     legendre polynomial.  [Schaum's outline, Mathematical Handbook,
c     by M. Spiegel, for example.]
c-----------------------------------------------------------------------
      IF (n==0) THEN
        ln=1._r8
        RETURN
      ELSE IF (n==1) THEN
        ln=x
        RETURN
      ELSE
        lim2=1._r8
        lim1=x
        DO i=2,n
          ln=(REAL(2*i-1,r8)*x*lim1-REAL(i-1,r8)*lim2)/REAL(i,r8)
          lim2=lim1
          lim1=ln
        ENDDO
      ENDIF
        
      END FUNCTION legendre_poly_fg
c-----------------------------------------------------------------------
c     subprogram 9. polint
c     Perform polynomial interpolation given various segments.
c-----------------------------------------------------------------------
      SUBROUTINE polint_fg(xa,ya,n,x,y,dy)
      USE fg_local
      INTEGER(i4), INTENT(IN) :: n
      REAL(r8), INTENT(IN) :: x
      REAL(r8), INTENT(OUT) :: dy,y
      REAL(r8), DIMENSION(n), INTENT(IN) :: xa,ya
      INTEGER(i4), PARAMETER :: NMAX=20
      INTEGER(i4) i,m,ns
      REAL(r8) ::  den,dif,dift,ho,hp,w
      REAL(r8), DIMENSION(:), ALLOCATABLE ::  c,d

      ALLOCATE(c(n),d(n))
      c=ya; d=ya
      ns=1
      dif=abs(x-xa(1))
      DO i=1,n
        dift=abs(x-xa(i))
        IF (dift.lt.dif) THEN
          ns=i
          dif=dift
        ENDIF
      ENDDO
      y=ya(ns)
      ns=ns-1
      DO m=1,n-1
        DO i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)
          den=ho-hp
          if(den.eq.0.) CALL fg_stop('failure in polint')
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
        ENDDO
        IF (2*ns.lt.n-m)THEN
          dy=c(ns+1)
        ELSE
          dy=d(ns)
          ns=ns-1
        ENDIF
        y=y+dy
      ENDDO
      DEALLOCATE(c,d)
      RETURN
      END SUBROUTINE polint_fg


