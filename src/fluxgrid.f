c-----------------------------------------------------------------------
c     program fluxgrid.
c     generates flux grids for NIMROD.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. fluxgrid.
c-----------------------------------------------------------------------
c     subprogram 1. fluxgrid.
c     initializes i/o and controls PROGRAM flow.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      PROGRAM fluxgrid
      USE fg_local
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      USE fgAnalyzeTypes
!      USE analyze
      IMPLICIT NONE
      INTEGER :: nargs, inlen
      CHARACTER(64) :: input_file,msg
      CHARACTER(128) :: filename
      LOGICAL :: file_stat
      TYPE(fileData) :: fileIn
      TYPE(controlMap) :: gIn
      TYPE(controlAux) :: ts
      TYPE(fgControlIO) :: io
      TYPE(inputEq) :: eqIn
      TYPE(mappedEq) :: mEq
      TYPE(singularSurfaces) :: sS
      TYPE(stabilityType) :: sT
      TYPE(globalEqProfiles) :: gEP
      TYPE(globalEqDiagnose) :: gEV
      TYPE(TransportGeometry) :: TG
      TYPE(neoclassical) :: neo
c-----------------------------------------------------------------------
c     Check to see if there is an argument
c-----------------------------------------------------------------------
      CALL get_arg_count(nargs)
      IF (nargs > 1) CALL fg_stop('Usage: fluxgrid <input file>')

      IF (nargs > 0) THEN
        CALL get_arg(1,input_file)
        INQUIRE(FILE=TRIM(input_file),EXIST=file_stat)
        IF (.NOT. file_stat) THEN
          msg = 'File does not exist: '//TRIM(input_file) 
          CALL fg_stop(msg)
        ENDIF
      ELSE
        input_file='fluxgrid.in'
      ENDIF
c-----------------------------------------------------------------------
c     If there is an input file given, then pull of the label
c-----------------------------------------------------------------------
      IF (nargs > 0) THEN
         inlen=LEN(TRIM(input_file))
         fileIn%runlabel=input_file(3:inlen-3)
      ELSE
         fileIn%runlabel=" "
      ENDIF
c-----------------------------------------------------------------------
c     start timer, read input, and open output file.
c-----------------------------------------------------------------------
      CALL time_stat(0_i4,2_i4)
      CALL fgSetInputType(input_file,gIn,ts,io,fileIn)
c-----------------------------------------------------------------------
      WRITE(6,*) 'Processing Equilibrium'
c-----------------------------------------------------------------------
      CALL read_eq(fileIn,ts,io,eqIn)
      IF(fileIn%eqType=="direct") THEN
         CALL mapDirectEq(gIn,io,eqIn,mEq)
      ELSEIF(fileIn%eqType=="inverse") THEN
         CALL mapInverseEq(gIn,io,eqIn,mEq)
      ELSE
        CALL fg_stop("Problem with eqType")
      ENDIF
      ! Now that we have mEq, we have everything we need
      CALL eqInputDealloc(eqIn)

c-----------------------------------------------------------------------
      WRITE(6,*) 'Analyze equilibrium'
c-----------------------------------------------------------------------
      CALL get_global(gIn,mEq,gEV,gEP)
      IF(io%stability) CALL get_stability(mEq,sT)
      CALL get_rationalSurface(gIn,mEq,sS)
      CALL get_transport(gEV,gEP,mEq,TG)
      IF(io%out_neoclassical) CALL get_neoclassical(TG,gEP,mEq,ts,neo)
cTMP      CALL get_thetasf(gIn,mEq,thetasf)

c-----------------------------------------------------------------------
      WRITE(6,*) 'Writing output.'
c-----------------------------------------------------------------------
      CALL write_out(io,fileIn,gIn,mEq,gEV,gEP,sS)
      IF(io%stability) CALL write_stability(sT,mEq)
      IF(io%out_nimrod)CALL write_nimrod(gIn,mEq,gEP)
      IF(io%out_toq)   CALL write_toq(mEq)
      IF(io%out_neoclassical) 
     &  CALL write_neoclassical(fileIn%runlabel,TG,gEP,mEq,neo)
      IF(io%out_facets) 
     &   CALL write_facets(fileIn%runlabel,io,ts,fileIn,mEq,gEV,gEP,TG)
      IF(io%out_gyro) 
     &   CALL write_gyro(fileIn%runlabel,io,ts,fileIn,mEq,gEV,gEP,TG)
!TMP      IF(io%out_pest)    CALL write_pest(binary_unit)
!TMP      IF(io%out_far)     CALL write_far(binary_unit)
      IF(io%out_tecplot) CALL write_dat(io,gIn,mEq,gEV,gEP,sS,sT)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      CALL time_stat(1_i4,2_i4)
      CALL fg_stop('Normal termination')
      END PROGRAM fluxgrid

c--------------------------------------------------------------------
c     subprogram time_stat.f
c     handles machine-dependent timing statistics.
c--------------------------------------------------------------------
c--------------------------------------------------------------------
c     delcarations.
c--------------------------------------------------------------------
      SUBROUTINE time_stat(mode,unit)         !#SIDL_SUB
      USE fg_local
      IMPLICIT NONE
      
      INTEGER(i4), INTENT(IN) :: mode
      INTEGER(i4), INTENT(IN) :: unit

      REAL(r8) :: time,time1
      REAL(r8), SAVE :: time0
c--------------------------------------------------------------------
c     get initial time.
c--------------------------------------------------------------------
      IF(mode == 0)THEN
         CALL timer(time0)
c--------------------------------------------------------------------
c     get final time.
c--------------------------------------------------------------------
      ELSE
         CALL timer(time1)
c--------------------------------------------------------------------
c     write elapsed time.
c     I don't like the writing to arbitrary unit since I want 
c       everything closed by the time this is called
c--------------------------------------------------------------------
         time=time1-time0
cSEK         WRITE(unit,'(1X,A,1P,E11.3)')"total cpu time = ",time
         WRITE(6,'(1X,A,1P,E11.3,A)')"total cpu time = ",
     &            time,CHAR(7)
      ENDIF
c--------------------------------------------------------------------
c     terminate routine.
c--------------------------------------------------------------------
      RETURN
      END SUBROUTINE time_stat

