      MODULE fgMapEqTypes
c-----------------------------------------------------------------------
c     The dominant information about the Grad-Shafranov equilibrium is
c     stored in 3 structures calculated in process_eq in inverse.f and
c     direct.f (note MKS units are used).  Note that auxilary
c     information characterizing the equilibrium is in analyze.f
c
c     sq                             nsq Surface quantities
c       sq%xs = Psi_normal
c       sq%fs(:,1) = R B_T (toroidal flux function)
c       sq%fs(:,2) = Pressure * mu0 (No flow)
c       sq%fs(:,3) = q (Safety factor)
c       sq%fs(:,4) = Mach number (Normalized toroidal flow)
c       sq%fs(:,5) = number density
c       sq%fs(:,6) = electron pressure * mu0
c       sq%fs(:,7) = zave
c       sq%fs(:,8) = zeff
c
c     twod                           Jacobian and metric elements
c	  twod%fs(1,:,:) = R(theta,rho)
c	  twod%fs(2,:,:) = Z(theta,rho)
c	  twod%fs(3,:,:) = jac(theta,rho)
c	  twod%fs(4,:,:) = gpsipsi(theta,rho)
c	  twod%fs(5,:,:) = gpsitheta(theta,rho)
c	  twod%fs(6,:,:) = gthetatheta(theta,rho)
c
c     dir  
c       field information calculated directly from what is in the
c       direct-type file.  Chosen by calc_direct but is default
c       structure used nimset.  NOTE: includes rblocks in vacuum region
c
c        dir%xs=twod%xs
c        dir%ys=rho
c        dir%fs(1,:,:) = R
c        dir%fs(2,:,:) = Z
c        dir%fs(3,:,:) = B_R
c        dir%fs(4,:,:) = B_Z
c        dir%fs(5,:,:) = B_tor
c        dir%fs(6,:,:) = J_R
c        dir%fs(7,:,:) = J_Z
c        dir%fs(8,:,:) = J_tor
c        dir%fs(9,:,:) = V_tor
c        dir%fs(10,:,:) = Pressure
c        dir%fs(11,:,:) = electron pressure
c        dir%fs(12,:,:) = n_density
c        dir%fs(13,:,:) = psi
c        dir%fs(14,:,:) = radial_coord
c-----------------------------------------------------------------------
c     Other useful information for processing:
c-----------------------------------------------------------------------
c     Type of equilibria: either inverse or direct: eqtype
c
c     Characterization of the plasma size:
c      rs1,rs2 -> locations of separatrix at z=zo
c      zst,rst -> Maximum Z (top) and corresponding R
c      zsb,rsb -> Minumum Z (bottom) and corresponding R
c      rsn,zsn -> Minumum R and corresponding Z
c      rsx,zsx -> Maximum R and corresponding Z
c
c     Boundary information calculated in process_eq in inverse.f
c       and direct.f:
c       ob - R,Z points of outer boundary of rblock region (that is
c            inside separatrix (determined by psihigh).
c       rzsep - R,Z points of separatrix
c       
c-----------------------------------------------------------------------
      USE fg_local
      USE fg_spline
      USE fg_bicube
      IMPLICIT NONE

      TYPE :: mappedEq             !#SIDL_ST
        INTEGER(i4) :: mpsi        !< # of radial points
        INTEGER(i4) :: mvac        !< # of radial points beyond psihigh
        INTEGER(i4) :: mtheta      !< # of poloidal points
        INTEGER(i4) :: direct_flag !< Whether the direct structure is used
        REAL(r8) :: zz             !< Average Z (for COM velocity) Not zeff!
        REAL(r8) :: psio           !< Toloidal flux
        REAL(r8) :: ro             !< R location of magnetic axis
        REAL(r8) :: zo             !< Z location of magnetic axis
        REAL(r8) :: rs1            !< R-inboard midplane
        REAL(r8) :: rs2            !< R-outboard midplane
        REAL(r8) :: zst            !< Z @ top
        REAL(r8) :: rst            !< R @ top
        REAL(r8) :: zsb            !< Z @ bottom
        REAL(r8) :: rsb            !< R @ top
        REAL(r8) :: rsn            !< R @ minimum R
        REAL(r8) :: zsn            !< Z @ minimum R
        REAL(r8) :: rsx            !< R @ maximum R
        REAL(r8) :: zsx            !< Z @ maximum R
        INTEGER(i4) :: nsq=8       !< # in sq type
        INTEGER(i4) :: ntwod=6     !< # in twod type
        INTEGER(i4) :: ndir=14     !< # in dir type 
        TYPE(spline_type) :: sq    !< Surface quantities
        TYPE(bicube_type) :: twod  !< R(psi,theta),Z(psi,theta)+other
        TYPE(bicube_type) :: dir   !< Direct data structure
        TYPE(spline_type) :: ob    !< R,Z points @ psihigh
        TYPE(spline_type) :: rzsep !< R,Z points on separatrix
      END TYPE


      END MODULE fgMapEqTypes
c-----------------------------------------------------------------------
c     subprogram 1. fgMapEqTypes.
c     deallocate the data
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE fgMapEqTypes_dealloc(mEq)
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      IF (ASSOCIATED(mEq%sq%fs))       CALL spline_dealloc(mEq%sq)
      IF (ASSOCIATED(mEq%ob%fs))       CALL spline_dealloc(mEq%ob)
      IF (ASSOCIATED(mEq%rzsep%fs))    CALL spline_dealloc(mEq%rzsep)
      IF (ASSOCIATED(mEq%twod%fs))     CALL bicube_dealloc(mEq%twod)
      IF (ASSOCIATED(mEq%dir%fs))      CALL bicube_dealloc(mEq%dir)

      END SUBROUTINE fgMapEqTypes_dealloc
c-----------------------------------------------------------------------
