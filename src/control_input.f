c-----------------------------------------------------------------------
c     subprogram input
c     handles input.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     subprogram 0. input
c     subprogram 1. sol_input
c     subprogram 2. read_fg_input
c     module declarations.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     fluxgrid input variable declarations.
c-----------------------------------------------------------------------
      MODULE fg_input
      USE fg_local
      IMPLICIT NONE


c-----------------------------------------------------------------------
c    filename gives the path of the data file specifying the
c    equilibrium.  The file may be ascii or binary.  It may be the
c    output of a direct Grad-Shafranov solver, which specifies poloidal
c    flux, R*B_phi, pressure, and safety factor on a 1D grid of flux
c    surfaces, and the poloidal flux psi on a 2D grid in R and Z; or of
c    an inverse solve, which replaces psi(R,Z) with values of R and Z on
c    a 2D grid of psi and theta on flux surfaces.
c
c    aux_file can include things that has additional data such as
c    profiles.  To date, only the "p" file format from Snyder and
c    Osborne is supported.
       
      CHARACTER(128) :: filename="82205-1.25X"
      CHARACTER(128) :: aux_file=""

c-----------------------------------------------------------------------
c     eq_type specifies the type of the input file.  It allows fluxgrid
c     to determine whether the file is ascii or binary, whether it is
c     direct or inverse, and how to interpret the data.  Here is a list
c     of allowed types:
c   
c     Name          Description               Direct/Inverse  Ascii/Binary
c     --------------------------------------------------------------------
c     "miller"      Bob Miller's TOQ code       inverse        binary (r4 data)
c     "miller8"     Bob Miller's TOQ code       inverse        binary (r8 data)
c     "galkin"      Sergei Galkin's code        inverse        binary
c     "chease"      Lausanne CHEASE code        inverse        ascii
c     "chum"        Modified Miller code        inverse        binary
c     "efit"        GA EFIT code                direct         ascii
c     "rsteq"       ORNL RSTEQ code             direct         binary
c-PRE     "IMT"         ITER Model Tokamak data     inverse        ascii
c     "soloviev"    Soloviev analytical model   direct         Calculate
c     "esc"         Run the esc code            inverse        Calculate


      CHARACTER(8) :: eq_type="efit"
      CHARACTER(20) :: aux_type=""  ! pfile, plasma_state

c-----------------------------------------------------------------------
c     IMT format are UFiles (an ASCII format developed by Doug McCune)
c     plus a directory structure.  The following specifies where the
c     data is located.  This is used instead of filename.
c-----------------------------------------------------------------------
      CHARACTER(100) :: IMTroot
      CHARACTER(100) :: IMTmachine
      CHARACTER(100) :: IMTshot

c-----------------------------------------------------------------------
c     The indices of the grid are 0:mpsi and 0:mtheta and correspond to
c     mx and my in the nimset/nimrod input file except when extending
c     the grid into the vacuum region.  See extr and mvac below.  mpsi
c     is the number of radial cells between the magnetic axis and the
c     last azimuthal grid line (before the separatrix).  mtheta is the
c     number of azimuthal cells.
c
c     Don't touch mxpie unless you really know what you are doing.

      INTEGER(i4) :: mpsi=64   
      INTEGER(i4) :: mtheta=64

c-----------------------------------------------------------------------
c     The grid in the grad_psi direction is controlled by grid_method
c     and pack_method.  
c
c     grid_method controls what to use as the "radial" coordinate in 
c     a "global" type of sense.
c      grid_method="uniform", distributes radial grid points uniformly 
c       in rho
c      grid_method="sqrt", distributes radial grid points uniformly 
c       in (rho)^1/2  This puts more points near the center as compared
c       to "uniform" method.
c      grid_method="stretch" (suggested by Don Pearlstein) distributes points
c       at both the axis and the edge as compared to the "uniform" method.
c      grid_method="power", distributes radial grid points uniformly 
c       in (rho)^radial_power such that:
c           radial_power=1.0 is equivalent to "uniform"
c           radial_power=0.5 is equivalent to "sqrt"
c           radial_power>1 pulls points away from the axis as compared to
c            the uniform method.
c
c     radial_variable controls how to choose rho as described above
c      radial_variable="psi" sets rho=psi_normal which is the most common
c       choice of rho.
c      radial_variable="bigr" sets rho=(R(ray)-R_o)/Delta_R where R(ray)
c       is the major radius from the magnetic axis, R_o, to the
c       separatrix and normalized such that rho goes from 0-1.  This
c       option generally gives better finite element grids.
c
c     pack_method is used to control how to pack around rational
c     surfaces which is most useful for interchange and tearing modes.
c     The "gauss" choice uses a Gaussian distribution and is described
c     in greater detail below
c      
      CHARACTER(8) :: grid_method="uniform"   
                                          ! "original", "uniform",
                                          ! "stretch", "sqrt",
                                          ! "power"
      CHARACTER(8) :: pack_method="none"  ! "none","gauss"
      REAL(r8) :: radial_power=1
      CHARACTER(8) :: radial_variable="bigr"  ! "bigr","psi"

c-----------------------------------------------------------------------
c     When packing around the rational surfaces, one has to choose which
c      rational surfaces to pack around.  Here one chooses the toroidal
c      mode number to pack around, and then gives a range of q values
c      to pack around.  For example, two pack around the q=2 and q=3/2
c      surfaces, one can choose nn=2 and qsmin = 1.49 and qsmax=2.01
c      (the q=2 surface corresponds to m=4/n=2).
      INTEGER(i4) :: nn=1       
      REAL(r8) :: qsmin=-HUGE(0) 
      REAL(r8) :: qsmax=HUGE(0) 

c-----------------------------------------------------------------------
c     For pack_method="gauss", the packing is Gaussian (i.e.,
c       a plot of dr vs. i shows a Gaussian) with the amplitude and width
c       of the Guassian by amp and wpack.  The amplitude is roughly
c       dr0/dr where dr0 is the value far away from the Gaussian and wpack
c       is the full-width given as a percentage of a = xmax-xmin.
c       For example, if mpsi=164 and you want to pack an extra 100 points
c       around the q=1 rational surface within a width of 0.1*a then:
c               nn=1, qsmin = 0.99, qsmax=1.01
c               wpack=0.1
c               amp=15.625 
c        where amp = dr0/dr = 1/(164-100) / (0.1*1/100) = 15.625

      REAL(r8) :: wpack=0._r8
      REAL(r8) :: amp=0._r8

c-----------------------------------------------------------------------
c     The grid in the theta direction is chosen by angle_method.
c
c     angle_method="jac"  specifies the theta angle by  choosing the
c     Jacobian: Jacobian = R^ipr/|B_P|^ipb .  For a discussion of the
c     types of grid chosen by this method, see the paper:
c     Grimm, Dewar, Manickam JCP 49 (1983) 94
c
c     angle_method="geom" creates the theta contours by forming straight
c     rays coming out of the magnetic axis.  Unlike the delta-W codes,
c     this is probably a good choice for NIMROD.  The default is "jac"
c     to make it backword compatible with previous versions
c
c     angle_pack chooses how to distribute the theta mesh:
c       .FALSE. => theta is equally spaced (dtheta=constant) 
c                 This is the default, and is no packing
c       .TRUE. => the theta is distributed such that on the last
c               closed flux surface, the distribution of points would
c               match that of choosing the Jacobian method above only
c               using indices jpb,jpr instead of ipb,ipr
     
      CHARACTER(8) :: angle_method="geom"   
      LOGICAL :: angle_pack=.FALSE.
      INTEGER(i4) :: ipb=1
      INTEGER(i4) :: ipr=0
      REAL(r8) :: jpb=1
      REAL(r8) :: jpr=0
      REAL(r8) :: jpz=0


c-----------------------------------------------------------------------
c     Controlling the structured mesh region:
c     The input parameter psilow is used to cut off stuff near the axis
c     which can be useful for when the equilibrium codes have problems 
c     near the axis.  A good value is 1e-4.  
c     
c     The input parameter psihigh determines how close to the edge the
c     ODE's are integrated.  It must be <= 1.  For an inverse
c     equilibrium, which never has a separatrix in the computational
c     domain, it may be set to 1.  For a direct equilibrium with a
c     separatrix, psihigh determines how close the code approaches the
c     separatrix. For some EFIT files with large currents near the edge,
c     this can be useful but difficult to use correctly since nimrod is
c     sensitive to currents at the edge.  Get some advice on those cases
c
c     extr_type and extr allows one to extend the rblock region beyond
c     that specified by psihigh.  Note that only nimrod is affected by
c     this option.  All of the other output (graphs and interfaces to
c     other codes) is unaffected by this option.
c
c     The specific method distance between the outer plasma boundary and
c     the rblock boundary is specified by extr_type:
c     1) extr_type='conformal'
c           The distance between the wall point and the outer boundary
c           defined by psihigh is constant as determined by extr.
c     2) extr_type='self_similar'
c           The distance extr=b/a is held constant where b is the
c           distance from the wall point to the origin and a is 
c           the distance from the edge to the origin.  The wall
c           point, the boundary point, and the origin are all 
c           colinear.  The center is chosen using extr_center,
c           and can either be 'geom' for the geometric center (the
c           conventional GATO choice) or 'mag_axis' for the
c           magnetic axis (the conventional MARS choice).
c     3) extr_type='wall'
c           The distance is based on a Fourier representation of the
c           wall, which is generally specified by a file.  Note that
c           you can't in general use the limiter information in an
c           EQDSK file since that most likely will not have a good
c           Fourier representation.  The number of modes used in the
c           Fourier representation is specified by "wall_modes"
c
c           The file containing the R,Z points is specified by "wall_file"
c           and has the same format as the "wall.asc" (which is
c           automatically generated if "efit" files contain limiter
c           data) file: 1st line is the the number of points, and 
c           each subsequent line are the R,Z points.
c           For DIII-D or ITER geometry, the data points are hard-coded
c           and can by used by setting wall_file to "use_d3d" or
c           "use_iter".
c
c     mvac is the number of points between the outer boundary (as
c     defined by psihigh) and the boundary described by extr.  This
c     normally corresponds to the vacuum region (psihigh ~ 1), but with
c     psihigh < 1, it will include the "plasma" region as well.
c     An alternative specification to mvac, is to specify vac_frac_fg
c      in which case, the value of mvac is overwritten with the value:
c                             mvac=ANINT(vac_frac_fg*mpsi)
c     For typical DIII-D, cases, a vac_frac_fg~0.15-0.25 is typical.
c     NOTE: For finite element grids with higher-order elements, mvac is
c     replaced with mvac*polynomial_degree.  See fe_grid below.
c
      REAL(r8) :: psilow=.01        ! minimum psi value, > 0
      REAL(r8) :: psihigh=.95       ! maximum psi value, < 1
      INTEGER(i4) :: mvac=0         !  # of radial points in "vacuum"
      REAL(r8) :: vac_frac_fg=0.    ! % of points to allocate to "vacuum"

      REAL(r8) :: extr=0.           ! extend rblock region
      CHARACTER(12) :: extr_type='none'         ! Wall boundary type
      CHARACTER(12) :: extr_center='geom'       ! Used with self_similar

      CHARACTER(128) :: wall_file='use_d3d'     ! use_d3d has hard vals
      INTEGER(i4) :: wall_modes=10              ! # of modes for wall file
      LOGICAL :: vac_pack=.FALSE.               ! Pack around vacuum interface

c-----------------------------------------------------------------------
c     CAUTION: THE FOLLOWING OPTIONS ARE NOT GUARANTEED TO WORK:
c     Currently, the capability for triangular regions is only contained
c     when reading EFIT (eqdsk) files.  In this case, we have the
c     following choices:
c           psihigh > 0, triangle=.F.      => pure rblock
c           psihigh > 0, triangle=.T.      => rblock and tblock regions
c           psihigh = 0, triangle=.T.      => pure tblock 
c           psihigh = 0, triangle=.F.      => exits.
c
c     The output files are nimdsk (a generalized eqdsk file) and poly
c     files.  The poly files are normally meant to be used with the
c     TRIANGLE code to produce the grid.  Then the grid and the
c     equilibrium contained in the nimdsk file are combined in nimset to
c     produce the dump file for nimrod.
c
c     wall_length is a minimum wall length used in constucting the
c       poly files associated with the outer boundary.  When negative,
c       an internal scheme finds a minimun length by inspecting the
c       rblock boundary and using twice this minimum length,
c       otherwise wall_length is this minimum value.

      LOGICAL :: triangle=.FALSE.
      REAL(r8) :: wall_length=-1.0


c-----------------------------------------------------------------------
c     For newq0 = 0, the code uses the original q profile specified in
c     the equilibrium file.  For newq0 /= 0, it readjusts the q profile
c     to give the specified value of q at the axis, in such a way as to
c     leave the Grad-Shafranov solution invariant.  This can be used to
c     explore the stability of a range of equilibria for each
c     equilibrium file.

      REAL(r8) :: newq0=0    ! desired q on axis; 0 => keep original

c-----------------------------------------------------------------------
c     tol0 gives the tolerance for mapping direct equilibria
c     The smaller the number the longer fluxgrid will take.  
c     10^-6 is OK, if there isn't any radial or poloidal packing
c     10^-8 is a safer choice however.
c
c     interp determines whether cubic spline interpolation is done for
c     some of the files, more detail at the expense of slower operation
c     

      REAL(r8) :: tol0=1.e-8    ! tolerance for mapping direct equilibria
      LOGICAL :: interp=.FALSE.

c-----------------------------------------------------------------------
c     For direct equilibria codes, it is possible to evaluate the 
c     magnetic fields directly from the input without going through
c     the entire mapping.  This is useful for debugging cases where
c     one is not sure if the crappy equilibrium your feeding into NIMROD
c     is fluxgrid's fault or the equilibrium code's fault.
c     
      LOGICAL :: calc_direct=.FALSE.

c-----------------------------------------------------------------------
c     For the toroidal current, there are two ways of calculating it:
c     "delstar"  -- direct evaluation of J_t = delstar(psi)
c     "use_gs"   -- use the Grad-Shafranov equation: J_t = -R^2 mu0 p'-FF' 
c     use_gs is recommended because there is less numerical error 
c       associated with the evaluation of the derivatives.  However,
c       if you don't think you are reading in a good equilibrium, go for
c       the more direct evaluation. 

      CHARACTER(8) :: j_t="use_gs"      

c-----------------------------------------------------------------------
c     If compiled with a Grad-Shafranov library, then we have the option
c     to refine or recalculate the equilibrium.  Currently this is only
c     enabled for the ESC code.

      CHARACTER(8) :: refine_type="none"

c-----------------------------------------------------------------------
c     To calculate the magnetic field in the R,phi,Z coordinates of
c     nimrod, one requires the Jacobian for the transformation of the
c     flux coordinates to R,Z coordinates.  Fluxgrid has 2 coordinate
c     systems: (rho,eta) and (R,Z).  (rho, eta) tends to work better
c     near the magnetic axis, while sometimes R,Z works better for the
c     edge in highly shaped plasmas.  The 'rjac' parameter gives the
c     "radius" (in terms of r=SQRT(psi_normal) at which to switch
c     between using rho,eta to calculate the grid, and R,Z coordinate
c     system to calculate the grid.  Basically, if you are having
c     problems with the numerical noise near the edge, switch this puppy
c     to one and see if that helps.

      REAL(r8) :: rjac=0.5


c-----------------------------------------------------------------------
c     NOTE: The following *_fg parameters are only used in the
c     standalone version of fluxgrid.  When using nimset, these values
c     are taken from the nimrod.in file.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     Whether to recalculate pressure or not.
c     PRE: Work for generalizing all of this is in progress
c     
      LOGICAL :: recalculate_pressure=.false.

c-----------------------------------------------------------------------
c     zave is the ratio of the ion charge to the electron charge.
c     Because the extended magnetohydrodynamic model used by NIMROD is a
c     TWO-fluid model, zave is a constant and does not have a spatial
c     dependence.  
c     PRE: Work for generalizing all of this is in progress
c     
      REAL(r8) :: zave=1
      CHARACTER(22) :: z_file="z_profile"
      CHARACTER(22) :: zeff_file="zeff_profile"

c-----------------------------------------------------------------------
c     The following parameters are not used by fluxgrid in any
c     significant way since MHD equilibria (although with flow, density
c     is technically used).
c
c     Since most equilibrium codes do not specify density, the ability
c     to specify density as a function of rho (=SQRT(psi_normal)) is 
c     introduced.  For "n_profile"=
c         constant:     n(rho)=ndens
c         power:  n(rho)=(ndens-nedge)*(1-rho**npp(1))**npp(2)+nedge
c         poly:   n(rho)=ndens+SUM(npp(j)*rho**(j), [j,1,10]) 
c         eqfile: Use n from the equilibrium data file if available. 
c           Currently this is only available from some efit files.
c           See read_*.f files.  This option is changing rapidly!
c

      REAL(r8) :: ndens_fg=1.e20                       ! density in core
      REAL(r8) :: nedge_fg=1.e20                       ! density at edge
      CHARACTER(8) :: n_profile_fg="constant"
      REAL(r8), DIMENSION(10) :: npp_fg=0.             ! n profile parameters
      CHARACTER(22) :: density_file="density_profile"

c-----------------------------------------------------------------------
c     Parameters for the steady-state electron pressure: Like density,
c     the electron pressure is not normally considered as part of the
c     steady-state equations; therefore, one is often free to choose the
c     steady-state profile for the electron pressure.  Currently, only
c     two options are given for specifying the steady-state profile.
c     For "pe_profile"=
c      pe_frac: pe_0 = pe_frac*p_0 (total steady-state pressure)
c      eqfile:  Use profiles coming from equilibrium file if available.  
c     
c     Note that for NIMROD simulations, these parameters are closely
c     related to "separate_pe" and  "tequil_rate" which
c     control the dynamics of the plasma evolution.  See nimset/input.f
c     for the information on the correct use.

      REAL(r8) :: pe_frac_fg=0.5
      CHARACTER(8) :: pe_profile_fg="pe_frac"          ! power
      REAL(r8) :: te_cent_fg=10000                     ! in eV
      REAL(r8) :: te_edge_fg=50                        ! in eV
      REAL(r8), DIMENSION(10) :: tepp_fg=0.            ! T_e profile parameters


c-----------------------------------------------------------------------
c     For traditional MHD instabilities, the absolute value of the
c     pressure does not matter; however, when used to calculate a
c     temperature, which is needed to calculate the resistivity for
c     example, the absolute value matters.  Equilibria often has
c     pressure going to zero which causes problems.  This variable adds a
c     constant gauge pressure to the pressure to adjust things properly.
c     It is in units of Pascals.
c     
c     Alternatively, if one specifies a temperature_gauge, then a      
c     pressure gauge is calculated to make the given temperatures 
c     at the edge.

      REAL(r8) :: pressure_gauge=0.
      REAL(r8) :: ion_temperature_gauge=0.
      REAL(r8) :: electron_temperature_gauge=0.
      REAL(r8) :: ndensity_gauge=0.
      REAL(r8) :: psi_norm_gauge=0.


c-----------------------------------------------------------------------
c     It is useful to calculate the length and time scales associated
c     with various diffusivity parameters since fluxgrid has the 
c     machinery for it (see fluxgrid.txt).  Note that these values
c     aren't actually used to set anything.  In particular, the output
c     in fluxgrid.txt can help you to determine the "elecd" and
c     "kin_visc" parameters to use in nimrod.in, but you must manually
c     set them yourself.

      REAL(r8) :: sfac=1.e5      ! Lundquist number
      REAL(r8) :: pr=1000        ! Prandtl number

c-----------------------------------------------------------------------
c     The MHD equations can be (and frequently are) normalized by a
c     magnetic field and a length scale.  In toroidal equilibria, if one
c     takes the vacuum R*B_toroidal as a given constant, then everything
c     is normalized by a single choice of length scale.  Because
c     equilibrium codes sometimes write out the de-dimensionalized
c     values, it can be useful to add in the normalizations by hand.
c     One must read the appropriate read*.f code to see which
c     equilibrium codes are actually affected.

      REAL(r8) :: fnorm_input=1
      REAL(r8) :: rnorm_input=1
     
 
c-----------------------------------------------------------------------
c     Set edriftToX to .TRUE. if the electron magnetic drift is into 
c     X-point in a single null driveted geometry. This parameter is used 
c     for the computation of modified Sauter bootstrap current experssion

      LOGICAL :: edriftToX = .FALSE.

c-----------------------------------------------------------------------
c     A bunch of parameters here for controlling the output files.
c     See the README file here for more info.
c-----------------------------------------------------------------------
c     Output useful for debugging
c---------------------------------
      LOGICAL :: bin_1d=.FALSE.
      LOGICAL :: out_1d=.FALSE.
      LOGICAL :: bin_2d=.FALSE.
      LOGICAL :: out_2d=.FALSE.
      LOGICAL :: diagnose=.FALSE.
c---------------------------------
c     Output D_I, D_R, ...
c---------------------------------
      LOGICAL :: stability=.FALSE.
      LOGICAL :: out_neoclassical=.FALSE.
c---------------------------------
c     Interfaces to other codes
c     NOTE: Standalone version only
c---------------------------------
      LOGICAL :: out_toq=.FALSE.
      LOGICAL :: out_nimrod=.FALSE.
      LOGICAL :: out_pies=.FALSE.
      LOGICAL :: out_pest=.FALSE.
      LOGICAL :: out_far=.FALSE.
      LOGICAL :: out_nimplot=.FALSE.
      LOGICAL :: out_facets=.FALSE.
      LOGICAL :: out_gyro=.FALSE.
      INTEGER(i4) :: ncell_facets=32   
c---------------------------------
c     Interface to TecPlot graphics (simple format)
c     Includes several bells and whistles
c     NOTE: Standalone version only
c---------------------------------
      LOGICAL :: out_tecplot=.FALSE.

c-----------------------------------------------------------------------
c     For constructing grids for use in higher-order element codes such
c      such as NIMROD, things have to be done slightly differently
c      because the interior points need to be equispaced within a cell.
c     If pd_fg > 1, then we will set up such a grid
c     To ensure that there are integer number of verticies, 
c       mpsi, mtheta, and mvac are multiplied by pd_fg
c     Note that this only affects the standalone version of fluxgrid.
c     If using nimset, this is handled automatically.

      INTEGER(i4) :: pd_fg=1                 ! Polynomial degree
      INTEGER(i4) :: pd_mesh=1               ! Polynomial degree of mesh

c-----------------------------------------------------------------------
c     These are in here for historical reasons and have been superseded.
c-----------------------------------------------------------------------
c     Don't touch mxpie unless you really know what you are doing.
      INTEGER(i4) :: mxpie=1    ! radial grid position of pie-shaped tblock
c     INTEGER(i4) :: mx=128     ! adapted nodes

      INTEGER(i4) :: grid_type=3        ! 0:original; 1:uniform,
                                        ! 2:stretch, 3:sqrt, 4:pack
      CHARACTER(8) :: mon_type="none"
      REAL(r8) :: delta0
      REAL(r8) :: zeff_fg=1 ! Replaced by zave which is more accurate
c-----------------------------------------------------------------------
c     This is a programming convenience: use zz instead of zeff_fg
c-----------------------------------------------------------------------
      REAL(r8) :: zz

c-----------------------------------------------------------------------
c     namelist declarations. 
c-----------------------------------------------------------------------
      NAMELIST/global_input/filename,aux_file,eq_type,aux_type,
     $     mpsi,mtheta,mxpie,psilow,psihigh,
     $     newq0,tol0,j_t,rjac,interp,calc_direct,
     $     grid_method,radial_power,radial_variable,
     $     pack_method,wpack,amp,nn,qsmin,qsmax,
     $     angle_method,angle_pack,ipb,ipr,jpb,jpr,jpz,
     $     extr,mvac,vac_frac_fg,extr_type,extr_center,
     $     triangle,wall_length,vac_pack,wall_file,wall_modes,
     $     sfac,zeff_fg,fnorm_input,rnorm_input,zave,
     $     pe_profile_fg,pe_frac_fg,te_cent_fg,te_edge_fg,tepp_fg,
     $     n_profile_fg,ndens_fg,nedge_fg,npp_fg,density_file,
     $     z_file,zeff_file,
     $     pressure_gauge,ion_temperature_gauge,ndensity_gauge,
     $     electron_temperature_gauge,psi_norm_gauge,
     $     bin_1d,out_1d,bin_2d,out_2d,stability,out_toq,out_nimrod,
     $     out_tecplot,out_neoclassical,
     $     out_pest,out_far,out_pies,out_nimplot,
     $     out_facets,ncell_facets,out_gyro,
     $     pd_fg,pd_mesh,
     $     grid_type,mon_type,delta0,
     $     IMTroot,IMTmachine,IMTshot,
     $     edriftToX

c-----------------------------------------------------------------------
c     Namelist for running the equilibrium
c-----------------------------------------------------------------------
      ! Soloviev only
      INTEGER(i4) :: mr=65      ! number of radial grid zones
      INTEGER(i4) :: mz=65      ! number of axial grid zones
      REAL(r8) :: e=1.          ! elongation
      REAL(r8) :: a=1.          ! minor radius
      REAL(r8) :: r0=3.         ! major radius
      REAL(r8) :: z0=0.         ! Z location of major radius
      REAL(r8) :: qaxis=1.26    ! safety factor at the o-point
      REAL(r8) :: shift=1.26    ! safety factor at the o-point
      ! and ESC as well
      REAL(r8) :: triangularity=1.  ! triangularity
      REAL(r8) :: b_0=1.
      REAL(r8) :: rtor=1.
      REAL(r8) :: paxis=0.04_r8;
      LOGICAL :: out=.TRUE.     ! produce ascii output?
      LOGICAL :: bin=.TRUE.     ! produce binary output?
      INTEGER :: mpol  ! number of poloidal modes (typically 3-4, should be <=8)
      CHARACTER(50) :: RBlocation="axis"
      CHARACTER(50) :: file_label=""
      CHARACTER(50) :: time_label=""

      NAMELIST/eq_input/mr,mz,e,a,r0,z0,out,bin,triangularity,
     $                  qaxis,b_0,paxis,mpol,shift,
     $                  file_label,time_label

      CONTAINS
c-----------------------------------------------------------------------
c     subprogram 1. read_fg_input.
c     reads global input.
c-----------------------------------------------------------------------
      SUBROUTINE read_fg_input(infile)

      CHARACTER(*), INTENT(IN) :: infile
      CHARACTER(14) :: tempfile
      CHARACTER(14) :: ineqfile
      INTEGER, PARAMETER :: eq_unit = 16  ! equilibrium file
c-----------------------------------------------------------------------
c     Strip the comments from the input file and store into a 
c     temporary file which is more like the normal file
c-----------------------------------------------------------------------
      tempfile='tempfg.in'
      CALL rmcoment(infile,tempfile)
      OPEN(UNIT=eq_unit,FILE=tempfile,STATUS="UNKNOWN")
      READ(UNIT=eq_unit,NML=global_input)
      CLOSE(UNIT=eq_unit,STATUS="DELETE")
c-----------------------------------------------------------------------
c     Backward compatability conversions:
c-----------------------------------------------------------------------
      IF(TRIM(grid_method) == 'use_gt')THEN
         SELECT CASE(grid_type)
         CASE(0)
           grid_method = 'original'
         CASE(1)
           grid_method = 'uniform'
         CASE(2)
           grid_method = 'stretch'
         CASE(3)
           grid_method = 'sqrt'
         CASE(4)
           grid_method = 'sqrt'
         CASE default
           grid_method = 'sqrt'
         END SELECT
      ENDIF
      IF(TRIM(pack_method)=='lorentz'.OR.
     &   TRIM(pack_method)=='no_gap') THEN
       WRITE(*,*) "monitor type packing functions no longer used"
       WRITE(*,*) "Use pack_method=gauss"
      ENDIF
c-----------------------------------------------------------------------
c     Set mvac=0 if not going into vacuum.  Makes if statements easier
c-----------------------------------------------------------------------
      IF(vac_frac_fg /= 0.) mvac=ANINT(vac_frac_fg*mpsi)
      IF ((TRIM(extr_type) == 'none') .OR.
     &   ((extr_type=='self_similar' .OR. extr_type=='conformal') .AND.
     &      (mvac <= 0 .OR. extr <=1))) mvac=0
      IF(mvac == 0) extr=0.
c-----------------------------------------------------------------------
c     If n_profile=constant, make sure that it is indeed constant.
c-----------------------------------------------------------------------
      IF (TRIM(n_profile_fg) == 'constant') nedge_fg=ndens_fg
c-----------------------------------------------------------------------
c     If needed, we need to read in another namelist file
c-----------------------------------------------------------------------
      IF(eq_type=="soloviev" .OR. eq_type=="esc") THEN
        ineqfile='fluxgrid_eq.in'
        tempfile='tempfgeq.in'
        CALL rmcoment(ineqfile,tempfile)
        OPEN(UNIT=eq_unit,FILE=tempfile,STATUS="UNKNOWN")
        READ(UNIT=eq_unit,NML=eq_input)
        CLOSE(UNIT=eq_unit,STATUS="DELETE")
      ENDIF
c-----------------------------------------------------------------------
      END SUBROUTINE read_fg_input
c-----------------------------------------------------------------------
c     subprogram 2. rmcomment
c     routine strips out comments beginning with an exclamation point
c-----------------------------------------------------------------------
      SUBROUTINE rmcoment(fileold,filenew)

      IMPLICIT NONE
      CHARACTER(*), INTENT(IN) :: fileold,filenew
      CHARACTER(128) :: line
      INTEGER, PARAMETER :: nold=55,nnew=56
      INTEGER cmax, ios
      LOGICAL :: file_exist
c-----------------------------------------------------------------------
c     Open files, but make sure the old one exists first.
c-----------------------------------------------------------------------
      INQUIRE(FILE=TRIM(fileold),EXIST=file_exist)
      IF(.NOT. file_exist) THEN
         PRINT *,'The file "',TRIM(fileold),'" could not be found.'
         STOP
      ENDIF

      OPEN(UNIT=nold,FILE=TRIM(fileold),status='OLD')      
      OPEN(UNIT=nnew,FILE=filenew,status='REPLACE')

c-----------------------------------------------------------------------
c     Strip comments.     Note: line lengths limited to 127 characters
c-----------------------------------------------------------------------
      DO
        READ(UNIT=nold,FMT='(a)',IOSTAT=ios) line 
        IF (ios /= 0) EXIT
        cmax=1
        DO WHILE(line(cmax:cmax).NE.'!' .AND. cmax .LE. 127)
           cmax=cmax+1
        ENDDO
        IF(cmax .GT. 1) WRITE(nnew,'(a)') line(1:cmax-1)
      ENDDO

c-----------------------------------------------------------------------
c     Close files and exit
c-----------------------------------------------------------------------
      CLOSE(nold)
      CLOSE(nnew)

      RETURN
      END SUBROUTINE rmcoment
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      END MODULE fg_input
