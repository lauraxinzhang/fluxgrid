c-----------------------------------------------------------------------
c     file direct.f.
c     processes direct equilibria.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     0. direct.
c     1. process_eq.
c     2. process_structured.
c     2. get_bfield.
c     3. position.
c     4. flder.
c     5. refine.
c     6. calc_vacuum.
c     7. get_psi_rz_direct
c     8. separatrix.
c     9. sep_distance.
c     10. gen_sep_distance.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     subprogram 1. mapDirectEq.
c     gets equilibrium data and massages it.
c-----------------------------------------------------------------------
      SUBROUTINE mapDirectEq(gIn,io,eqIn,mEq) !#SIDL_SUB !#SIDL_SUB_NAME=map_direct_eq#
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      USE fg_local
      USE fg_spline
      USE fg_bicube
      USE grid
      IMPLICIT NONE

      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file

      INTEGER(i4) :: mpsi,mtheta
c-----------------------------------------------------------------------
c     Start off by copying over some of the original values
c-----------------------------------------------------------------------
      CALL copyMembers(gIn,eqIn,mEq)
c-----------------------------------------------------------------------
c     For convenience
c-----------------------------------------------------------------------
      mpsi=gIn%mpsi; mtheta=gIn%mtheta
c-----------------------------------------------------------------------
c     define positions  
c-----------------------------------------------------------------------
      IF(gIn%grid_method == "original")mpsi=eqIn%sq_in%nodes
      CALL position(eqIn,mEq)
c-----------------------------------------------------------------------
c     Handle the rblock (if psihigh > 0) and tblock regions
c-PRE  Anything else would have to be handled here.     
c-----------------------------------------------------------------------
      IF (gIn%psihigh > 0.) THEN 
        CALL process_structured(gIn,io,eqIn,mEq)
        CALL write_piegrid
      ENDIF
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE mapDirectEq


c-----------------------------------------------------------------------
c     subprogram 2. process_structured.
c     Process region between psilow and psihigh
c-----------------------------------------------------------------------
      SUBROUTINE process_structured(gIn,io,eqIn,mEq)
      USE fg_local
      USE fg_physdat
      USE lsode_mod
      USE flder_mod
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      USE grid
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

c-----------------------------------------------------------------------
c     declarations pertaining to lsode.
c-----------------------------------------------------------------------
      INTEGER(i4) :: iopt,istate,itask,itol,mf,istep
      INTEGER(i4), PARAMETER :: neq=4,liw=20,lrw=22+neq*16
      REAL(r8) :: atol,rtol
      INTEGER(i4), DIMENSION(liw) :: iwork=0
      REAL(r8), DIMENSION(neq) :: y
      REAL(r8), DIMENSION(lrw) :: rwork=0
c-----------------------------------------------------------------------
c     other local variables.
c-----------------------------------------------------------------------
      TYPE(q_type) :: qdat
      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      INTEGER(i4) :: itau,itheta,ipsi,ilim
      INTEGER(i4), PARAMETER :: nstep=16384
      INTEGER(i4), PARAMETER :: nlim=16384
      REAL(r8) :: btor, jr,jz,jt,vt,shift,modpp,vsnd
      REAL(r8) :: dr,rfac,r,z,psifac,eta,err,psi0
      REAL(r8) :: v11,v12,v21,v22,g11,g22,g12,jacob,jacfac
      REAL(r8), DIMENSION(0:nstep,0:5) :: temp
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rcoord
      REAL(r8), PARAMETER :: eps=1e-12
      TYPE(spline_type) :: ff
      TYPE(bicube_type) :: rz
      REAL(r8) :: psio
      CHARACTER(128) :: tt
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie,pd_fg,pd_mesh
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
      psio=eqIn%psio; pd_fg=gIn%pd; pd_mesh=gIn%pd_mesh
c-----------------------------------------------------------------------
c     Get things set up for the flder
c-----------------------------------------------------------------------
      CALL copyFldMembers(gIn,eqIn)
c-----------------------------------------------------------------------
c     Set up psi grid
c-----------------------------------------------------------------------
      ALLOCATE(rcoord(0:mpsi))
      CALL calc_geomx(eqIn,mEq)
      CALL psigrid(gIn,io,qdat,rcoord,
     &             eqIn%sq_in%xs,eqIn%sq_in%fs(:,3),gIn%pd,gIn%pd_mesh)
      mpsi = SIZE(rcoord)-1
c-----------------------------------------------------------------------
c     Allocate global arrays
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%sq,mpsi,mEq%nsq)
      CALL bicube_alloc(mEq%twod,mtheta,mpsi,mEq%ntwod)
      CALL bicube_alloc(r2g,mtheta,mpsi,2_i4)
      CALL bicube_alloc(rz,mtheta,mpsi,2_i4)
c-----------------------------------------------------------------------
c     find flux surface.
c-----------------------------------------------------------------------
      DO ipsi=mpsi,0,-1
         IF (gIn%radial_variable=="bigr") THEN
            r=rcoord(ipsi)*dbigr0+mEq%ro
            z=mEq%zo
            CALL get_bfield(r,z,eqIn,1_i4)
         ELSE
            psifac=rcoord(ipsi)
            psi0=psio*(1-psifac)
            r=mEq%ro+SQRT(psifac)*(mEq%rs2-mEq%ro)
            z=mEq%zo
            ilim=0
            DO
               ilim=ilim+1
               CALL get_bfield(r,z,eqIn,1_i4)
               dr=(psi0-eqIn%bf%psi)/eqIn%bf%psir
               r=r+dr
               IF(ABS(dr) <= eps*r)EXIT
               IF(ilim < nlim) THEN
                 CALL fg_stop("Exceeded limit while looking for grid")
                 EXIT
               ENDIF
            ENDDO
         ENDIF
         psi0=eqIn%bf%psi
         mEq%sq%xs(ipsi)=1.-psi0/psio
         r2g%ys(ipsi)=mEq%sq%xs(ipsi)
         mEq%twod%ys(ipsi)=mEq%sq%xs(ipsi)
         rz%ys(ipsi)=mEq%sq%xs(ipsi)
c-----------------------------------------------------------------------
c     initialize variables.
c-----------------------------------------------------------------------
         istep=0
         eta=0.
         y(1)=0.
         y(2)=SQRT((r-mEq%ro)**2+(z-mEq%zo)**2)
         y(3)=0.
         y(4)=0.
c-----------------------------------------------------------------------
c     set up integrator parameters.
c-----------------------------------------------------------------------
         istate=1; itask=5; iopt=1; mf=10; itol=1; 
         rtol=gIn%tol0; atol=gIn%tol0*y(2);rwork(1)=twopi; rwork(11)=0
c-----------------------------------------------------------------------
c     advance differential equations  and store results for each step.
c                 y(1) --> transformed angle
c                 y(2) --> effective radial coordinate
c                 y(3) --> 1/R^2 (used to calculate q)
c                 y(4) --> packing angle
c-----------------------------------------------------------------------
         DO
            rfac=y(2)
            CALL refine(rfac,eta,psi0,eqIn,mEq)
            r=mEq%ro+rfac*COS(eta)
            z=mEq%zo+rfac*SIN(eta)
            CALL get_bfield(r,z,eqIn,2_i4)
            temp(istep,0)=y(1)                  ! transformed angle
            temp(istep,1)=rfac**2               ! effective radius squared
            temp(istep,2)=eta                   ! geometric angle.
            temp(istep,3)=y(4)                  ! packing angle
            temp(istep,4)=r                     ! r
            temp(istep,5)=z                     ! z
            err=(eqIn%bf%psi-psi0)/eqIn%bf%psi
            IF(eta >= twopi .OR. istep >= nstep  .OR.  istate < 0
     $            .OR. ABS(err) >= 1)EXIT
            istep=istep+1
            CALL lsode(flder,(/neq/),y,eta,twopi,itol,
     $           (/rtol/),(/atol/),itask,
     $           istate,iopt,rwork,lrw,iwork,liw,mf=mf)
         ENDDO
c-----------------------------------------------------------------------
c     check for exceeding array bounds.
c-----------------------------------------------------------------------
         IF(eta < twopi)THEN
            WRITE(6,'(a,i4,a,1p,e10.3,a,i3)')
     $           ' In SUBROUTINE newsurf, istep = nstep =',nstep,
     $           ' at eta = ',eta,', ipsi =',ipsi
            STOP
         ENDIF
c-----------------------------------------------------------------------
c        Map temp(0:istep) -> r2g(0:mtheta,:,:), twod(0:mtheta,:,:)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c        Ensure periodicity
c-----------------------------------------------------------------------
         temp(0:istep,0)=temp(0:istep,0)*twopi/y(1)
         temp(0:istep,3)=temp(0:istep,3)*twopi/y(4)
c-----------------------------------------------------------------------
c        On first step, set xs (theta grid)
c-----------------------------------------------------------------------
         IF(ipsi == mpsi)THEN
            SELECT CASE(TRIM(gIn%angle_method))
            CASE('jac')
               CALL taugrid(gIn,temp(:,0),temp(:,3),r2g%xs,
     &                      istep,pd_fg,pd_mesh)
            CASE('geom')
               CALL taugrid(gIn,temp(:,2),temp(:,3),r2g%xs,
     &                      istep,pd_fg,pd_mesh)
            CASE default
              CALL fg_stop('Invalid angle_method')
            END SELECT
            mEq%twod%xs = r2g%xs                           ! theta
            rz%xs = r2g%xs                             ! theta
         ENDIF
c-----------------------------------------------------------------------
c        fit to cubic splines vs. theta and interpolate to tau grid.
c-----------------------------------------------------------------------
         CALL spline_alloc(ff,istep,4_i4)
         SELECT CASE(TRIM(gIn%angle_method))
         CASE('jac')
           ff%xs(0:istep)=temp(0:istep,0)               ! Calculated angle
         CASE('geom')
           ff%xs(0:istep)=temp(0:istep,2)               ! Geometric angle
         END SELECT
         ff%fs(0:istep,1)=temp(0:istep,1)
         ff%fs(0:istep,2)=temp(0:istep,2)-ff%xs(0:istep)
         ff%fs(0:istep,3)=temp(0:istep,4)
         ff%fs(0:istep,4)=temp(0:istep,5)
         CALL spline_fit(ff,"periodic")
         DO itau=0,mtheta
            CALL spline_eval(ff,r2g%xs(itau),0_i4)
            r2g%fs(1,itau,ipsi)=ff%f(1)
            r2g%fs(2,itau,ipsi)=ff%f(2)
            mEq%twod%fs(1,itau,ipsi)=ff%f(3)
            mEq%twod%fs(2,itau,ipsi)=ff%f(4)
            rz%fs(1,itau,ipsi)=ff%f(3)
            rz%fs(2,itau,ipsi)=ff%f(4)
         ENDDO
         CALL spline_dealloc(ff)
c-----------------------------------------------------------------------
c     evaluate surface quantities.
c-----------------------------------------------------------------------
         mEq%sq%fs(ipsi,1)=eqIn%bf%f
         mEq%sq%fs(ipsi,2)=eqIn%bf%p
         mEq%sq%fs(ipsi,3)=y(3)*eqIn%bf%f/twopi
         mEq%sq%fs(ipsi,4)=eqIn%bf%mach
         mEq%sq%fs(ipsi,5)=eqIn%bf%nd
         mEq%sq%fs(ipsi,6)=eqIn%bf%pe
         mEq%sq%fs(ipsi,7)=eqIn%bf%zave
         mEq%sq%fs(ipsi,8)=eqIn%bf%zeff
      ENDDO
c-----------------------------------------------------------------------
c     fit to splines.
c-----------------------------------------------------------------------
      CALL bicube_fit(r2g,"periodic","extrap")
      CALL bicube_fit(rz,"periodic","extrap")
      !CHAR tt='    psi   '//'    f     '//'    p     '//'    q     '
      !CHAR tt=TRIM(tt)//'   mach   '//'   ndens  '//'    pe    '
      !CHAR mEq%sq%title=tt
      CALL spline_fit(mEq%sq,"extrap")
c-----------------------------------------------------------------------
c     compute metric quantities.
c     Inside a radius rjac use r2g grid, outside use twod (RZ) grid
c     Works better for highly shaped equilibria due to nature of derivs
c-----------------------------------------------------------------------
      DO ipsi=0,mpsi
         DO itheta=0,mtheta
            rfac=SQRT(r2g%fs(1,itheta,ipsi))
            eta=r2g%fs(2,itheta,ipsi)+r2g%xs(itheta)
c            r=ro+rfac*COS(eta)
            r=mEq%twod%fs(1,itheta,ipsi)
            IF (SQRT(mEq%sq%xs(ipsi)) < gIn%rjac) THEN
              jacfac=r2g%fsy(1,itheta,ipsi)*(1.+r2g%fsx(2,itheta,ipsi))
     $           -r2g%fsx(1,itheta,ipsi)*r2g%fsy(2,itheta,ipsi)
              v11= (1.+r2g%fsx(2,itheta,ipsi))*2.*rfac/jacfac
              v12=-r2g%fsx(1,itheta,ipsi)/(rfac*jacfac)
              v21=-r2g%fsy(2,itheta,ipsi) *2.*rfac/jacfac
              v22= r2g%fsy(1,itheta,ipsi)/(rfac*jacfac)
            ELSE
              jacfac = 2.*(
     $         rz%fsy(1,itheta,ipsi)*rz%fsx(2,itheta,ipsi)-
     $         rz%fsx(1,itheta,ipsi)*rz%fsy(2,itheta,ipsi))
              v11=-rz%fsx(2,itheta,ipsi)*2./jacfac           ! d(rho)/dR
              v12= rz%fsx(1,itheta,ipsi)*2./jacfac         ! d(rho)/dZ
              v21= rz%fsy(2,itheta,ipsi)*2./jacfac         ! d(theta)/dR
              v22=-rz%fsy(1,itheta,ipsi)*2./jacfac         ! d(theta)/dZ
            ENDIF
            g11 = v11*v11+v12*v12
            g12 = v11*v21+v12*v22
            g22 = v21*v21+v22*v22
            jacob=r*jacfac/2.
            mEq%twod%fs(3,itheta,ipsi)= jacob/psio   ! Jac. w/ psi
            mEq%twod%fs(4,itheta,ipsi)= g11*psio**2       ! g^(psi,psi)
            mEq%twod%fs(5,itheta,ipsi)= g12*psio          ! g^(psi,theta)
            mEq%twod%fs(6,itheta,ipsi)= g22               ! g^(theta,theta)
         ENDDO
      ENDDO
c-----------------------------------------------------------------------
c     fit to splines.
c-----------------------------------------------------------------------
      CALL bicube_fit(mEq%twod,"periodic","extrap")
c-----------------------------------------------------------------------
c     If requested, calculate the bfield directly from the zmin_ina.
c-----------------------------------------------------------------------
      IF (gIn%calc_direct .OR. mvac>0) THEN
        CALL bicube_alloc(mEq%dir,mtheta,mpsi+mvac,mEq%ndir)
        mEq%direct_flag=1
        mEq%dir%xs = mEq%twod%xs                  ! theta
        mEq%dir%ys(0:mpsi) = mEq%twod%ys          ! radial coord.
        DO ipsi=0,mpsi
          DO itheta=0,mtheta
            r= mEq%twod%fs(1,itheta,ipsi)
            z= mEq%twod%fs(2,itheta,ipsi)
            CALL get_bfield(r,z,eqIn,1_i4)
            shift=eqIn%bf%mach**2*((r/mEq%ro)**2-1)
            vsnd=(2.*eqIn%bf%p/mu0/eqIn%bf%nd/ms(2)/mEq%zz)**0.5
            vt=r/mEq%ro* eqIn%bf%mach*vsnd
            btor=eqIn%bf%f/r
            jr = -eqIn%bf%f1*eqIn%bf%br/mu0/psio
            jz = -eqIn%bf%f1*eqIn%bf%bz/mu0/psio
            modpp=EXP(shift)*(eqIn%bf%p1 
     &       +eqIn%bf%p*2.*eqIn%bf%mach1*eqIn%bf%mach*((r/mEq%ro)**2-1))
            jt = -(eqIn%bf%f*eqIn%bf%f1 + r**2 * modpp)/psio/(mu0*r**2)

            mEq%dir%fs(1,itheta,ipsi)=r
            mEq%dir%fs(2,itheta,ipsi)=z
            mEq%dir%fs(3,itheta,ipsi)=eqIn%bf%br
            mEq%dir%fs(4,itheta,ipsi)=eqIn%bf%bz
            mEq%dir%fs(5,itheta,ipsi)=btor
            mEq%dir%fs(6,itheta,ipsi)=jr
            mEq%dir%fs(7,itheta,ipsi)=jz
            mEq%dir%fs(8,itheta,ipsi)=jt
            mEq%dir%fs(9,itheta,ipsi)=vt
            mEq%dir%fs(10,itheta,ipsi)=eqIn%bf%p *EXP(shift)/mu0
            mEq%dir%fs(11,itheta,ipsi)=eqIn%bf%pe*EXP(shift)/mu0
            mEq%dir%fs(12,itheta,ipsi)=eqIn%bf%nd*EXP(shift)
            mEq%dir%fs(13,itheta,ipsi)=(1.-mEq%sq%xs(ipsi))*psio
            mEq%dir%fs(14,itheta,ipsi)=SQRT(mEq%sq%xs(ipsi))     ! High A expansion
          ENDDO
        ENDDO
        CALL bicube_fit(mEq%dir,"periodic","extrap")
      ENDIF
c-----------------------------------------------------------------------
c     fit outer boundary to cubic splines and diagnose 2D quantities.
c     Note: ob = outer boundary of rblock region, and not separatrix
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%ob,mtheta,4_i4)
      mEq%ob%xs=r2g%xs
      mEq%ob%fs(:,1:2)=TRANSPOSE(r2g%fs(:,:,mpsi))
      mEq%ob%fs(:,3:4)=TRANSPOSE(mEq%twod%fs(1:2,:,mpsi))
      CALL spline_fit(mEq%ob,"periodic")
c-----------------------------------------------------------------------
c     Find separatrix (rzsep spline type & rsep,zsep arrays)
c-----------------------------------------------------------------------
      CALL separatrix(gIn,eqIn,mEq)
c-----------------------------------------------------------------------
c     Generate rblocks for vacuum region if asked for
c-----------------------------------------------------------------------
      IF (TRIM(gIn%extr_type) /= 'none') CALL calc_vacuum(gIn,eqIn,mEq)
c-----------------------------------------------------------------------
c     Diagnose the mapping if asked for (2d.txt, 2d.bin)
c-----------------------------------------------------------------------
      IF(io%out_2d .OR. io%bin_2d) CALL write_2d(gIn,mEq,r2g,io)
c-----------------------------------------------------------------------
c     Dellocate arrays
c-----------------------------------------------------------------------
      CALL bicube_dealloc(r2g)
      CALL bicube_dealloc(rz)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE process_structured
c-----------------------------------------------------------------------
c     subprogram 2. get_bfield.
c     evaluates bicubic splines for field components and derivatives.
c     NOTE: The returned pressure, electron pressure, and density do not
c     include the shear flow.
c-----------------------------------------------------------------------
      SUBROUTINE get_bfield(r,z,eqIn,mode)
      USE fg_local
      USE fg_bicube
      USE fgInputEqTypes
      IMPLICIT NONE

      INTEGER(i4), INTENT(IN) :: mode
      REAL(r8), INTENT(IN) :: r,z
      TYPE(inputEq), INTENT(INOUT) :: eqIn
c-----------------------------------------------------------------------
c     compute spline interpolations.
c-----------------------------------------------------------------------
      CALL bicube_eval(eqIn%psi_in,r,z,mode)
      eqIn%bf%psi=eqIn%psi_in%f(1)
      CALL spline_eval(eqIn%sq_in,1.-eqIn%bf%psi/eqIn%psio,1_i4)
      eqIn%bf%f=eqIn%sq_in%f(1)
      eqIn%bf%f1=eqIn%sq_in%f1(1)
      eqIn%bf%p=eqIn%sq_in%f(2)
      eqIn%bf%p1=eqIn%sq_in%f1(2)
      eqIn%bf%mach=eqIn%sq_in%f(4)
      eqIn%bf%mach1=eqIn%sq_in%f1(2)
      eqIn%bf%nd=eqIn%sq_in%f(5)
      eqIn%bf%pe=eqIn%sq_in%f(6)
      eqIn%bf%zave=eqIn%sq_in%f(7)
      eqIn%bf%zeff=eqIn%sq_in%f(8)
      IF(mode == 0)RETURN
c-----------------------------------------------------------------------
c     evaluate magnetic fields.
c-----------------------------------------------------------------------
      eqIn%bf%psir=eqIn%psi_in%fx(1)
      eqIn%bf%psiz=eqIn%psi_in%fy(1)
      eqIn%bf%br= eqIn%bf%psiz/r
      eqIn%bf%bz=-eqIn%bf%psir/r
      IF(mode == 1)RETURN
c-----------------------------------------------------------------------
c     evaluate derivatives of magnetic fields.
c-----------------------------------------------------------------------
      eqIn%bf%psirr=eqIn%psi_in%fxx(1)
      eqIn%bf%psirz=eqIn%psi_in%fxy(1)
      eqIn%bf%psizz=eqIn%psi_in%fyy(1)
      eqIn%bf%brr=(eqIn%bf%psirz-eqIn%bf%br)/r
      eqIn%bf%brz=eqIn%bf%psizz/r
      eqIn%bf%bzr=-(eqIn%bf%psirr+eqIn%bf%bz)/r
      eqIn%bf%bzz=-eqIn%bf%psirz/r
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE get_bfield
c-----------------------------------------------------------------------
c     subprogram 3. position.
c     finds radial positions of o-point and separatrix.
c-----------------------------------------------------------------------
      SUBROUTINE position(eqIn,mEq)
      USE fg_local
      USE fgInputEqTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      REAL(r8), PARAMETER :: eps=1e-12
      REAL(r8) :: ajac(2,2),det,dr,dz,fac,r,z
c-----------------------------------------------------------------------
c     scan to find zero crossing of bz on midplane.
c-----------------------------------------------------------------------
      IF(eqIn%ro == 0)THEN
         r=(eqIn%rmax_in+eqIn%rmin_in)/2
         z=(eqIn%zmax_in+eqIn%zmin_in)/2
         dr=(eqIn%rmax_in-eqIn%rmin_in)/20
         DO
            CALL get_bfield(r,z,eqIn,1_i4)
            IF(eqIn%bf%bz >= 0)EXIT
            r=r+dr
         ENDDO
      ELSE
         r=eqIn%ro
         z=eqIn%zo
      ENDIF
c-----------------------------------------------------------------------
c     use newton iteration to find o-point.
c-----------------------------------------------------------------------
      DO
         CALL get_bfield(r,z,eqIn,2_i4)
         ajac(1,1)=eqIn%bf%brr
         ajac(1,2)=eqIn%bf%brz
         ajac(2,1)=eqIn%bf%bzr
         ajac(2,2)=eqIn%bf%bzz
         det=ajac(1,1)*ajac(2,2)-ajac(1,2)*ajac(2,1)
         dr=(ajac(1,2)*eqIn%bf%bz-ajac(2,2)*eqIn%bf%br)/det
         dz=(ajac(2,1)*eqIn%bf%br-ajac(1,1)*eqIn%bf%bz)/det
         r=r+dr
         z=z+dz
         IF(ABS(dr) <= eps*r .AND. ABS(dz) <= eps*r)EXIT
      ENDDO
      mEq%ro=r;  mEq%zo=z
c-----------------------------------------------------------------------
c     renormalize psi.
c-----------------------------------------------------------------------
      fac=eqIn%psio/eqIn%bf%psi
      eqIn%psi_in%fs=eqIn%psi_in%fs*fac
      eqIn%psi_in%fsx=eqIn%psi_in%fsx*fac
      eqIn%psi_in%fsy=eqIn%psi_in%fsy*fac
      eqIn%psi_in%fsxy=eqIn%psi_in%fsxy*fac
c-----------------------------------------------------------------------
c     use newton iteration to find inboard separatrix position.
c-----------------------------------------------------------------------
      r=(3*eqIn%rmin_in+mEq%ro)/4
      z=mEq%zo
      DO
         CALL get_bfield(r,z,eqIn,1_i4)
         dr=-eqIn%bf%psi/eqIn%bf%psir
         r=r+dr
         IF(ABS(dr) <= eps*r)EXIT
      ENDDO
      mEq%rs1=r
c-----------------------------------------------------------------------
c     use newton iteration to find outboard separatrix position.
c-----------------------------------------------------------------------
      r=(mEq%ro+3*eqIn%rmax_in)/4
      DO
         CALL get_bfield(r,z,eqIn,1_i4)
         dr=-eqIn%bf%psi/eqIn%bf%psir
         r=r+dr
         IF(ABS(dr) <= eps*r)EXIT
      ENDDO
      mEq%rs2=r
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE position
c-----------------------------------------------------------------------
c     subprogram 5. calc_geomx
c     Create a spline type of geometric radial coordinate x vs. psi.  
c     The geometrical x in this case is (R(ray)-Ro)/DeltaR) where the
c     ray chosen is from the O-point to the separatrix along midplane
c     to the separatrix.  It is then normalized between 0 and 1 like
c     psi.  In this way, we can choose either a geometry based grid
c     or a psi-based grid. geomx defined in grid module.
c     For convenience, also do the inverse.
c-----------------------------------------------------------------------
      SUBROUTINE calc_geomx(eqIn,mEq)
      USE fg_local
      USE grid
      USE fgInputEqTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      REAL(r8), PARAMETER :: eps=1e-10
      REAL(r8), DIMENSION(:), POINTER :: psix
      INTEGER(i4) :: ma,ix
      REAL(r8) :: dr,r,z,psifac,psi0
      LOGICAL, PARAMETER :: diagnose=.TRUE.
c-----------------------------------------------------------------------
      psix=>eqIN%sq_in%xs
      ma=eqIN%ms_in
      CALL spline_alloc(rvss,ma,1_i4)
      rvss%xs=psix

      DO ix=ma,1,-1
         psifac=psix(ix)
         psi0=eqIn%psio*(1-psifac)
         r=mEq%ro+SQRT(psifac)*(mEq%rs2-mEq%ro)
         z=mEq%zo
         DO
            CALL get_bfield(r,z,eqIn,1_i4)
            dr=(psi0-eqIn%bf%psi)/eqIn%bf%psir
            r=r+dr
            IF(ABS(dr) <= eps*r)EXIT
         ENDDO
         rvss%fs(ix,1)=r
      ENDDO
      rvss%fs(0,1)=mEq%ro
      dbigr0=rvss%fs(ma,1)-mEq%ro
      rvss%fs(:,1)=(rvss%fs(:,1)-mEq%ro)/dbigr0
      CALL spline_fit(rvss,"extrap")

      CALL spline_alloc(svsr,ma,1_i4)
      svsr%xs=rvss%fs(:,1)
      svsr%fs(:,1)=rvss%xs
      CALL spline_fit(svsr,"extrap")
c-----------------------------------------------------------------------
c     Diagnostics
c-----------------------------------------------------------------------
      IF (diagnose) THEN
        CALL open_bin(binary_unit,'rvss.bin','UNKNOWN','REWIND',32_i4)
        DO ix=0,ma
          WRITE(binary_unit)REAL(ix,4),
     $                      REAL(rvss%xs(ix),4),REAL(rvss%fs(ix,1),4)
        ENDDO
        CALL close_bin(binary_unit,'rvss.bin')

        CALL open_bin(binary_unit,'svsr.bin','UNKNOWN','REWIND',32_i4)
        DO ix=0,ma
          WRITE(binary_unit)REAL(ix,4),
     $                      REAL(svsr%xs(ix),4),REAL(svsr%fs(ix,1),4)
        ENDDO
        CALL close_bin(binary_unit,'svsr.bin')

        OPEN(UNIT=ascii_unit,FILE='ssrr.txt',STATUS='UNKNOWN')
        DO ix=0,ma
          WRITE(ascii_unit,*)ix,rvss%xs(ix),svsr%fs(ix,1),
     $                          svsr%xs(ix),rvss%fs(ix,1)
        ENDDO
        CLOSE(UNIT=ascii_unit)
      ENDIF
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE calc_geomx
c-----------------------------------------------------------------------
c     subprogram 5. refine.
c     moves a point orthogonally to a specified flux surface.
c-----------------------------------------------------------------------
      SUBROUTINE refine(rfac,eta,psi0,eqIn,mEq)
      USE fg_local
      USE fgInputEqTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(IN) :: mEq
      
      REAL(r8) :: rfac,eta,psi0
      
      REAL(r8) :: dpsi,cosfac,sinfac,drfac,r,z
      REAL(r8), PARAMETER :: eps=1.e-12
c-----------------------------------------------------------------------
c     initialize iteration.
c-----------------------------------------------------------------------
      cosfac=COS(eta)
      sinfac=SIN(eta)
      r=mEq%ro+rfac*cosfac
      z=mEq%zo+rfac*sinfac
      CALL get_bfield(r,z,eqIn,1_i4)
      dpsi=eqIn%bf%psi-psi0
c-----------------------------------------------------------------------
c     refine rfac by newton iteration.
c-----------------------------------------------------------------------
      DO
         drfac=-dpsi/(eqIn%bf%psir*cosfac+eqIn%bf%psiz*sinfac)
         rfac=rfac+drfac
         r=mEq%ro+rfac*cosfac
         z=mEq%zo+rfac*sinfac
         CALL get_bfield(r,z,eqIn,1_i4)
         dpsi=eqIn%bf%psi-psi0
         IF(ABS(dpsi) <= eps*psi0 .OR. ABS(drfac) <= eps*rfac)EXIT
      ENDDO
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE refine
c-----------------------------------------------------------------------
c     subprogram 7. calc_vacuum.
c     Set up the grid and assign the equilibrium values for region
c     between outer boundary as defined by psihigh and vacuum vessel
c-----------------------------------------------------------------------
      SUBROUTINE calc_vacuum(gIn,eqIn,mEq)
      USE fg_local
      USE grid
      USE fg_physdat
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      INTEGER(i4) :: ivac, itheta
      REAL(r8) :: sep_distance
      REAL(r8) :: rnew,znew, radc, btor,jr,jz,jt,vt,radc2
      REAL(r8) :: shift,vsnd,modpp,fsep,psep,nsep,pesep,machsep
      REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: vac_rz
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
c-----------------------------------------------------------------------
c     Check input parameters (see input.f -- mvac used as the check)
c-----------------------------------------------------------------------
      IF(gIn%mvac == 0 )  RETURN
c-----------------------------------------------------------------------
c     Set up vacuum grid (see grid.f)
c-----------------------------------------------------------------------
      ALLOCATE(vac_rz(2,0:mtheta,0:mvac))
      CALL vacgrid(gIn,mEq,vac_rz,eqIn%rmin_in,eqIn%rmax_in,
     &              eqIn%zmin_in,eqIn%zmax_in,gIn%pd,gIn%pd_mesh)
c-----------------------------------------------------------------------
c     Allocate vac bicube type (see global.f for definitions)
c-----------------------------------------------------------------------
      mEq%dir%ys(mpsi:mpsi+mvac)=(/(REAL(ivac),ivac=0,mvac)/)/REAL(mvac)
      mEq%dir%fs(1:2,:,mpsi:mpsi+mvac)=vac_rz
      DEALLOCATE(vac_rz)
c-----------------------------------------------------------------------
c     Assign the equilibrium values to the grid
c     Calculation assumes no currents outside of separatrix
c-----------------------------------------------------------------------
      fsep=eqIn%sq_in%fs(eqIn%ms_in,1)
      psep=eqIn%sq_in%fs(eqIn%ms_in,2)
      machsep=eqIn%sq_in%fs(eqIn%ms_in,4)
      nsep=eqIn%sq_in%fs(eqIn%ms_in,5)
      pesep=eqIn%sq_in%fs(eqIn%ms_in,6)
      DO ivac=mpsi,mpsi+mvac
        rnew= mEq%dir%fs(1,0,ivac)
        radc2=(rnew-mEq%ro)/
     &         (mEq%twod%fs(1,0,mpsi)-mEq%ro)*SQRT(mEq%sq%xs(mpsi))
        DO itheta=0,mtheta
          rnew= mEq%dir%fs(1,itheta,ivac)
          znew= mEq%dir%fs(2,itheta,ivac)
          CALL get_bfield(rnew,znew,eqIn,1_i4)
          radc=sep_distance(rnew,znew,itheta,gIn,mEq)
          IF (radc < 0.) THEN                 ! Inside separatrix
             btor=eqIn%bf%f/rnew
             jr = -eqIn%bf%f1*eqIn%bf%br/mu0/mEq%psio
             jz = -eqIn%bf%f1*eqIn%bf%bz/mu0/mEq%psio
             shift=eqIn%bf%mach**2*((rnew/mEq%ro)**2-1)
             modpp=(eqIn%bf%p1+eqIn%bf%p*2.*eqIn%bf%mach1
     &             *eqIn%bf%mach*((rnew/mEq%ro)**2-1))*EXP(shift)
             jt = -(eqIn%bf%f*eqIn%bf%f1 + rnew**2*modpp)/
     &                        mEq%psio/(mu0*rnew**2)
             vsnd=(2.*ABS(eqIn%bf%p)/eqIn%bf%nd/ms(2)/mEq%zz)**0.5
             vt=rnew/mEq%ro* eqIn%bf%mach*vsnd
             radc= SQRT(1.-eqIn%bf%psi/mEq%psio)
          ELSE                                ! Outside separatrix
             eqIn%bf%mach=machsep
             eqIn%bf%p=psep
             eqIn%bf%nd=nsep
             eqIn%bf%pe=pesep
             shift=eqIn%bf%mach**2*((rnew/mEq%ro)**2-1)
             vsnd=(2.*eqIn%bf%p/eqIn%bf%nd/ms(2)/mEq%zz)**0.5
             vt=rnew/mEq%ro* eqIn%bf%mach*vsnd
             jr = 0.
             jz = 0.
             jt = 0.
             IF(rnew == 0) THEN
                btor=0.
             ELSE
                btor=fsep/rnew
             ENDIF
             radc=1. + radc
          ENDIF
          mEq%dir%fs(3,itheta,ivac)=eqIn%bf%br
          mEq%dir%fs(4,itheta,ivac)=eqIn%bf%bz
          mEq%dir%fs(5,itheta,ivac)=btor
          mEq%dir%fs(6,itheta,ivac)=jr
          mEq%dir%fs(7,itheta,ivac)=jz
          mEq%dir%fs(8,itheta,ivac)=jt
          mEq%dir%fs(9,itheta,ivac)=vt
          mEq%dir%fs(10,itheta,ivac)=eqIn%bf%p *EXP(shift)/mu0
          mEq%dir%fs(11,itheta,ivac)=eqIn%bf%pe*EXP(shift)/mu0
          mEq%dir%fs(12,itheta,ivac)=eqIn%bf%nd*EXP(shift)
          mEq%dir%fs(13,itheta,ivac)=eqIn%bf%psi
          !mEq%dir%fs(14,itheta,ivac)=radc
          mEq%dir%fs(14,itheta,ivac)=radc2
        ENDDO
      ENDDO
c-----------------------------------------------------------------------
c     Fit data
c-----------------------------------------------------------------------
      CALL bicube_fit(mEq%dir,"periodic","extrap")
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE calc_vacuum
c-----------------------------------------------------------------------
c     subprogram 8. write_piegrid.
c     reads positions, writes equilibrium quantities.
c-----------------------------------------------------------------------
      SUBROUTINE write_piegrid(eqIn)
      USE fgInputEqTypes
      IMPLICIT NONE

      TYPE(inputEq), INTENT(INOUT) :: eqIn
      INTEGER(i4) :: iv,idum,mvert
      INTEGER :: ios
      REAL(r8) :: rval,zval
      INTEGER, PARAMETER :: eq_unit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq1_unit=43
      INTEGER, PARAMETER :: eq2_unit=44
c-----------------------------------------------------------------------
c     computations.
c-----------------------------------------------------------------------
      OPEN(UNIT=eq_unit,FILE='pie.1.node', STATUS='old',iostat=ios)
      IF(ios.NE.0)RETURN
      READ(eq_unit,*)mvert,idum,idum,idum
      OPEN(UNIT=eq2_unit,FILE='pie.dat', STATUS='replace')
      WRITE(6,*)"Processing file pie.1.node"
      WRITE(6,*)"Writing file pie.dat"
      DO iv=1,mvert
        READ(eq_unit,*)idum,rval,zval,idum
        CALL get_bfield(rval,zval,eqIn,1_i4)
        write(eq2_unit,*)eqIn%bf%br,eqIn%bf%bz,eqIn%bf%f,eqIn%bf%p
      ENDDO
      CLOSE(UNIT=eq2_unit)
      CLOSE(UNIT=eq_unit)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE write_piegrid
c-----------------------------------------------------------------------
c     subprogram 8. get_psi_rz_direct
c     Sets up quantities to use by nimset, although can be used for
c     for any array.
c-----------------------------------------------------------------------
      SUBROUTINE get_psi_rz_direct(rzpoints,psip,psip_r,psip_z,eqIn)
      USE fg_local
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      REAL(r8), DIMENSION(:,0:,0:,:), INTENT(IN) :: rzpoints
      REAL(r8), DIMENSION(0:,0:,:), INTENT(INOUT) :: psip
      REAL(r8), DIMENSION(0:,0:,:), INTENT(INOUT) :: psip_r,psip_z
      INTEGER(i4) :: i1,i2,i3
      REAL(r8) :: rp,zp
c-----------------------------------------------------------------------
      DO i3=1,SIZE(rzpoints,4)
       DO i2=0,SIZE(rzpoints,3)-1
        DO i1=0,SIZE(rzpoints,2)-1
          rp= rzpoints(1,i1,i2,i3)
          zp= rzpoints(2,i1,i2,i3)
          CALL get_bfield(rp,zp,eqIn,1_i4)
          psip(i1,i2,i3)=eqIn%bf%psi
          psip_r(i1,i2,i3)=eqIn%bf%psir
          psip_z(i1,i2,i3)=eqIn%bf%psiz
        ENDDO
       ENDDO
      ENDDO
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE get_psi_rz_direct
c-----------------------------------------------------------------------
c     subprogram 9. separatrix.
c     Finds RZ values of separatrix
c     Based on Alan's contour finding routine (See process_eq also)
c     To avoid X points and its problems, the separatrix is defined here
c      as the flux surface that is epsln away from actual separatrix.
c-----------------------------------------------------------------------
      SUBROUTINE separatrix(gIn,eqIn,mEq)
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      USE lsode_mod
      USE fg_local
      USE fg_physdat
      USE flder_mod
      IMPLICIT NONE
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq
      INTEGER(i4) :: iopt,istate,itask,itol,mf,istep
      INTEGER(i4), PARAMETER :: neq=4,liw=20,lrw=22+neq*16
      REAL(r8) :: atol,rtol
      INTEGER(i4), DIMENSION(liw) :: iwork=0
      REAL(r8), DIMENSION(neq) :: y
      REAL(r8), DIMENSION(lrw) :: rwork=0
c-----------------------------------------------------------------------
c     other local variables.
c-----------------------------------------------------------------------
      REAL(r8),DIMENSION(:,:),ALLOCATABLE :: rzsep
      REAL(r8), DIMENSION(4,2) :: rzextrema
      INTEGER(i4), PARAMETER :: nstep=14096
      REAL(r8), PARAMETER :: eps=1.e-12, epsln=1e-4
      INTEGER(i4) :: itau
      INTEGER(i4), DIMENSION(1) :: loc
      REAL(r8) :: dr,rfac,r,z,psifac,eta,err,psi0
      REAL(r8), DIMENSION(0:nstep,0:4) :: temp
      TYPE(spline_type) :: ff
c-----------------------------------------------------------------------
c     INTERFACE statement
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE find_rzextrema(rzvy,rzextrema)
        USE fg_local
        USE fg_physdat
        USE fg_spline
        IMPLICIT NONE
        REAL(r8), DIMENSION(:,0:), INTENT(IN) :: rzvy
        REAL(r8), DIMENSION(4,2), INTENT(OUT) :: rzextrema
        END SUBROUTINE find_rzextrema
      END INTERFACE
c-----------------------------------------------------------------------
c     Initialize
c-----------------------------------------------------------------------
      psifac=1. - epsln                          ! Important
      psi0=eqIn%psio*(1-psifac)
      r=mEq%ro+SQRT(psifac)*(mEq%rs2-mEq%ro)
      z=mEq%zo
      DO
         CALL get_bfield(r,z,eqIn,1_i4)
         dr=(psi0-eqIn%bf%psi)/eqIn%bf%psir
         r=r+dr
         IF(ABS(dr) <= eps*r)EXIT
      ENDDO
      psi0=eqIn%bf%psi
c-----------------------------------------------------------------------
c     initialize variables for contour integration.
c-----------------------------------------------------------------------
      istep=0;  eta=0.
      y(1)=0.
      y(2)=SQRT((r-mEq%ro)**2+(z-mEq%zo)**2)
      y(3)=0.
      y(4)=0.
c-----------------------------------------------------------------------
c     set up integrator parameters.
c-----------------------------------------------------------------------
      istate=1; itask=5; iopt=1; mf=10
      itol=1; rtol=gIn%tol0; atol=gIn%tol0*y(2)
      rwork(1)=twopi; rwork(11)=0
c-----------------------------------------------------------------------
c     store results for each step.
c-----------------------------------------------------------------------
      DO
         rfac=y(2)
         CALL refine(rfac,eta,psi0,eqIn,mEq)
         r=mEq%ro+rfac*COS(eta)
         z=mEq%zo+rfac*SIN(eta)
         CALL get_bfield(r,z,eqIn,1_i4)
         temp(istep,0)=y(1)               !   transformed angle
         temp(istep,1)=rfac**2            !   effective radius squared
         temp(istep,2)=eta                !   geometric angle.
         temp(istep,3)=r                  !   r
         temp(istep,4)=z                  !   z
         err=(eqIn%bf%psi-psi0)/eqIn%bf%psi
c-----------------------------------------------------------------------
         IF(eta >= twopi .OR. istep >= nstep  .OR.  istate < 0
     $            .OR. ABS(err) >= 1)EXIT
         istep=istep+1
         CALL lsode(flder,(/neq/),y,eta,twopi,itol,
     $           (/rtol/),(/atol/),itask,
     $           istate,iopt,rwork,lrw,iwork,liw,mf=mf)
      ENDDO
c-----------------------------------------------------------------------
c     check for exceeding array bounds.
c-----------------------------------------------------------------------
      IF(eta < twopi)THEN
         WRITE(6,'(a,i4,a,1p,e10.3,a,i3)')
     $           ' In SUBROUTINE separatrix, istep = nstep =',nstep,
     $           ' at eta = ',eta
         STOP
      ENDIF
c-----------------------------------------------------------------------
c     fit to cubic splines vs. theta and interpolate to tau grid.
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%rzsep,gIn%mtheta,2_i4)
      mEq%rzsep%xs=mEq%twod%xs
      temp(0:istep,0)=temp(0:istep,0)*twopi/y(1)
      CALL spline_alloc(ff,istep,2_i4)
      SELECT CASE(TRIM(gIn%angle_method))
      CASE('jac')
        ff%xs(0:istep)=temp(0:istep,0)
      CASE('geom')
        ff%xs(0:istep)=temp(0:istep,2)
      END SELECT
      ff%fs(0:istep,1)=temp(0:istep,3)
      ff%fs(0:istep,2)=temp(0:istep,4)
      CALL spline_fit(ff,"periodic")
      DO itau=0,gIn%mtheta
         CALL spline_eval(ff,mEq%twod%xs(itau),0_i4)
         mEq%rzsep%fs(itau,1)=ff%f(1);   mEq%rzsep%fs(itau,2)=ff%f(2)
      ENDDO
      CALL spline_fit(mEq%rzsep,"periodic")
c-----------------------------------------------------------------------
c     Save the larger array of separtrix positions since it will allow
c      a more accurate determination of distances.  Find key positions.
c-----------------------------------------------------------------------
      ALLOCATE(rzsep(2,0:istep))
      rzsep=TRANSPOSE(temp(0:istep,3:4))
      CALL find_rzextrema(rzsep,rzextrema)
      mEq%rsn=rzextrema(1,1);     mEq%zsn=rzextrema(1,2)
      mEq%rsx=rzextrema(2,1);     mEq%zsx=rzextrema(2,2)
      mEq%rsb=rzextrema(3,1);     mEq%zsb=rzextrema(3,2)
      mEq%rst=rzextrema(4,1);     mEq%zst=rzextrema(4,2)
      DEALLOCATE(rzsep)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE separatrix
c-----------------------------------------------------------------------
c     subprogram 10. sep_distance.
c     Gives the distance to separatrix along a theta array given by itau
c     Negative distance implies that we're inside the separatrix.
c-----------------------------------------------------------------------
      FUNCTION sep_distance(r,z,itau,gIn,mEq)
      USE fg_local
      USE fgControlTypes
      USE fgMapEqTypes
      IMPLICIT NONE

      REAL(r8), INTENT(IN) :: r,z
      INTEGER(i4), INTENT(IN) :: itau
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      REAL(r8), PARAMETER :: eps=1e-10
      REAL(r8) :: sep_distance,rs,zs,rw,zw,bnorm,anorm,dnorm
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
c-----------------------------------------------------------------------
      rs=mEq%rzsep%fs(itau,1);   zs=mEq%rzsep%fs(itau,2)
      sep_distance = SQRT((r - rs)**2 + (z - zs)**2)
c-----------------------------------------------------------------------
c     Find out whether we're in or out
c-----------------------------------------------------------------------
      IF( (mEq%ro-r)*(rs-r)<=eps .AND. (mEq%zo-z)*(zs-z)<=eps ) THEN
         sep_distance = -sep_distance
      ENDIF
c-----------------------------------------------------------------------
c     Normalize such that at the wall, we have uniform values equal
c      to an approximate b/a-1
c-----------------------------------------------------------------------
      rw= mEq%dir%fs(1,itau,mpsi+mvac); zw= mEq%dir%fs(2,itau,mpsi+mvac)
      dnorm = SQRT((rw - rs)**2 + (zw - zs)**2)
      rw= mEq%dir%fs(1,0,mpsi+mvac); zw= mEq%dir%fs(2,0,mpsi+mvac)
      bnorm = SQRT((rw - mEq%ro)**2 + (zw - mEq%zo)**2)
      rs=mEq%rzsep%fs(0,1);   zs=mEq%rzsep%fs(0,2)
      anorm = SQRT((rs - mEq%ro)**2 + (zs - mEq%zo)**2)
      sep_distance = sep_distance/dnorm * (bnorm/anorm - 1.)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END FUNCTION sep_distance
