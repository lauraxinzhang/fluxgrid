!---------------------------------------------------------------------
!#
!# File:  input_type.f.
!#
!# Purpose:  Declare the common derived types for controllinng the
!#           mapping
!#                          
!#           
!#
!## $Id: input_type.f 1276 2009-08-05 21:54:17Z srinath $
!---------------------------------------------------------------------
!> These are types to control how fluxgrid works.  It is based on the
!! variables in the global common block in fg_input.f and one should
!! see the variables defined in it for definition.
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!     subprogram 0. control_type
!-----------------------------------------------------------------------
      MODULE fgControlTypes
      USE fg_local
      IMPLICIT NONE

!> fileData
!! @Used for provenance and to control where to get the data
      TYPE :: fileData                    !#SIDL_ST
        CHARACTER(64) :: origFile         !< File that the data comes from
        CHARACTER(64) :: fileType         !< Format of file: efit, miller, etc.
        CHARACTER(64) :: auxFile         !< Format of file: efit, miller, etc.
        CHARACTER(64) :: auxType         !< Format of file: efit, miller, etc.
        CHARACTER(64) :: eqType           !< direct or inverse
        CHARACTER(64) :: runlabel         !< A label for the output files
        CHARACTER(64) :: inputFile        !< The input file for controlling fluxgrid
      END TYPE

!> controlMap
!! @note Mostly grid control settings
       TYPE :: controlMap           !#SIDL_ST
        INTEGER(i4) :: mpsi
        INTEGER(i4) :: mtheta
        INTEGER(i4) :: mvac
        CHARACTER(8) :: grid_method
        CHARACTER(8) :: pack_method
        REAL(r8) :: radial_power
        CHARACTER(8) :: radial_variable
        INTEGER(i4) :: nn
        REAL(r8) :: qsmin
        REAL(r8) :: qsmax
        REAL(r8) :: wpack
        REAL(r8) :: amp
        CHARACTER(8) :: angle_method
        LOGICAL :: angle_pack
        INTEGER(i4) :: ipb
        INTEGER(i4) :: ipr
        REAL(r8) :: jpb
        REAL(r8) :: jpr
        REAL(r8) :: jpz
        REAL(r8) :: psilow
        REAL(r8) :: psihigh
        REAL(r8) :: vac_frac
        REAL(r8) :: extr
        CHARACTER(12) :: extr_type
        CHARACTER(12) :: extr_center
        CHARACTER(128) :: wall_file
        INTEGER(i4) :: wall_modes
        LOGICAL :: vac_pack
        LOGICAL :: triangle
        INTEGER(i4) :: pd
        INTEGER(i4) :: pd_mesh
        INTEGER(i4) :: mxpie
        REAL(r8) :: tol0
        LOGICAL :: interp
        LOGICAL :: calc_direct
        CHARACTER(8) :: j_t
        REAL(r8) :: rjac
       END TYPE controlMap
!-----------------------------------------------------------------------
!> controlAux
!! @note Things to control the profiles mostly
      TYPE :: controlAux                  !#SIDL_ST
         REAL(r8) :: zz
         REAL(r8) :: ndens
         REAL(r8) :: nedge
         REAL(r8) :: newq0
         CHARACTER(8) :: n_profile
         REAL(r8), DIMENSION(10) :: npp
         REAL(r8) :: pe_frac
         CHARACTER(8) :: pe_profile
         REAL(r8) :: te_cent
         REAL(r8) :: te_edge
         REAL(r8), DIMENSION(10) :: tepp
         REAL(r8) :: pressure_gauge
         REAL(r8) :: ion_temperature_gauge
         REAL(r8) :: electron_temperature_gauge !#SIDL_ST_MEMNAME=elecTempGauge#
         REAL(r8) :: ndensity_gauge
         REAL(r8) :: psi_norm_gauge
         REAL(r8) :: fnorm_input
         REAL(r8) :: rnorm_input
         CHARACTER(22) :: density_file
         CHARACTER(22) :: z_file
         CHARACTER(22) :: zeff_file
        LOGICAL :: edriftToX
        LOGICAL :: set_ndens
        LOGICAL :: set_pressure
        LOGICAL :: set_pressure_electron
        LOGICAL :: set_rotation
        LOGICAL :: set_zave
        LOGICAL :: set_zeff
        LOGICAL :: recalculate_pressure
      END TYPE controlAux
!-----------------------------------------------------------------------
!> fgControlIO
!! @note Just control the I/O and diagnostics related to various outputs
      TYPE :: fgControlIO                  !#SIDL_ST
        LOGICAL :: bin_1d
        LOGICAL :: out_1d
        LOGICAL :: bin_2d
        LOGICAL :: out_2d
        LOGICAL :: diagnose
        LOGICAL :: stability
        LOGICAL :: out_neoclassical
        LOGICAL :: out_toq
        LOGICAL :: out_nimrod
        LOGICAL :: out_pies
        LOGICAL :: out_pest
        LOGICAL :: out_far
        LOGICAL :: out_nimplot
        LOGICAL :: out_facets
        LOGICAL :: out_gyro
        INTEGER(i4) :: ncell_facets
        LOGICAL :: out_tecplot
        REAL(r8) :: sfac
        REAL(r8) :: pr
      END TYPE fgControlIO

      END MODULE fgControlTypes
c-----------------------------------------------------------------------
c     subprogram 1. set types from namelist
c-----------------------------------------------------------------------
      SUBROUTINE fgSetInputType(infile,tgin,tset,tio,fileIn) !#SIDL_SUB !#SIDL_SUB_NAME=fgSetInType#
      USE fgControlTypes
      USE fg_input
      USE fgInputEqTypes
      IMPLICIT NONE

      CHARACTER(*), INTENT(IN)           :: infile
      TYPE(controlMap), INTENT(INOUT)    :: tgin
      TYPE(controlAux), INTENT(INOUT)    :: tset
      TYPE(fgControlIO), INTENT(INOUT)   :: tio
      TYPE(fileData), INTENT(INOUT)      :: fileIn

c-----------------------------------------------------------------------
c     If infile is not given, then this will set the defaults of the
c     global values in the modules file
c-----------------------------------------------------------------------
      IF(infile/="") CALL read_fg_input(infile)
c-----------------------------------------------------------------------
c     Process equilibrium type
c-----------------------------------------------------------------------
      tgin%mpsi=mpsi
      tgin%mtheta=mtheta
      tgin%grid_method=grid_method
      tgin%pack_method=pack_method
      tgin%radial_power=radial_power
      tgin%radial_variable=radial_variable
      tgin%nn=nn
      tgin%qsmin=qsmin
      tgin%qsmax=qsmax
      tgin%wpack=wpack
      tgin%amp=amp
      tgin%angle_method=angle_method
      tgin%angle_pack=angle_pack
      tgin%ipb=ipb
      tgin%ipr=ipr
      tgin%jpb=jpb
      tgin%jpr=jpr
      tgin%jpz=jpz
      tgin%psilow=psilow
      tgin%psihigh=psihigh
      tgin%mvac=mvac
      tgin%vac_frac=vac_frac_fg
      tgin%extr=extr
      tgin%extr_type=extr_type
      tgin%extr_center=extr_center
      tgin%wall_file=wall_file
      tgin%wall_modes=wall_modes
      tgin%vac_pack=vac_pack
      tgin%pd=pd_fg
      tgin%pd_mesh=pd_mesh
      tgin%mxpie=mxpie
      tgin%triangle=triangle
      tgin%tol0=tol0
      tgin%interp=interp
      tgin%calc_direct=calc_direct
      tgin%j_t=j_t
      tgin%rjac=rjac
c-----------------------------------------------------------------------
c     Process equilibrium type
c-----------------------------------------------------------------------
      tset%newq0=newq0
      tset%zz=zeff_fg
      tset%ndens=ndens_fg
      tset%nedge=nedge_fg
      tset%n_profile=n_profile_fg
      tset%npp=npp_fg
      tset%pe_frac=pe_frac_fg
      tset%pe_profile=pe_profile_fg
      tset%te_cent=te_cent_fg
      tset%te_edge=te_edge_fg
      tset%tepp=tepp_fg
      tset%pressure_gauge=pressure_gauge
      tset%ion_temperature_gauge=ion_temperature_gauge
      tset%electron_temperature_gauge=electron_temperature_gauge
      tset%ndensity_gauge=ndensity_gauge
      tset%psi_norm_gauge=psi_norm_gauge
      tset%fnorm_input=fnorm_input
      tset%rnorm_input=rnorm_input
      tset%density_file=density_file
      tset%recalculate_pressure=recalculate_pressure
      tset%z_file=z_file
      tset%zeff_file=zeff_file
      tset%edriftToX=edriftToX
c-----------------------------------------------------------------------
c     I/O control
c-----------------------------------------------------------------------
      tio%bin_1d=bin_1d
      tio%out_1d=out_1d
      tio%bin_2d=bin_2d
      tio%out_2d=out_2d
      tio%diagnose=diagnose
      tio%stability=stability
      tio%out_neoclassical=out_neoclassical
      tio%out_toq=out_toq
      tio%out_nimrod=out_nimrod
      tio%out_pies=out_pies
      tio%out_pest=out_pest
      tio%out_far=out_far
      tio%out_nimplot=out_nimplot
      tio%out_gyro=out_gyro
      tio%out_facets=out_facets
      tio%ncell_facets=ncell_facets
      tio%out_tecplot=out_tecplot
      tio%sfac=sfac
      tio%pr=pr
      
c-----------------------------------------------------------------------
c     This ensures that there are integer number of vertices when
c     generating higher-order element grids.  The mxpie parameter is
c     because we generally don't use the center, but instead calculate
c     those values manually by extapolation to avoid problems on axis.
c-----------------------------------------------------------------------
      IF (tgin%pd>1) THEN
        tgin%mpsi=tgin%mpsi*tgin%pd-tgin%mxpie
        tgin%mtheta=tgin%mtheta*tgin%pd
        tgin%mvac=tgin%mvac*tgin%pd
      ENDIF

      fileIn%origFile=filename
      fileIn%fileType=eq_type
      fileIn%auxType=aux_type
      fileIn%auxFile=TRIM(aux_file)
      fileIn%eqType=eq_type
      fileIn%inputFile=infile

      RETURN
      END SUBROUTINE fgSetInputType

