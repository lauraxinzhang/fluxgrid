c-----------------------------------------------------------------------
c     file grid.f.
c     Routines that are useful for setting up the grid.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c      0. grid.
c      1. psigrid.
c      3. qfind.
c      7. drpack.
c      8. taugrid.
c      9. vacgrid.
c     10. vac_boundary.
c     11. get_wall.
c     12. rz2fourier.
c     13. fourier2rz.
c     14. fourier2grid.
c     15. geom_ratio.
c-----------------------------------------------------------------------
      MODULE grid
      USE fg_local
      USE fg_bicube
      USE fgControlTypes
      IMPLICIT NONE

      REAL(r8), DIMENSION(:), ALLOCATABLE, PRIVATE :: xi,delta
      INTEGER(i4), PRIVATE :: mpsiv
      REAL(r8) :: dbigr0,rhigh,rlow
      TYPE(spline_type) :: rvss,svsr

      TYPE :: q_type
      INTEGER(i4) :: nsing,nn
      INTEGER(i4), DIMENSION(64) :: msing
      REAL(r8) :: q0,qmin,qmax,qa
      REAL(r8), DIMENSION(64) :: qsing,q1sing,rsing
      END TYPE q_type

c-----------------------------------------------------------------------
c       Mapping information:
c         r2g%xs(:) = theta           r2g%ys(:) = Psi_normal
c         r2g%fs(1,:,:) = rho         r2g%fs(2,:,:) = eta
c-----------------------------------------------------------------------
      TYPE(bicube_type) :: r2g                  

      TYPE(q_type) :: qin

      CONTAINS
c-----------------------------------------------------------------------
c     subprogram 1. psigrid.
c     defines the radial grid.
c     Special care is handled for grids with higher-order elements.  In
c      that case (pd>1), interior points need to be equally spaced
c      between the vertices.
c     Note that if radial_variable=bigr, then rnew throughout most of
c      this calculation is in terms of R(ray).  Right before the return,
c      then the grid is converted to a psi-based grid.
c-----------------------------------------------------------------------
      SUBROUTINE psigrid(gIn,io,qt,rnew,eqgrid,eqq,pd,pd_mesh)
      USE fg_spline
      USE fg_physdat
      IMPLICIT NONE

      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(fgControlIO), INTENT(IN) :: io
      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      TYPE(q_type), INTENT(INOUT) :: qt
      REAL(r8), DIMENSION(0:), INTENT(IN) :: eqgrid, eqq
      REAL(r8), DIMENSION(0:), INTENT(OUT) :: rnew
      INTEGER(i4), INTENT(IN) :: pd,pd_mesh

      INTEGER(i4) :: ipsi,m, ipd,ivert,mpsic,itrp
      REAL(r8), DIMENSION(:), ALLOCATABLE :: xgrid,xpol,ypol
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rold, roldfe
      REAL(r8) :: dpsi,a,b,sinfac,cosfac,xi,yi,dyi
      REAL(r8), PARAMETER :: eps=1e-7
      REAL(r8), DIMENSION(:), ALLOCATABLE :: x_node

      REAL(r8), PARAMETER :: piby2=pi/2._r8
      LOGICAL, PARAMETER :: diagnose=.FALSE.
      CHARACTER(256) :: msg
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie,pd_fg
c-----------------------------------------------------------------------
c     INTERFACE for polynomial interpolation and for getting nodes
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE poly_nodes_fg(n,x)
        USE fg_local
        IMPLICIT NONE
        INTEGER(i4), INTENT(IN) :: n
        REAL(r8), DIMENSION(0:n), INTENT(OUT) :: x
        END SUBROUTINE poly_nodes_fg
      END INTERFACE
c-----------------------------------------------------------------------
c     Set up stuff for grid with higher_order elements
c-----------------------------------------------------------------------
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
      pd_fg=gIn%pd
      IF (pd==1) THEN
        mpsiv=mpsi+mxpie
        mpsic=mpsi+mxpie
      ELSE
        mpsic=(mpsi+mxpie)/pd_fg 
        mpsiv=(mpsi+mxpie)/pd_fg*pd_mesh 
      ENDIF
      IF (gIn%radial_variable=="bigr") THEN
        CALL spline_eval(rvss,gIn%psihigh,0_i4)
        rhigh=rvss%f(1)
        CALL spline_eval(rvss,gIn%psilow,0_i4)
        rlow=rvss%f(1)
      ELSE
        rhigh=gIn%psihigh
        rlow=gIn%psilow
      ENDIF
c-----------------------------------------------------------------------
c     Select grid method
c-----------------------------------------------------------------------
      ALLOCATE(xgrid(0:mpsiv),rold(0:mpsiv),roldfe(0:mpsi))
      SELECT CASE(gIn%grid_method)
      CASE("original")
       IF(pd_fg>1.OR.gIn%radial_variable/='psi'
     &           .OR.gIn%pack_method/='none') THEN
        msg="pd_fg, pack_method, or radial_variable"
        msg=msg//" incompatible with having the original psi mesh"
        CALL fg_stop(msg)
       ENDIF
       xgrid=eqgrid
c-----------------------------------------------------------------------
c     uniform grid.
c-----------------------------------------------------------------------
      CASE("uniform")
         xgrid=rhigh*(/(ipsi,ipsi=0,mpsiv)/)/mpsiv
c-----------------------------------------------------------------------
c     stretched grid.
c-----------------------------------------------------------------------
      CASE("stretch")
         m=mpsiv+2
         cosfac=COS(pi/(2*m))**2
         sinfac=SIN(pi/(2*m))**2
         a=(rlow*cosfac-rhigh*sinfac)/(cosfac-sinfac)
         b=(rhigh-rlow)/(cosfac-sinfac)
         xgrid=a+b*SIN(piby2*(/(ipsi,ipsi=0,m-1)/)/m)**2
c-----------------------------------------------------------------------
c     sqrt grid.
c-----------------------------------------------------------------------
      CASE("sqrt")
         xgrid=rhigh*(/(ipsi**2,ipsi=0,mpsiv)/)/(mpsiv)**2
c-----------------------------------------------------------------------
c     power grid.  Superset of uniform and sqrt
c-----------------------------------------------------------------------
      CASE("power")
         xgrid=rhigh*(/(ipsi**(1./gIn%radial_power),ipsi=0,mpsiv)/)
     $          /REAL(mpsiv)**(1./gIn%radial_power)
      CASE("use_gt")
        CALL fg_stop
     &     ("grid: Could not convert grid_type to grid_method")
c-----------------------------------------------------------------------
c     Stop if they haven't specified grid type
c-----------------------------------------------------------------------
      CASE default
         CALL fg_stop("grid: Unrecognized grid_method")
      END SELECT
c-----------------------------------------------------------------------
c     Temporary storage of the old grid for diagnostic purposes
c-----------------------------------------------------------------------
      rold = xgrid
c-----------------------------------------------------------------------
c     Calculate q stuff, and pack radial coordinate if asked for.
c-----------------------------------------------------------------------
      SELECT CASE(gIn%pack_method)
      CASE("none")
      CASE("gauss")
        CALL qfind(gIn,gIn%nn,qt,eqgrid,eqq)
        CALL drpack(gIn,qt,xgrid)
      CASE default
        WRITE(msg,*)"Unknown pack_method ",gIn%pack_method
        CALL fg_stop(msg)
      END SELECT
c-----------------------------------------------------------------------
c     If creating a higher-order element grid, then the interior
c     points need to be equally spaced.  
c-----------------------------------------------------------------------
      IF (pd==1) THEN
        rnew=xgrid(mxpie:mpsiv)
        roldfe=rold(mxpie:mpsiv)
      ELSE
        ALLOCATE(x_node(0:pd),xpol(0:pd_mesh),ypol(0:pd_mesh))
        xpol=(/(REAL(ipd),ipd=0,pd_mesh)/)/REAL(pd_mesh)
        CALL poly_nodes_fg(pd,x_node)

        DO ivert=1,mpsic
          itrp=ivert*pd_mesh
          ypol=xgrid(itrp-pd_mesh:itrp)
          DO ipd=1,pd
            !xi=REAL(ipd)/REAL(pd)
            xi=x_node(ipd)
            ipsi=pd*(ivert-1)+ipd-1       !-PRE mxpie should be here
            CALL polint_fg(xpol,ypol,pd_mesh+1,xi,yi,dyi)
            rnew(ipsi)=yi
            roldfe(ipsi)=rold(ivert-1)+
     $                   (rold(ivert)-rold(ivert-1))*REAL(ipd)/pd
          ENDDO
        ENDDO
      ENDIF
c-----------------------------------------------------------------------
c     Diagnostics - use drawpack.in file
c-----------------------------------------------------------------------
      IF(diagnose)THEN
        CALL open_bin(binary_unit,'packr.bin','UNKNOWN','REWIND',32_i4)
        WRITE(binary_unit)REAL(0.,4),REAL(roldfe(0),4),REAL(rnew(0),4),
     $                  REAL((rnew(0) - 0.),4)
        DO ipsi=1,mpsi
          WRITE(binary_unit)REAL(ipsi,4),
     $                  REAL(roldfe(ipsi),4),
     $                  REAL(rnew(ipsi),4),
     $                  REAL((rnew(ipsi) - rnew(ipsi-1)),4)
        ENDDO
        CALL close_bin(binary_unit,'pack.bin')
c-----------------------------------------------------------------------
c     diagnose rnew array.
c-----------------------------------------------------------------------
         OPEN(UNIT=ascii_unit,FILE='psi.txt',STATUS='UNKNOWN')
         WRITE(ascii_unit,2010)mpsi,rlow,rhigh
         WRITE(ascii_unit,2020)
         dpsi=0
         DO ipsi=0,mpsi
            WRITE(ascii_unit,2030)ipsi,rnew(ipsi),dpsi
            dpsi=rnew(ipsi+1)-rnew(ipsi)
         ENDDO
         WRITE(ascii_unit,2020)
         CLOSE(UNIT=ascii_unit)
      ENDIF
 2010 FORMAT(3x,'mpsi',3x,'rlow',4x,'rhigh'//i6,1p,2e11.3)
 2020 FORMAT(/3x,'ipsi',4x,'psi',8x,'dpsi'/)
 2030 FORMAT(i6,1p,2e11.3)
c-----------------------------------------------------------------------
      DEALLOCATE(xgrid,rold,roldfe)
      IF(ALLOCATED(x_node)) DEALLOCATE(x_node,xpol,ypol)
      RETURN
      END SUBROUTINE psigrid
c-----------------------------------------------------------------------
c     subprogram 3. qfind.
c     finds positions of singular values of q.
c     eqgrid = psi_normal, eqq = q on eqgrid
c     If radial_variable=bigr, then we actually return the location of
c      the rational surfaces in terms of R(ray)_normal
c-----------------------------------------------------------------------
      SUBROUTINE qfind(gIn,nfind,qt,eqgrid,eqq)

      TYPE(controlMap), INTENT(IN) :: gIn
      INTEGER(i4), INTENT(IN) :: nfind
      TYPE(q_type), INTENT(OUT) :: qt
      REAL(r8), DIMENSION(0:), INTENT(IN) :: eqgrid, eqq
      TYPE(spline_type), TARGET :: qq
      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file

      INTEGER(i4) :: ipsi,ie,me,i,mnodes,ising,m,dm,it
      INTEGER(i4), PARAMETER :: itmax=200

      REAL(r8) :: b,c,d,dx,x0,x,xmax,
     $     singfac,psifac,dq,psifac0,psifac1
      REAL(r8), DIMENSION(0:30) :: qe,psie
      REAL(r8), DIMENSION(:), POINTER :: psifacs
      REAL(r8), DIMENSION(:,:), POINTER :: sqs
c-----------------------------------------------------------------------
c     Set up spline type
c-----------------------------------------------------------------------
      mnodes=SIZE(eqgrid)-1                             ! 1 b/c 0:mnodes
      CALL spline_alloc(qq,mnodes,1_i4)
      qq%xs = eqgrid
      qq%fs(:,1) = eqq
      CALL spline_fit(qq,"extrap")
c-----------------------------------------------------------------------
c     store left end point.  (e = extremum)
c-----------------------------------------------------------------------
      psifacs => qq%xs
      sqs => qq%fs
      ie=0
      psie(ie)=psifacs(0)
      qe(ie)=sqs(0,1)
c-----------------------------------------------------------------------
c     set up search for extrema of q.
c-----------------------------------------------------------------------
      DO ipsi=0,mnodes-1
         CALL spline_eval(qq,psifacs(ipsi),3_i4)
         xmax=psifacs(ipsi+1)-psifacs(ipsi)
         b=qq%f1(1)
         c=qq%f2(1)
         d=qq%f3(1)
         x0=-c/d
         dx=x0*x0-2*b/d
c-----------------------------------------------------------------------
c     store extremum.
c-----------------------------------------------------------------------
         IF(dx >= 0)THEN
            dx=SQRT(dx)
            DO i=1,2
               x=x0-dx
               IF(x >= 0 .AND. x < xmax)THEN
                  ie=ie+1
                  psie(ie)=psifacs(ipsi)+x
                  CALL spline_eval(qq,psie(ie),0_i4)
                  qe(ie)=qq%f(1)
               ENDIF
               dx=-dx
            ENDDO
         ENDIF
c-----------------------------------------------------------------------
c     complete search and store right end point.
c-----------------------------------------------------------------------
      ENDDO
      ie=ie+1
      psie(ie)=psifacs(mnodes)
      qe(ie)=sqs(mnodes,1)
      me=ie
c-----------------------------------------------------------------------
c     find special q values and initialize qt arrays.
c-----------------------------------------------------------------------
      qt%nn=nfind
      qt%q0=qq%fs(0,1)-qq%fs1(0,1)*qq%xs(0)
      qt%qmin=min(MINVAL(qe(0:me)),qt%q0)
      qt%qmax=qq%fs(mnodes,1)
      qt%qa=qq%fs(mnodes,1)+qq%fs1(mnodes,1)*(1-qq%xs(mnodes))
      qt%msing=0; qt%rsing=0.; qt%qsing=0.; qt%q1sing=0.
c-----------------------------------------------------------------------
c     start loop over extrema to find singular surfaces.
c-----------------------------------------------------------------------
      ising=0
      DO ie=1,me
         m=qt%nn*qe(ie-1)
         dq=qe(ie)-qe(ie-1)
         dm=sign(1._r8,dq)
         IF(dm > 0)m=m+1
c-----------------------------------------------------------------------
c     find singular surfaces by binary search.
c-----------------------------------------------------------------------
         DO
            IF((m-gIn%nn*qe(ie))*dm > 0)EXIT
            it=0
            psifac0=psie(ie-1)
            psifac1=psie(ie)
            DO
               it=it+1
               psifac=(psifac0+psifac1)/2
               CALL spline_eval(qq,psifac,0_i4)
               singfac=(m-qt%nn*qq%f(1))*dm
               IF(singfac > 0)THEN
                  psifac0=psifac
                  psifac=(psifac+psifac1)/2
               ELSE
                  psifac1=psifac
                  psifac=(psifac+psifac0)/2
               ENDIF
               IF(ABS(singfac) <= 1e-12)EXIT
               IF(ABS(psifac-psifac1) <= 1e-10)EXIT
               IF(ABS(psifac-psifac0) <= 1e-10)EXIT
               IF(it > itmax)THEN
                  WRITE(6,*)"Searching between",psie(ie-1),psie(ie)
                  CALL fg_stop("qfind can't find root")
               ENDIF
            ENDDO
c-----------------------------------------------------------------------
c     store singular surfaces.
c-----------------------------------------------------------------------
            ising=ising+1
            CALL spline_eval(qq,psifac,1_i4)
            qt%msing(ising)=m
            qt%qsing(ising)=REAL(m,r8)/qt%nn
            qt%q1sing(ising)=qq%f1(1)
            qt%rsing(ising)=psifac
            IF (gIn%radial_variable=="bigr") THEN
              CALL spline_eval(rvss,psifac,0_i4)
              qt%rsing(ising)=rvss%f(1)
            ENDIF
            m=m+dm
         ENDDO
      ENDDO
      qt%nsing=ising
      CALL spline_dealloc(qq)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE qfind
c-----------------------------------------------------------------------
c     subprogram 7. drpack.
c     Packs based on specifying the desired dr directly.
c     Note that if radial_variable=bigr then rsing is actually R_sing
c-----------------------------------------------------------------------
      SUBROUTINE drpack(gIn,qt,xgrid)

      TYPE(controlMap), INTENT(IN) :: gIn
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      TYPE(q_type), INTENT(IN) :: qt
      REAL(r8), DIMENSION(0:), INTENT(INOUT) :: xgrid

      INTEGER(i4) :: ix, jx, iq, jmax, nsing, nrint_max
      REAL(r8), PARAMETER :: eps=1.e-5
      REAL(r8), DIMENSION(qt%nsing+1) :: rpack
      REAL(r8) :: rexp, rext, dr0, drint, factor
      REAL(r8), DIMENSION(0:mpsiv) :: rix
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rjx,rint
      CHARACTER(64) :: msg
      LOGICAL, PARAMETER :: diagnose=.FALSE.
c-----------------------------------------------------------------------
c     Set up some key arrays
c-----------------------------------------------------------------------
      nrint_max=1./eps
      ALLOCATE(rint(0:nrint_max)); rint(:)=0.
      dr0 = 1./REAL(mpsiv,r8)                            ! equispaced dr
      rix=(/(REAL(ix,r8),ix=0,mpsiv)/)*dr0
c-----------------------------------------------------------------------
c     count singular surfaces for packing.
c-----------------------------------------------------------------------
      nsing=0
      DO iq=1,qt%nsing
         IF(qt%qsing(iq) >= gIn%qsmin  .AND. qt%qsing(iq) <= gIn%qsmax
     &   .AND. qt%rsing(iq) >  rlow   .AND. qt%rsing(iq) <  rhigh ) THEN
              nsing=nsing+1
              rpack(nsing)=qt%rsing(iq)
         ENDIF
      ENDDO
      IF (gIn%vac_pack) THEN
         nsing=nsing+1
         rpack(nsing)=1.-eps
      ENDIF
      IF(nsing == 0)THEN
       WRITE(6,*) "No psi packing: no singular surfaces in range"
       RETURN
      ENDIF
c-----------------------------------------------------------------------
c     Pack.  To make dr vs. j look like Gaussians, multiply by a shaping
c       Gaussian shaping function.  This will cause the new r (rr) to be
c       on a larger grid (size of jmax).
c     Because in integrating these shape functions we aren't guaranteed
c      to end exactly on 1, integrate to figure out where we end
c-----------------------------------------------------------------------
      rint(0)=0.
c      rint(0)=xgrid(0)
      drint=dr0
      drint_loop: DO 
        factor = 1._r8
        jmax=0
        pack_loop: DO jx=1,nrint_max
          rext = rint(jx-1) + drint*factor                    ! Extrapolate r
          factor = 1._r8
          DO iq=1,nsing
            rexp = (rext - rpack(iq))/gIn%wpack
            factor = factor*(1.-(1. - 1./gIn%amp)*exp(-2.*rexp**2) )
          ENDDO
          rint(jx) = rint(jx-1) + drint*factor
          IF (rint(jx) >= xgrid(mpsiv)) THEN
             jmax = jx
             EXIT pack_loop
          ENDIF
        ENDDO pack_loop
        IF (rint(jx) <= xgrid(mpsiv)+eps) EXIT drint_loop
        IF (jmax == 0) CALL fg_stop("ERRORS: Try decreasing packing")
        drint=drint/(1.+0.001/eps*(rint(jmax)-xgrid(mpsiv)))
      ENDDO drint_loop
      rint(:)=xgrid(mpsiv)*rint(:)/rint(jmax)
      drint = 1./REAL(mpsiv)
      ALLOCATE(rjx(0:jmax))
      rjx=(/(REAL(jx),jx=0,jmax)/)/REAL(jmax)
c-----------------------------------------------------------------------
c     Diagnostics
c-----------------------------------------------------------------------
      IF (diagnose) THEN
        CALL open_bin(binary_unit,'rr.bin','UNKNOWN','REWIND',32_i4)
        DO jx=0,jmax
          WRITE(binary_unit)REAL(jx,4),
     $                    REAL(rjx(jx),4),
     $                    REAL(rint(jx),4)
        ENDDO
        CALL close_bin(binary_unit,'rr.bin')
      ENDIF
c-----------------------------------------------------------------------
c     Need to remap to be on the same size grid as the original.
c-----------------------------------------------------------------------
      newi: DO ix=0,mpsiv
        DO jx=1,jmax
          IF ((xgrid(ix)-rjx(jx-1))*(xgrid(ix)-rjx(jx))<=0.) THEN
            xgrid(ix)=rint(jx-1)
     $        +(xgrid(ix)-rjx(jx-1))*(rint(jx)-rint(jx-1))/
     $                             (rjx(jx)-rjx(jx-1))
            CYCLE newi
          ENDIF
        ENDDO
        ! Handle case of extreme packing where last cell much bigger
        ! than last unpacked cell
        IF (ix==mpsiv+1) CYCLE newi
        WRITE(msg,'(a,i3)') "fluxgrid: not finding new coordinate ",ix
        CALL fg_stop(msg)
      ENDDO newi
c-----------------------------------------------------------------------
      DEALLOCATE(rjx,rint)
      RETURN
      END SUBROUTINE drpack
c-----------------------------------------------------------------------
c     subprogram 8. taugrid.
c     defines the poloidal grid (taus).
c-----------------------------------------------------------------------
      SUBROUTINE taugrid(gIn,thetas,thpack,taus,mnode,pd,pd_mesh)
      USE fg_physdat

      TYPE(controlMap), INTENT(IN) :: gIn
      INTEGER(i4), INTENT(IN) :: mnode, pd, pd_mesh
      REAL(r8), DIMENSION(0:), INTENT(IN)  :: thetas,thpack
      REAL(r8), DIMENSION(0:), INTENT(OUT) :: taus

      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      INTEGER(i4) :: itau,ipd,ivert,mthetav,itrp
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie,pd_fg
      REAL(r8) :: xi,yi,dyi,dtau
      REAL(r8), DIMENSION(:), ALLOCATABLE :: x_node,xpol,ypol
      REAL(r8), DIMENSION(:), ALLOCATABLE:: tausv
      LOGICAL, PARAMETER :: out=.FALSE.
      TYPE(spline_type) :: thgrid
c-----------------------------------------------------------------------
c     INTERFACE for polynomial interpolation and for getting nodes
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE poly_nodes_fg(n,x)
        USE fg_local
        IMPLICIT NONE
        INTEGER(i4), INTENT(IN) :: n
        REAL(r8), DIMENSION(0:n), INTENT(OUT) :: x
        END SUBROUTINE poly_nodes_fg
      END INTERFACE
c-----------------------------------------------------------------------
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
      pd_fg=gIn%pd
c-----------------------------------------------------------------------
c     Default is equally-spaced theta
c-----------------------------------------------------------------------
      ALLOCATE(x_node(0:pd))
      IF(pd==1) THEN
         taus=twopi*(/(itau,itau=0,mtheta)/)/DFLOAT(mtheta)
      ELSE
        CALL poly_nodes_fg(pd,x_node)
        taus(0)=0.
        DO ivert=1,mtheta/pd
          DO ipd=1,pd
           itau=pd*(ivert-1)+ipd
           taus(itau)=pd*(ivert-1.+x_node(ipd))*twopi/REAL(mtheta)
          ENDDO
        ENDDO
      ENDIF
c-----------------------------------------------------------------------
c     Alter taus if we want to adapt
c-----------------------------------------------------------------------
      IF(gIn%angle_pack) THEN
         mthetav=mtheta/pd*pd_mesh
         ALLOCATE(tausv(0:mthetav))
         tausv=(/(itau,itau=0,mthetav)/)*twopi/mthetav

         CALL spline_alloc(thgrid,mnode,1_i4)
         thgrid%xs=thpack(0:mnode)
         thgrid%fs(:,1)=thetas(0:mnode)
         CALL spline_fit(thgrid,"extrap")

         DO itau=0,mthetav
            CALL spline_eval(thgrid,tausv(itau),0_i4)
            tausv(itau)=thgrid%f(1)
         ENDDO
         CALL spline_dealloc(thgrid)

         mthetav=mtheta/pd
         IF (pd==1) THEN
           taus=tausv
         ELSE
           ALLOCATE(xpol(0:pd_mesh),ypol(0:pd_mesh))
           xpol=(/(REAL(ipd),ipd=0,pd_mesh)/)/REAL(pd_mesh)
           CALL poly_nodes_fg(pd,x_node)
           taus(0)=tausv(0)
           DO ivert=1,mthetav
             itrp=ivert*pd_mesh
             ypol=tausv(itrp-pd_mesh:itrp)
             DO ipd=1,pd
               xi=x_node(ipd)
               itau=pd*(ivert-1)+ipd
               CALL polint_fg(xpol,ypol,pd_mesh+1,xi,yi,dyi)
               taus(itau)=yi
             ENDDO
           ENDDO
           DEALLOCATE(xpol,ypol)
         ENDIF
         DEALLOCATE(tausv)
      ENDIF
      DEALLOCATE(x_node)
c-----------------------------------------------------------------------
c     diagnose taus array.
c-----------------------------------------------------------------------
      IF(out)THEN
         OPEN(UNIT=ascii_unit,FILE='thetas.txt',STATUS='UNKNOWN')
         DO itau=0,mnode
            WRITE(ascii_unit,*)itau,thpack(itau),thetas(itau)
         ENDDO
         CLOSE(UNIT=ascii_unit)
         OPEN(UNIT=ascii_unit,FILE='taus.txt',STATUS='UNKNOWN')
         WRITE(ascii_unit,2010)
         dtau=0
         DO itau=0,mtheta
            WRITE(ascii_unit,2020)itau,taus(itau),dtau
            dtau=taus(itau+1)-taus(itau)
         ENDDO
         WRITE(ascii_unit,2010)
         CLOSE(UNIT=ascii_unit)
      ENDIF
c-----------------------------------------------------------------------
c     WRITE formats.
c-----------------------------------------------------------------------
 2010 FORMAT(/3x,'itau',4x,'tau',8x,'dtau'/)
 2020 FORMAT(i6,1p,2e11.3)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE taugrid
c-----------------------------------------------------------------------
c     subprogram 7. vac_grid.
c     Generates the R,Z grid for the region between the boundary 
c     defined by psihigh and the boundary defined by extr or d3d wall
c-----------------------------------------------------------------------
      SUBROUTINE vacgrid(gIn,mEq,vgrid,rmin,rmax,zmin,zmax,pd,pd_mesh)
      USE fgMapEqTypes
      IMPLICIT NONE

      TYPE(mappedEq), INTENT(INOUT) :: mEq
      TYPE(controlMap), INTENT(IN) :: gIn
      REAL(r8), DIMENSION(:,0:,0:), INTENT(OUT) :: vgrid
      REAL(r8), INTENT(IN) :: rmin,rmax,zmin,zmax
      INTEGER(i4), INTENT(IN) :: pd, pd_mesh

      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      INTEGER(i4) :: ivac, itheta
      REAL(r8) :: rcntr,zcntr,rob,zob,rnew,znew,rnext,znext,r1,z1,r2,z2
      REAL(r8) :: slope, ds, crossprod, small,angle,dsep,dr0,dr1,drsep
      REAL(r8) :: r3,z3
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: rmg, zmg
      REAL(r8), DIMENSION(:), ALLOCATABLE :: vxgrid
      INTEGER(i4) :: mvac,mpsi,mtheta,mxpie,pd_fg
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
      pd_fg=gIn%pd
c-----------------------------------------------------------------------
c     Inner boundary of vacuum is outer boundary of plasma
c     If using wall, need to get outer boundary, otherwise it's known
c     PRE- Actually I should do conformal and self-similar walls in the
c     boundary so I know if they'll fit or not.   Not worth fixing now.
c-----------------------------------------------------------------------
      vgrid(1,:,0)=mEq%ob%fs(:,3); vgrid(2,:,0)=mEq%ob%fs(:,4)
      CALL vac_boundary(gIn,mEq,vgrid(1,:,mvac),vgrid(2,:,mvac),
     &                  rmin,rmax,zmin,zmax)
c-----------------------------------------------------------------------
c     Set up grid that controls the "radial" direction.
c-----------------------------------------------------------------------
      ALLOCATE(vxgrid(0:mvac))
      vxgrid(0)=mEq%ob%fs(0,3)                              ! R_ob
      vxgrid(mvac)=vgrid(1,0,mvac)
      dr0=mEq%twod%fs(1,0,mpsi)-mEq%twod%fs(1,0,mpsi-pd_fg+pd_mesh-1)
      dr1=mEq%twod%fs(1,0,mpsi-pd_fg+pd_mesh-1)
     &    -mEq%twod%fs(1,0,mpsi-pd_fg+pd_mesh)
      drsep=2*dr0-dr1
      !drsep=dr0
      dsep=vxgrid(mvac)-vxgrid(0)
      CALL vac_radial_grid(gIn,vxgrid,pd,pd_mesh,dsep,dr0)
c-----------------------------------------------------------------------
c     Now set up the full mesh depending on the type.
c-----------------------------------------------------------------------
      SELECT CASE(gIn%extr_type)
c-----------------------------------------------------------------------
      CASE("conformal")
c-----------------------------------------------------------------------
        small =1.e-5
        rcntr=0.5*(mEq%rs2+mEq%rs1)
        DO ivac=1,mvac
          ds=vxgrid(ivac)-vxgrid(0)
          DO itheta=0,mtheta
            rob= mEq%ob%fs(itheta,3);    zob= mEq%ob%fs(itheta,4)
c                                                       R_ob,Z_ob ordered
            IF (itheta == mtheta) THEN
              rnext=mEq%ob%fs(1,3) - rob;      znext=mEq%ob%fs(1,4)-zob
            ELSE
              rnext=mEq%ob%fs(itheta+1,3)-rob
              znext=mEq%ob%fs(itheta+1,4)-zob
            ENDIF
c                                                      Slope defines perp
            IF (ABS(mEq%ob%fs1(itheta,3)) < small) THEN
               slope=0.1*HUGE(0)*mEq%ob%fs1(itheta,3)/
     &                         ABS(mEq%ob%fs1(itheta,3))
            ELSE
               slope=mEq%ob%fs1(itheta,4)/mEq%ob%fs1(itheta,3)  !dZ/dth/dR/dth
            ENDIF
c                                                        SQRT can be +or-
            znew=ds/SQRT(1.+slope**2)
            rnew=-slope*znew
c                                                        curl => sign
            crossprod=rnew*znext-znew*rnext
            IF (crossprod < 0.) THEN
                rnew=-rnew+rob; znew=-znew+zob
            ELSE
                rnew= rnew+rob; znew= znew+zob
            ENDIF
            vgrid(1,itheta,ivac)=rnew; vgrid(2,itheta,ivac)=znew
          ENDDO
        ENDDO
c-----------------------------------------------------------------------
      CASE("self_similar")
c-----------------------------------------------------------------------
        IF (gIn%extr_center == 'geom') THEN
           rcntr=0.5*(mEq%rs2+mEq%rs1);  zcntr=0.
        ELSEIF (gIn%extr_center == 'mag_axis') THEN
           rcntr=mEq%ro;  zcntr=mEq%zo
        ENDIF
        DO ivac=1,mvac
          ds=1.+(gIn%extr-1.)*
     &       (vxgrid(ivac)-vxgrid(0))/(vxgrid(mvac)-vxgrid(0))
          DO itheta=0,mtheta
            rob= mEq%ob%fs(itheta,3);   zob= mEq%ob%fs(itheta,4)

            rnew= rcntr + ds*(rob-rcntr)
            znew= zcntr + ds*(zob-zcntr)

            vgrid(1,itheta,ivac)=rnew; vgrid(2,itheta,ivac)=znew
          ENDDO
        ENDDO
c-----------------------------------------------------------------------
      CASE("wall")
c-----------------------------------------------------------------------
c       Each ray has a vxgrid setup such that dr0 is continuous
c-----------------------------------------------------------------------
        DO itheta=0,mtheta
           r1=mEq%twod%fs(1,itheta,mpsi)
           z1=mEq%twod%fs(2,itheta,mpsi)
           IF (pd_mesh==1) THEN
              r2=mEq%twod%fs(1,itheta,mpsi-pd_fg)
              z2=mEq%twod%fs(2,itheta,mpsi-pd_fg)
              r3=mEq%twod%fs(1,itheta,mpsi-2*pd_fg)
              z3=mEq%twod%fs(2,itheta,mpsi-2*pd_fg)
              dr0=((r2-r1)**2+(z2-z1)**2)**0.5
              dr1=((r3-r2)**2+(z3-z2)**2)**0.5
              drsep=2*dr0-dr1
           ELSE
              !r2=twod%fs(1,itheta,mpsi-1)
              !z2=twod%fs(2,itheta,mpsi-1)
              !r3=twod%fs(1,itheta,mpsi-2)
              !z3=twod%fs(2,itheta,mpsi-2)
              !r2=twod%fs(1,itheta,mpsi-pd_fg+pd_mesh-1)
              !z2=twod%fs(2,itheta,mpsi-pd_fg+pd_mesh-1)
              !r3=twod%fs(1,itheta,mpsi-2*pd_fg+pd_mesh-1)
              !z3=twod%fs(2,itheta,mpsi-2*pd_fg+pd_mesh-1)
              !CURRENT:
              !r2=twod%fs(1,itheta,mpsi-pd_fg+pd_mesh-1)
              !z2=twod%fs(2,itheta,mpsi-pd_fg+pd_mesh-1)
              !r3=twod%fs(1,itheta,mpsi-pd_fg+pd_mesh-2)
              !z3=twod%fs(2,itheta,mpsi-pd_fg+pd_mesh-2)
              dr0=((r2-r1)**2+(z2-z1)**2)**0.5
              dr1=((r3-r2)**2+(z3-z2)**2)**0.5
              drsep=dr0
              !drsep=2*dr0-dr1
           ENDIF
           !WRITE(*,*) 'dr0, dr1, drsep ', dr0, dr1, drsep
           r2=vgrid(1,itheta,mvac)
           z2=vgrid(2,itheta,mvac)
           dsep=((r2-r1)**2+(z2-z1)**2)**0.5

           vxgrid(0)=0.;  vxgrid(mvac)=dsep
           CALL vac_radial_grid(gIn,vxgrid,pd,pd_mesh,dsep,dr0)
           vgrid(1,itheta,:)=r1+vxgrid(:)*(r2-r1)/dsep
           vgrid(2,itheta,:)=z1+vxgrid(:)*(z2-z1)/dsep
        ENDDO
c-----------------------------------------------------------------------
      CASE("wall_fourier")
c-----------------------------------------------------------------------
c       Set up grid of Fourier coefficients (rmg,zmg) and then transform
c       back into real space to get the grid
c-----------------------------------------------------------------------
        ALLOCATE(rmg(0:mtheta,0:mvac),zmg(0:mtheta,0:mvac))
        CALL rz2fourier(mEq%ob%fs(:,3),mEq%ob%fs(:,4),rmg(:,0),zmg(:,0))
        CALL rz2fourier(vgrid(1,:,mvac),vgrid(2,:,mvac),
     &                  rmg(:,mvac),    zmg(:,mvac))

        DO ivac=1,mvac-1
          ds=(vxgrid(ivac)-vxgrid(0))/(vxgrid(mvac)-vxgrid(0))
          rmg(:,ivac)=rmg(:,0) + ds*(rmg(:,mvac)-rmg(:,0))
          zmg(:,ivac)=zmg(:,0) + ds*(zmg(:,mvac)-zmg(:,0))
        ENDDO

        CALL fourier2grid(rmg,zmg,vgrid(1,:,:),vgrid(2,:,:))
        DEALLOCATE(rmg,zmg)
c-----------------------------------------------------------------------
      END SELECT
c-----------------------------------------------------------------------
c     Diagnostics - use drawpack.in file
c-----------------------------------------------------------------------
      CALL open_bin(binary_unit,'pack.bin','UNKNOWN','REWIND',32_i4)
        WRITE(binary_unit)REAL(0.,4),
     $            REAL(mEq%twod%fs(1,0,0),4),REAL(mEq%twod%fs(1,0,0),4),
     $            REAL(mEq%twod%fs(1,0,0)-mEq%ro,4)
      DO ivac=1,mpsi
        WRITE(binary_unit)REAL(ivac,4),
     $      REAL(mEq%twod%fs(1,0,ivac),4),REAL(mEq%twod%fs(1,0,ivac),4),
     $      REAL(mEq%twod%fs(1,0,ivac)-mEq%twod%fs(1,0,ivac-1),4)
      ENDDO
      DO ivac=1,mvac
        WRITE(binary_unit)REAL(mpsi+ivac,4),
     $       REAL(vgrid(1,0,ivac),4),REAL(vgrid(1,0,ivac),4),
     $       REAL(vgrid(1,0,ivac)-vgrid(1,0,ivac-1),4)
      ENDDO
      CALL close_bin(binary_unit,'pack.bin')
c-----------------------------------------------------------------------
      DEALLOCATE(vxgrid)
      RETURN
      END SUBROUTINE vacgrid
c-----------------------------------------------------------------------
c     subprogram 7. vac_radial_grid.
c     Generates the R,Z grid for the region between the boundary 
c     defined by psihigh and the boundary defined by extr or d3d wall
c-----------------------------------------------------------------------
      SUBROUTINE vac_radial_grid(gIn,vacxgrid,pd,pd_mesh,dsep,dr0)
      USE fgMapEqTypes

      TYPE(controlMap), INTENT(IN) :: gIn
      REAL(r8), DIMENSION(0:), INTENT(INOUT) :: vacxgrid
      REAL(r8), INTENT(IN) :: dsep,dr0
      INTEGER(i4), INTENT(IN) :: pd, pd_mesh

      REAL(r8) :: ds, dsnorm,cr
      INTEGER(i4) :: ivac, mvac_vert,ivert,ipd
      INTEGER(i4) :: mvac,mpsi,mtheta
      REAL(r8), DIMENSION(:), ALLOCATABLE :: vrgrid
      INTEGER(i4) :: itrp
      REAL(r8) :: xi,yi,dyi
      REAL(r8), DIMENSION(:), ALLOCATABLE :: x_node,xpol,ypol
c-----------------------------------------------------------------------
c     INTERFACE for polynomial interpolation and for getting nodes
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE poly_nodes_fg(n,x)
        USE fg_local
        IMPLICIT NONE
        INTEGER(i4), INTENT(IN) :: n
        REAL(r8), DIMENSION(0:n), INTENT(OUT) :: x
        END SUBROUTINE poly_nodes_fg
      END INTERFACE
c-----------------------------------------------------------------------
c     Set up stuff for grid with higher_order elements
c-----------------------------------------------------------------------
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta
      mvac_vert=mvac/pd*pd_mesh 
      ALLOCATE(vrgrid(0:mvac_vert))
      vrgrid(0)=vacxgrid(0)
      vrgrid(mvac_vert)=vacxgrid(mvac)
c-----------------------------------------------------------------------
c     Set up grid such that Delta(R) along Z=zo is continuous, i.e., 
c      Delta(R)_inside_sep=Delta(R)_outside_sep
c     After that, the grid is based on a geometric progression:
c       dr(i+1)/dr(i) = constant ratio = cr
c     To fit to GA wall, let distance be determined by distance to wall
c-----------------------------------------------------------------------
      cr=geom_ratio(dsep,dr0,mvac_vert-1)

      ds=0
      DO ivac=1,mvac_vert
        ds=ds + dr0*cr**(ivac-1)
        dsnorm=ds/dsep
        vrgrid(ivac)=vrgrid(0) + dsnorm*dsep
      ENDDO
      IF (gIn%extr_type == 'wall') vrgrid(mvac_vert)=vacxgrid(mvac)
c-----------------------------------------------------------------------
c     Do the special mojo for handling high-order grids
c-----------------------------------------------------------------------
      IF (mvac_vert==mvac) THEN
        vacxgrid=vrgrid
      ELSE
        mvac_vert=mvac/pd
        ALLOCATE(x_node(0:pd))
        CALL poly_nodes_fg(pd,x_node)
        vacxgrid(0)=vrgrid(0)
        ALLOCATE(xpol(0:pd_mesh),ypol(0:pd_mesh))
        xpol=(/(REAL(ipd),ipd=0,pd_mesh)/)/REAL(pd_mesh)
        CALL poly_nodes_fg(pd,x_node)
        DO ivert=1,mvac_vert
          itrp=ivert*pd_mesh
          ypol=vrgrid(itrp-pd_mesh:itrp)
          DO ipd=1,pd
            !xi=pd*(ivert-1)+ipd
            xi=x_node(ipd)
            ivac=pd*(ivert-1)+ipd
            CALL polint_fg(xpol,ypol,pd_mesh+1,xi,yi,dyi)
            vacxgrid(ivac)=yi
          ENDDO
        ENDDO
        DEALLOCATE(x_node,xpol,ypol)
      ENDIF
c-----------------------------------------------------------------------
      DEALLOCATE(vrgrid)
      RETURN
      END SUBROUTINE vac_radial_grid
c-----------------------------------------------------------------------
c     subprogram 7. vac_boundary.
c     This calculates the outer boundary of the vacuum region.
c     For walls, we need to find where lines defined by the points
c     (R_m, Z_m) and (R_ob,Z_ob) intersect the wall (using 
c     Fourier spectral version of wall.
c-----------------------------------------------------------------------
      SUBROUTINE vac_boundary(gIn,mEq,rwall, zwall,rmin,rmax,zmin,zmax)
      USE fgMapEqTypes
      IMPLICIT NONE

      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq
      REAL(r8), DIMENSION(0:), INTENT(OUT) :: rwall, zwall
      REAL(r8), INTENT(IN) :: rmin,rmax,zmin,zmax

      REAL(r8), DIMENSION(:), ALLOCATABLE :: rfw, zfw
      REAL(r8) :: rp,zp, denom, ra,za,rb,zb,r1,z1,r2,z2, dra,dr1,dza,dz1
      REAL(r8) :: rnew,znew,rob,zob,rnext,znext,small,rcntr,acr,dsep
      REAL(r8) :: slope,crossprod,zcntr
      INTEGER(i4) :: incr,itheta, nfw, here,prev,lap
      LOGICAL :: outside_box=.FALSE.
      INTEGER(i4) :: mvac,mpsi,mtheta
c-----------------------------------------------------------------------
c     Boundary depends on extr_type
c-----------------------------------------------------------------------
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta
      SELECT CASE(gIn%extr_type)
c-----------------------------------------------------------------------
c     Conformal wall has the same shape as the outer boundary.
c-----------------------------------------------------------------------
      CASE("conformal")
        small =1.e-5
        rcntr=0.5*(mEq%rs2+mEq%rs1)
        acr=mEq%ob%fs(0,3)-rcntr                                  ! a
        dsep=(gIn%extr-1.)*acr                                    ! b-a
        DO itheta=0,mtheta
          rob= mEq%ob%fs(itheta,3)
          zob= mEq%ob%fs(itheta,4)
c                                                       R_ob,Z_ob ordered
          IF (itheta == mtheta) THEN
            rnext=mEq%ob%fs(1,3) - rob
            znext=mEq%ob%fs(1,4) - zob
          ELSE
            rnext=mEq%ob%fs(itheta+1,3) - rob
            znext=mEq%ob%fs(itheta+1,4) - zob
          ENDIF
c                                                      Slope defines perp
          IF (ABS(mEq%ob%fs1(itheta,3)) < small) THEN
             slope=0.1*HUGE(0)*mEq%ob%fs1(itheta,3)/
     &                         ABS(mEq%ob%fs1(itheta,3))
          ELSE
             slope=mEq%ob%fs1(itheta,4)/mEq%ob%fs1(itheta,3)  !dZ/dth/dR/dth
          ENDIF
c                                                        SQRT can be +or-
          znew=dsep/SQRT(1.+slope**2)
          rnew=-slope*znew
c                                                        curl => sign
          crossprod=rnew*znext-znew*rnext
          IF (crossprod < 0.) THEN
              rnew=-rnew+rob; znew=-znew+zob
          ELSE
              rnew=rnew+rob; znew=znew+zob
          ENDIF
c                                                        Stay in domain
          IF(rnew > rmax .OR. rnew < rmin) outside_box=.TRUE.
          IF(znew > zmax .OR. znew < zmin) outside_box=.TRUE.
          IF (outside_box) THEN
           WRITE(6,*) 'Vacuum boundary has gone outside of'
           WRITE(6,*) 'equilibrium domain.'
           CALL fg_stop('Please decrease extr')
          ENDIF
          rwall(itheta)=rnew
          zwall(itheta)=znew
        ENDDO

c-----------------------------------------------------------------------
c     Self-similar wall just extrapolates the distance between center
c     and outer boundary by a fixed amount
c-----------------------------------------------------------------------
      CASE("self_similar")
        IF (gIn%extr_center == 'geom') THEN
           rcntr=0.5*(mEq%rs2+mEq%rs1);  zcntr=0.
        ELSEIF (gIn%extr_center == 'mag_axis') THEN
           rcntr=mEq%ro;  zcntr=mEq%zo
        ENDIF
        DO itheta=0,mtheta
          rob= mEq%ob%fs(itheta,3);   zob= mEq%ob%fs(itheta,4)

          rnew= rcntr + gIn%extr*(rob-rcntr)
          znew= zcntr + gIn%extr*(zob-zcntr)
c                                                        Stay in domain
          IF(rnew > rmax .OR. rnew < rmin) outside_box=.TRUE.
          IF(znew > zmax .OR. znew < zmin) outside_box=.TRUE.
          IF (outside_box) THEN
           WRITE(6,*) 'Vacuum boundary has gone outside of'
           WRITE(6,*) 'equilibrium domain.'
           CALL fg_stop('Please decrease extr')
          ENDIF
          rwall(itheta)=rnew
          zwall(itheta)=znew
        ENDDO
c-----------------------------------------------------------------------
c     Get the wall with 3x the number of points I need to make 
c      interpolation robust.  The get_wall uses Fourier series to do so.
c-----------------------------------------------------------------------
      CASE("wall","wall_fourier")
      nfw=3*mtheta
      ALLOCATE(rfw(nfw),zfw(nfw))
      CALL get_wall(gIn,rfw,zfw)
c-----------------------------------------------------------------------
c     Find intersection (rp,zp) of ray (ra,za & rb,zb --line a) 
c     with wall line segment (r1,z1 & r2,z2 --line 1)
c     Complicated formulas, but straightforward to derive
c     Search algorithm is also complicated by the fact that zfw starts
c      at Z=0 whereas mEq%ob%fs(:4) starts at zo which may be above
c      or below Z=0.  Start 3/4 of the way through the outer boundary
c      and continue 1/4 the way through.
c-----------------------------------------------------------------------
      incr=INT(0.75*nfw);   here=incr;   prev=incr-1;  lap=0
      ra=mEq%ro; za=mEq%zo
      DO itheta=0,mtheta
          rb=mEq%ob%fs(itheta,3);  zb=mEq%ob%fs(itheta,4)
          dra=rb-ra;           dza=zb-za
          DO
             r1=rfw(prev);      z1=zfw(prev)
             r2=rfw(here);      z2=zfw(here)
             dr1=r2-r1;          dz1=z2-z1
             denom=dz1*dra-dza*dr1
             rp=((za-z1)*dr1*dra - dza*dr1*ra + dz1*dra*r1)/denom
             zp=((r1-ra)*dza*dz1 + dz1*dra*za - dza*dr1*z1)/denom
             IF( (r1-rp)*(r2-rp)<=0 .AND. (z1-zp)*(z2-zp)<=0 ) THEN
               rwall(itheta)=rp;  zwall(itheta)=zp 
               EXIT
             ELSE
               incr=incr+1;   here=incr;   prev=incr-1
               IF(incr==nfw+1) THEN
                  incr=2; here=incr; lap=lap+1
               ENDIF
               IF (lap == 3) CALL fg_stop("Problems finding wall")
             ENDIF
          ENDDO
      ENDDO
      DEALLOCATE(rfw,zfw)
c-----------------------------------------------------------------------
c     Try to create an outer boundary that is UEDGE/BOUT like
c-----------------------------------------------------------------------
      CASE("edge")
c         CALL get_edge_boundary
c-----------------------------------------------------------------------
      CASE default
           CALL fg_stop("extr_type choice not recognized")
      END SELECT
c-----------------------------------------------------------------------
c     Diagnostics
c-----------------------------------------------------------------------
c      OPEN(UNIT=77,FILE="plotwall.dat",STATUS="UNKNOWN")
c      WRITE(77,*) 'VARIABLES = "R", "Z"'
c      WRITE(77,*) 'ZONE ',',i=',SIZE(rwall)
c      DO itheta=0,SIZE(rwall)-1
c            WRITE(77,*) rwall(itheta), zwall(itheta)
c      ENDDO 
c      CLOSE(77)
      END SUBROUTINE vac_boundary
 
c-----------------------------------------------------------------------
c     subprogram 7. get_wall.
c     Defines the shape of the wall based on a Fourier spectral 
c       decomposition of "actual" wall.
c     Procedure:
c      1) get (R,Z) on smaller grid (either hardwired or file) (rws,zws)
c      2) get Fourier components of smaller grid
c      3) Fourier transform into real space based on larger grid
c-----------------------------------------------------------------------
      SUBROUTINE get_wall(gIn, xwall, ywall)

      TYPE(controlMap) :: gIn
      REAL(r8), DIMENSION(:), INTENT(OUT) :: xwall, ywall

      INTEGER(i4) :: nwall, k, nws
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rws,zws, rbin,zbin, rmg,zmg
      LOGICAL :: file_exist

      INTEGER(i4), PARAMETER :: nd3d=51
      INTEGER(i4), PARAMETER :: niter=52
      INTEGER(i4), PARAMETER :: nd3d_lim=95
      REAL(r8), DIMENSION(nd3d) :: rd3d, zd3d
      REAL(r8), DIMENSION(nd3d_lim) :: rd3dl, zd3dl
      REAL(r8), DIMENSION(niter) :: riter, ziter
      DATA (rd3d(k),k=1,nd3d)
     $     /2.443303419949772, 2.445003360252620, 2.435206281190796,
     &      2.397229070763543, 2.349096435450708, 2.296697511366329,
     &      2.226202284287925, 2.132775562815938, 2.027583832063605,
     &      1.910630360626907, 1.778445679008138, 1.635079922339590,
     &      1.484330005656411, 1.324367385828326, 1.162101456042379,
     &      1.027709774120046, 0.9531004737753517, 0.9291925921464999,
     &      0.9306128757867301, 0.9373734199854492, 0.9374716427460326,
     &      0.9309174125983126, 0.9309062023801610, 0.9330912388264760,
     &      0.9290759363555934, 0.9313924528732267, 0.9325192643910063,
     &      0.9298801313664510, 0.9355275529633224, 0.9383072037111380,
     &      0.9315511559488159, 0.9286559025958544, 0.9465601634341064,
     &      1.000382927938164, 1.092264926894742, 1.215568147385899,
     &      1.343869169462016, 1.443290163237878, 1.545839461727406,
     &      1.659387736523215, 1.785869422676203, 1.919004656822144,
     &      2.048477467998743, 2.170484269722559, 2.273692536052303,
     &      2.345258986779728, 2.405126351903085, 2.441499638224660,
     &      2.444671091311594, 2.441550619712000, 2.443303419949772/
      DATA (zd3d(k),k=1,nd3d)
     &     /0.1070010083064705, 0.2407488482372501, 0.3804810616106032,
     &      0.5159387361983736, 0.6503946165564584, 0.7935848357649634,
     &      0.9352181481113857, 1.059155770383247, 1.178561334034986,
     &      1.293970423413222, 1.384524187526246, 1.432284507191533,
     &      1.453445898862813, 1.463657741995547, 1.445934022958783,
     &      1.359242878727507, 1.195323634649968, 1.004001380805289,
     &      0.8237611508094629, 0.6662857938612209, 0.5291230710159969,
     &      0.4026518905384536, 0.2771781125246763, 0.1533390886307173,
     &      2.974376768179259E-02, -9.479066317849494E-02,
     &      -0.2219425844279092, -0.3553396294163588,
     &      -0.4901821828518301, -0.6351465365923717,
     &      -0.8027612626835731, -0.9874117999240944,
     &      -1.170409819654592, -1.321107993701108,
     &      -1.416381737427392, -1.457845801192058,
     &      -1.463452619441855, -1.457140387484942,
     &      -1.446593354617252, -1.427322714872727,
     &      -1.380546621923976, -1.286917884397917,
     &      -1.155789433396968, -1.012819480748024,
     &      -0.8474364195572380, -0.6613050759653780,
     &      -0.4935139420070444, -0.3285717485346793,
     &    -0.1676609207868461,-2.517907021385858E-02,0.1070010083064715/
      DATA (rd3dl(k),k=1,nd3d_lim)
     &     /2.377000, 2.374100, 2.371200,
     &      2.368186, 2.364302, 2.359407,
     &      2.353502, 2.346586, 2.338625,
     &      2.329208, 2.318236, 2.305708,
     &      2.291625, 2.276023, 2.258886,
     &      2.240174, 2.219886, 2.198023,
     &      2.175068, 2.153992, 2.135301,
     &      2.118992, 2.105068, 2.090241,
     &      2.052995, 1.989447, 1.899595,
     &      1.783441, 1.647673, 1.532503,
     &      1.444646, 1.384103, 1.350873,
     &      1.341268, 1.335259, 1.329856,
     &      1.325059, 1.320868, 1.317145,
     &      1.313073, 1.308515, 1.303473,
     &      1.297945, 1.290936, 1.275718,
     &      1.251045, 1.216918, 1.173336,
     &      1.122800, 1.080400, 1.048667,
     &      1.027600, 1.017200, 1.016000,
     &      1.016000, 1.016000, 1.016000,
     &      1.018629, 1.026794, 1.040494,
     &      1.059729, 1.084500, 1.111574,
     &      1.137961, 1.163661, 1.188674,
     &      1.213000, 1.237518, 1.263127,
     &      1.289827, 1.317618, 1.346500,
     &      1.379758, 1.421136, 1.470636,
     &      1.528258, 1.594000, 1.663945,
     &      1.734618, 1.806018, 1.878145,
     &      1.951000, 2.021561, 2.086567,
     &      2.146017, 2.199911, 2.248250,
     &      2.289843, 2.322789, 2.347089,
     &      2.362743, 2.369750, 2.372650,
     &      2.375550,2.377000/
      DATA (zd3dl(k),k=1,nd3d_lim)
     &     /0.133000, 0.186400, 0.239800,
     &      0.292527, 0.340097, 0.381687,
     &      0.417297, 0.446927, 0.471723,
     &      0.498320, 0.527785, 0.560120,
     &      0.595323, 0.633427, 0.675130,
     &      0.720631, 0.769930, 0.823027,
     &      0.878323, 0.925786, 0.963674,
     &      0.991986, 1.010723, 1.022386,
     &      1.042068, 1.072295, 1.113068,
     &      1.164386, 1.223400, 1.272867,
     &      1.309889, 1.334467, 1.346600,
     &      1.348000, 1.348000, 1.348000,
     &      1.348000, 1.348000, 1.348000,
     &      1.348000, 1.348000, 1.348000,
     &      1.348000, 1.347086, 1.339168,
     &      1.323129, 1.298968, 1.266686,
     &      1.227905, 1.189370, 1.151417,
     &      1.111923, 1.069494, 0.890874,
     &      0.606510, 0.247187, -0.185545,
     &      -0.632136, -0.931313, -1.128879,
     &      -1.249823, -1.293000, -1.318313,
     &      -1.337970, -1.351970, -1.360313,
     &      -1.363000, -1.363000, -1.363000,
     &      -1.363000, -1.363000, -1.363000,
     &      -1.360831, -1.354097, -1.342797,
     &      -1.326931, -1.306500, -1.280753,
     &      -1.248379, -1.209379, -1.163753,
     &      -1.111500, -1.050419, -0.977379,
     &      -0.892379, -0.795419, -0.686500,
     &      -0.577773, -0.481409, -0.397409,
     &      -0.325773, -0.266500, -0.213100,
     &      -0.159700,0.133000/
      DATA (riter(k),k=1,niter)
     &     /8.261495227854306, 8.274659875271938, 8.128660812267146,
     &      7.827429826396759, 7.526171234142119, 7.205082926276565,
     &      6.880311748720418, 6.584537727750065, 6.299053062161763,
     &      6.027764194695722, 5.768869592898587, 5.529401175829742,
     &      5.315757416498469, 5.127379337415737, 4.929547011752678,
     &      4.742346819598767, 4.575165042974717, 4.430855773264750,
     &      4.295571501607868, 4.148534794637162, 4.068328101486016,
     &      4.058935354640545, 4.057070080417800, 4.051862274751929,
     &      4.047375445663540, 4.051483122951774, 4.050946622230709,
     &      4.084883865927435, 4.184350759297605, 4.269249661215698,
     &      4.336445569207013, 4.425945089754215, 4.518935261093658,
     &      4.660900245562150, 4.850404761206950, 5.012344277070412,
     &      5.130227077393367, 5.265817488122284, 5.437277733065149,
     &      5.640833157219395, 5.847412076641922, 6.043640081177417,
     &      6.262156988934132, 6.507915778824459, 6.766109191289588,
     &      7.031618797949846, 7.343857723175982, 7.655309868586396,
     &      7.918817723549097, 8.169139054420796, 8.279456256270812,
     &      8.261495227854306/
      DATA (ziter(k),k=1,niter)
     &     /0.6120738847734692, 1.294282013417222, 1.967049485220385,
     &      2.506961693230795, 2.974458302894581, 3.361699241644254,
     &      3.646108914647129, 3.910138832457473, 4.105777959429736,
     &      4.317710472825907, 4.504267574981021, 4.581874641644053,
     &      4.621193426085777, 4.623208395687827, 4.607211688803997,
     &      4.560507773492360, 4.431882421025191, 4.219688782078997,
     &      3.979537585659474, 3.683863976493400, 3.243305647086647,
     &      2.741303811109093, 2.154583511612502, 1.539336002960783,
     &      0.8517087667955127, 0.1202059178240859, -0.607417852501778,
     &      -1.290355735493282, -1.839739729808598, -2.328530976725061,
     &      -2.777644544496936, -3.150299314788512, -3.472226115355060,
     &      -3.647444477363941, -3.631414341423199, -3.640333110204590,
     &      -3.779831093160790, -3.870646158123883, -3.822200907134530,
     &      -3.598903745551534, -3.337213258910051, -3.257137182934460,
     &      -3.133535518728639, -2.924470753876894, -2.679978289600963,
     &      -2.358729433550962, -2.096252675547865, -1.734150373527225,
     &      -1.247118886666625, -0.7061827716313122, 
     &      -5.488552410586956E-02,0.6120738847734688/
c-----------------------------------------------------------------------
c     Get data points for smaller wall.  Must be somewhat smooth in
c      order to take Fourier coefficients
c-----------------------------------------------------------------------
      IF (gIn%wall_file == 'use_d3d') THEN
         nws=nd3d
         ALLOCATE(rws(nws),zws(nws))
         rws=rd3d;  zws=zd3d
      ELSEIF (gIn%wall_file == 'use_d3d_lim') THEN
         nws=nd3d_lim
         ALLOCATE(rws(nws),zws(nws))
         rws=rd3dl;  zws=zd3dl
      ELSEIF (gIn%wall_file == 'use_iter') THEN
         nws=niter
         ALLOCATE(rws(nws),zws(nws))
         rws=riter;  zws=ziter
      ELSE 
         INQUIRE(FILE=gIn%wall_file,EXIST=file_exist)
         IF(.NOT. file_exist) CALL fg_stop("wall_file does not exit")
         OPEN(UNIT=13,FILE=gIn%wall_file,STATUS='old')
         READ(13,*) nws
         ALLOCATE(rbin(nws),zbin(nws))
         DO k=1,nws
           READ(13,*) rbin(k), zbin(k)
         ENDDO
         CLOSE(13)
c                                   Make sure last point repeats first 
         IF (rbin(1)/=rbin(nws) .OR. zbin(1).NE.zbin(nws)) THEN
           nws=nws+1
           ALLOCATE(rws(nws),zws(nws))
           rws(1:nws-1)=rbin;  zws(1:nws-1)=zbin
           rws(nws)=rws(1);  zws(nws)=zws(1)
         ELSE
           ALLOCATE(rws(nws),zws(nws))
           rws(:)=rbin;  zws(:)=zbin
         ENDIF
         DEALLOCATE(rbin,zbin)
      ENDIF
c-----------------------------------------------------------------------
c     Get Fourier coefficients
c-----------------------------------------------------------------------
      ALLOCATE(rmg(nws),zmg(nws)); rmg=0.; zmg=0.
      CALL rz2fourier(rws,zws,rmg,zmg)
c-----------------------------------------------------------------------
c     Calculate R,Z on larger size
c-----------------------------------------------------------------------
      rws=rmg; zws=zmg                          ! Temporary storage
      DEALLOCATE(rmg,zmg)
      nwall=SIZE(xwall)
      ALLOCATE(rmg(nwall),zmg(nwall)); rmg=0.; zmg=0.
      IF(2*gIn%wall_modes > nws) THEN
        WRITE(6,*) "Not enough data for ",gIn%wall_modes," wall_modes"
        WRITE(6,*) "Using maximum possible"
        rmg(1:nws)=rws
        zmg(1:nws)=zws
      ELSE
        rmg(1:2*gIn%wall_modes)=rws(1:2*gIn%wall_modes)
        zmg(1:2*gIn%wall_modes)=zws(1:2*gIn%wall_modes)
      ENDIF

      CALL fourier2rz(xwall,ywall,rmg,zmg)
c-----------------------------------------------------------------------
      DEALLOCATE(rmg,zmg)
      RETURN
      END SUBROUTINE get_wall
c-----------------------------------------------------------------------
c     subprogram 8. rz2fourier.
c     Get the Fourier coefficients for a series of rz points 
c-----------------------------------------------------------------------
      SUBROUTINE rz2fourier(rin, zin, rm,zm)
      USE rfft_mod

      REAL(r8), DIMENSION(:), INTENT(OUT) :: rm,zm
      REAL(r8), DIMENSION(:), INTENT(IN) :: rin,zin
c-----------------------------------------------------------------------
c     Allocate storage and initialize fourier routines
c-----------------------------------------------------------------------
      nfft = SIZE(rin)
      ALLOCATE(wa(nfft),ch(nfft),c(nfft)); wa = 0.; ch = 0.; c  = 0.
      CALL rffti
c-----------------------------------------------------------------------
c     Get the coefficients
c-----------------------------------------------------------------------
      c = rin
      CALL rfftf       
      rm=c/REAL(nfft,r8)

      c = zin
      CALL rfftf       
      zm=c/REAL(nfft,r8)

      DEALLOCATE(c,wa,ch)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE rz2fourier
c-----------------------------------------------------------------------
c     subprogram 8. fourier2rz.
c     Get R,Z given their fourier coefficients
c-----------------------------------------------------------------------
      SUBROUTINE fourier2rz(rin, zin, rm,zm)
      USE rfft_mod
      IMPLICIT NONE

      REAL(r8), DIMENSION(:), INTENT(IN) :: rm,zm
      REAL(r8), DIMENSION(:), INTENT(OUT) :: rin,zin
c-----------------------------------------------------------------------
c     Allocate storage and initialize fourier routines
c-----------------------------------------------------------------------
      nfft = SIZE(rin)
      ALLOCATE(wa(nfft),ch(nfft),c(nfft)); wa = 0.; ch = 0.; c  = 0.
      CALL rffti
c-----------------------------------------------------------------------
c     Do the conversion
c-----------------------------------------------------------------------
      c = rm
      CALL rfftb
      rin=c

      c = zm
      CALL rfftb
      zin=c

      DEALLOCATE(c,wa,ch)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE fourier2rz
c-----------------------------------------------------------------------
c     subprogram 8. fourier2grid.
c     Given a 2d array of R,Z fourier coefficients, go back to grid.
c-----------------------------------------------------------------------
      SUBROUTINE fourier2grid(rmin, zmin, rout,zout)
      USE rfft_mod
      IMPLICIT NONE

      REAL(r8), DIMENSION(:,:), INTENT(OUT) :: rout,zout
      REAL(r8), DIMENSION(:,:), INTENT(IN) :: rmin,zmin
      INTEGER(i4) :: ir, mr
c-----------------------------------------------------------------------
c     Allocate storage and initialize fourier routines
c-----------------------------------------------------------------------
      nfft = SIZE(rmin,1)
      ALLOCATE(wa(nfft),ch(nfft),c(nfft)); wa = 0.; ch = 0.; c  = 0.
      CALL rffti
c-----------------------------------------------------------------------
c     Get the grid from the coefficients
c-----------------------------------------------------------------------
      mr= SIZE(rmin,2)

      DO ir=1,mr
        c = rmin(:,ir)
        CALL rfftb
        rout(:,ir)=c

        c = zmin(:,ir)
        CALL rfftb
        zout(:,ir)=c
      ENDDO

      DEALLOCATE(c,wa,ch)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE fourier2grid
c-----------------------------------------------------------------------
c     subprogram 7. geom_ratio.
c     Calculate the constant ratio used in constructing geometric series
c-----------------------------------------------------------------------
      FUNCTION geom_ratio(dsep,dr0,mr) RESULT(cr)
      IMPLICIT NONE

      REAL(r8), INTENT(IN) :: dsep, dr0
      INTEGER(i4), INTENT(IN) :: mr

      INTEGER(i4), PARAMETER :: maxits=1000
      REAL(r8), PARAMETER :: cr_accuracy = 1.e-12
      REAL(r8) :: ratio, cr, upper_cr, lower_cr, cr_old, cr_extrema
      INTEGER(i4) :: i
c-----------------------------------------------------------------------
c     The grid is based on a geometric progression:
c       dr(i+1)/dr(i) = constant ratio = cr
c     This leads to the following equation for cr:
c       cr^(mr+1) -ratio*cr + ratio-1 =0 where ratio = (b-a)/dr0
c-----------------------------------------------------------------------
      ratio = dsep/dr0                                      ! (b-a)/dr0
      upper_cr = ratio**(1./mr)                             ! Analytic
      lower_cr = 1. - 1./ratio                              !  limits
      cr_extrema =  (ratio/REAL((mr+1)))**(1./REAL(mr))
      IF(cr_extrema > 1) THEN
          cr_old=(upper_cr+cr_extrema)/2.                   ! Init guess
      ELSEIF(cr_extrema < 1) THEN
          cr_old=(lower_cr+cr_extrema)/2.                   ! Init guess
      ELSEIF(cr_extrema == 1) THEN
          cr=1.
          RETURN
      ENDIF
c-----------------------------------------------------------------------
c     Find cr by Newton Iteration.  Write suggestions
c-----------------------------------------------------------------------
      i=0
      DO
         i=i+1
         IF(i > maxits) CALL fg_stop("Problem:  try decreasing mvac")
         cr = cr_old - (cr_old**(mr+1)-ratio*cr_old+ratio-1)/
     &                 ((mr+1)*cr_old**mr - ratio)
         IF (ABS(cr-cr_old) < cr_accuracy) EXIT
         cr_old=cr
      ENDDO
c      IF (cr < 1.)       WRITE(6,*) "   SUGGESTION: Decrease mvac"
      IF (cr > upper_cr) CALL fg_stop("PROBLEM: Try increasing mvac")
      IF (cr > upper_cr) CALL fg_stop("PROBLEM: Try decreasing mvac")
c-----------------------------------------------------------------------
c        ds=0.
c        DO i=0,mr
c          ds=ds + dr0*cr**i
c          WRITE(*,*) i, ds, dsep
c        ENDDO
c-----------------------------------------------------------------------
      RETURN
      END FUNCTION geom_ratio
c-----------------------------------------------------------------------
      END MODULE grid
