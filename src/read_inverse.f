c-----------------------------------------------------------------------
c     file read_inverse.f.
c     Files which read in inverse-type equilibria
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. read_chease
c     4. read_miller8
c     6. read_miller8b
c     6. read_jsolver
c     6. read_fluxgrid
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     subprogram 1. read_chease.
c     reads data from chease.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE read_chease(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgInputEqTypes
      USE fg_spline
      USE fg_bicube
      IMPLICIT NONE

      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      INTEGER, PARAMETER :: eqUnit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44

      INTEGER(i4) :: ntnova,npsi1,isym,ia,itau,i,ms_in, mt_in
      REAL(r8), DIMENSION(5) :: axx
      REAL(r8), DIMENSION(:), POINTER ::   zcpr,zcppr,zq,zdq,ztmf
      REAL(r8), DIMENSION(:), POINTER ::   ztp,zfb,zfbp,zpsi,zpsim
      REAL(r8), DIMENSION(:,:), POINTER :: zrcp,zzcp,zjacm,zjac
      REAL(r8) :: n_edge,psio
      TYPE(spline_type) :: tmp_dens
c-----------------------------------------------------------------------
c     open file and read sizes.
c-----------------------------------------------------------------------
      fileIn%eqType="inverse"
      CALL open_bin(eqUnit,TRIM(fileIn%origFile),
     &               "UNKNOWN","REWIND",32_i4)
      READ(eqUnit)ntnova,npsi1,isym              ! last data is symmettry
      READ(eqUnit)axx
      IF(isym == 1)CALL fg_stop
     & ("read_chease:  Cannot process symmetric data")
c-----------------------------------------------------------------------
c     allocate local arrays.
c-----------------------------------------------------------------------
      ALLOCATE(zcpr(npsi1-1),zcppr(npsi1),zq(npsi1),zdq(npsi1))
      ALLOCATE(ztmf(npsi1),ztp(npsi1),zfb(npsi1),zfbp(npsi1))
      ALLOCATE(zpsi(npsi1),zzcp(ntnova+3,npsi1),zpsim(npsi1-1))
      ALLOCATE(zrcp(ntnova+3,npsi1))
      ALLOCATE(zjacm(ntnova+3,npsi1),zjac(ntnova+3,npsi1))
c-----------------------------------------------------------------------
c     read arrays and close file.
c-----------------------------------------------------------------------
      READ(eqUnit)zcpr                               ! Pressure
      READ(eqUnit)zcppr                              ! d Pressure/d psi
      READ(eqUnit)zq                                 ! q
      READ(eqUnit)zdq                                ! d q/d psi
      READ(eqUnit)ztmf
      READ(eqUnit)ztp
      READ(eqUnit)zfb                                ! F/q
      READ(eqUnit)zfbp                               ! d (F/q )/d psi
      READ(eqUnit)zpsi
      READ(eqUnit)zpsim
      READ(eqUnit)zrcp
      READ(eqUnit)zzcp
      READ(eqUnit)zjacm
      READ(eqUnit)zjac
      CALL close_bin(eqUnit)
c-----------------------------------------------------------------------
c     copy 1D arrays.
c-----------------------------------------------------------------------
      ms_in=npsi1-2
      eqIn%ms_in=ms_in
      CALL spline_alloc(eqIn%sq_in,ms_in,eqIn%nsq_in)
      psio=zpsi(npsi1)-zpsi(1)
      eqIn%psio=psio
      eqIn%sq_in%xs=(zpsi(:)-zpsi(1))/psio
      eqIn%sq_in%fs(:,1)=ztmf(2:)
      eqIn%sq_in%fs(:,2)=zcppr(2:)
      eqIn%sq_in%fs(:,3)=zq(2:)
      eqIn%sq_in%fs(:,4)=0
      ia=ms_in
      CALL spline_fit(eqIn%sq_in,"extrap")
c-----------------------------------------------------------------------
c     integrate pressure.
c-----------------------------------------------------------------------
      CALL spline_int(eqIn%sq_in)
      eqIn%sq_in%fs(:,2)=(eqIn%sq_in%fsi(:,2)
     &                   -eqIn%sq_in%fsi(ms_in,2))*psio
      CALL spline_fit(eqIn%sq_in,"extrap")
c-----------------------------------------------------------------------
c     copy 2D arrays.
c-----------------------------------------------------------------------
      mt_in=ntnova
      eqIn%ro=SUM(zrcp(1:ntnova,1))/ntnova
      eqIn%zo=SUM(zzcp(1:ntnova,1))/ntnova
      ALLOCATE(eqIn%rg_in(0:mt_in,0:ms_in))
      ALLOCATE(eqIn%zg_in(0:mt_in,0:ms_in))
      eqIn%rg_in=zrcp(1:ntnova+1,2:)
      eqIn%zg_in=zzcp(1:ntnova+1,2:)
      OPEN(UNIT = 7, FILE='temp.dat',STATUS='UNKNOWN')
      DO ia=0,ms_in
        write(7,*)
        DO itau=0,mt_in
          write(7,fmt='(x,e12.5,x,e12.5,x,e12.5)')
     &        REAL(itau)
     &       ,eqIn%rg_in(itau,ia)-eqIn%ro
     &       ,eqIn%zg_in(itau,ia)-eqIn%zo
        ENDDO
      ENDDO
      CLOSE(7)
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     deallocate local arrays and process inverse equilibrium.
c-----------------------------------------------------------------------
      DEALLOCATE(zcpr,zcppr,zq,zdq,ztmf,ztp,zfb,zfbp,zpsi,zpsim)
      DEALLOCATE(zrcp,zzcp,zjacm,zjac)
      RETURN
      END SUBROUTINE read_chease
c-----------------------------------------------------------------------
c     subprogram 7. read_miller.
c     reads data from Bob Miller's TOQ equilibrium code.
c     This is the r8 ascii data which is the best
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE read_miller8(io,ts,fileIn,eqIn) 
      USE fg_local
      USE fg_physdat
      USE fg_spline
      USE fg_bicube
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE

      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      INTEGER, PARAMETER :: eqUnit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44

      INTEGER(i4) :: i, ms_in, mt_in
      REAL(r8), DIMENSION(:), ALLOCATABLE :: psis_in,fs_in,ps_in,qs_in
      REAL(r8), DIMENSION(:), ALLOCATABLE :: mach_in
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: rd_in,zd_in
      REAL(r8) :: n_edge, psio
      TYPE(spline_type) :: tmp_dens
c-----------------------------------------------------------------------
c     the reals and integers are assumed to be 32 bit.
c-----------------------------------------------------------------------
      fileIn%eqType="inverse"
      OPEN(eqUnit,file=TRIM(fileIn%origFile),status="UNKNOWN")
      READ(eqUnit,*)mt_in,ms_in
      mt_in=mt_in-1
      ms_in=ms_in-1
      eqIn%ms_in=ms_in; eqIn%mt_in=mt_in
c-----------------------------------------------------------------------
c     allocate and read arrays, close files.
c-----------------------------------------------------------------------
      ALLOCATE(psis_in(0:ms_in))
      ALLOCATE(fs_in(0:ms_in))
      ALLOCATE(ps_in(0:ms_in))
      ALLOCATE(qs_in(0:ms_in))
      ALLOCATE(rd_in(0:mt_in,0:ms_in))
      ALLOCATE(zd_in(0:mt_in,0:ms_in))
      ALLOCATE(mach_in(0:ms_in))
      READ(eqUnit,*)psis_in
      READ(eqUnit,*)fs_in
      READ(eqUnit,*)ps_in
      READ(eqUnit,*)qs_in
      READ(eqUnit,*)rd_in
      READ(eqUnit,*)zd_in
      mach_in(:) = 0.
      READ(eqUnit,*,END=999) mach_in
      CALL close_bin(eqUnit,TRIM(fileIn%origFile))
 999  CONTINUE
c-----------------------------------------------------------------------
c     copy and modify 1D arrays.
c-----------------------------------------------------------------------
      psio=psis_in(ms_in)-psis_in(0)
      eqIn%psio=psio
      ! Ignore the center point
      ms_in=ms_in-1
      eqIn%ms_in=ms_in
      CALL spline_alloc(eqIn%sq_in,ms_in,eqIn%nsq_in)
      eqIn%sq_in%xs(:)=(psis_in(1:ms_in+1)-psis_in(0))/psio
      eqIn%sq_in%fs(:,1)=fs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,2)=ps_in(1:ms_in+1)*mu0
      eqIn%sq_in%fs(:,3)=qs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,4)=mach_in(1:ms_in+1)
c-----------------------------------------------------------------------
c     copy 2D arrays.
c-----------------------------------------------------------------------
      eqIn%ro=rd_in(0,0)
      eqIn%zo=zd_in(0,0)
      ALLOCATE(eqIn%rg_in(0:mt_in,0:ms_in))
      ALLOCATE(eqIn%zg_in(0:mt_in,0:ms_in))
      eqIn%rg_in=rd_in(:,1:ms_in+1)
      eqIn%zg_in=zd_in(:,1:ms_in+1)
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      DEALLOCATE(psis_in,fs_in,ps_in,qs_in,rd_in,zd_in,mach_in)
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate program.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_miller8
c-----------------------------------------------------------------------
c     subprogram 7. read_miller.
c     reads data from Bob Miller's TOQ equilibrium code.
c     This is the r8 binary data
c-----------------------------------------------------------------------
      SUBROUTINE read_miller8b(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fg_spline
      USE fg_bicube
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      INTEGER, PARAMETER :: eqUnit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44

      INTEGER(i4) :: i, ms_in, mt_in
      REAL(r8), DIMENSION(:), ALLOCATABLE :: psis_in,fs_in,ps_in,qs_in,
     &                                       mach_in
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: rd_in,zd_in
      REAL(r8) :: n_edge, psio
      TYPE(spline_type) :: tmp_dens
c-----------------------------------------------------------------------
c     the reals and integers are assumed to be 32 bit.
c-----------------------------------------------------------------------
      fileIn%eqType="inverse"
      CALL open_bin(eqUnit,TRIM(fileIn%origFile),
     &                "UNKNOWN","REWIND",32_i4)
      READ(eqUnit)mt_in,ms_in
      mt_in=mt_in-1
      ms_in=ms_in-1
      eqIn%ms_in=ms_in; eqIn%mt_in=mt_in
c-----------------------------------------------------------------------
c     allocate and read arrays, close files.
c-----------------------------------------------------------------------
      ALLOCATE(psis_in(0:ms_in))
      ALLOCATE(fs_in(0:ms_in))
      ALLOCATE(ps_in(0:ms_in))
      ALLOCATE(qs_in(0:ms_in))
      ALLOCATE(rd_in(0:mt_in,0:ms_in))
      ALLOCATE(zd_in(0:mt_in,0:ms_in))
      ALLOCATE(mach_in(0:ms_in))
      READ(eqUnit)psis_in
      READ(eqUnit)fs_in
      READ(eqUnit)ps_in
      READ(eqUnit)qs_in
      READ(eqUnit)rd_in
      READ(eqUnit)zd_in
      mach_in(:) = 0.
      READ(eqUnit,END=999) mach_in
      CALL close_bin(eqUnit,TRIM(fileIn%origFile))
 999  CONTINUE
c-----------------------------------------------------------------------
c     copy and modify 1D arrays.
c-----------------------------------------------------------------------
      psio=psis_in(ms_in)-psis_in(0);              eqIn%psio=psio
      ! Ignore the center point
      ms_in=ms_in-1
      eqIn%ms_in=ms_in
      CALL spline_alloc(eqIn%sq_in,ms_in,eqIn%nsq_in)
      eqIn%sq_in%xs(:)=(psis_in(1:ms_in+1)-psis_in(0))/psio
      eqIn%sq_in%fs(:,1)=fs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,2)=ps_in(1:ms_in+1)*mu0
      eqIn%sq_in%fs(:,3)=qs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,4)=mach_in(1:ms_in+1)
c-----------------------------------------------------------------------
c     copy 2D arrays.
c-----------------------------------------------------------------------
      eqIn%ro=rd_in(0,0)
      eqIn%zo=zd_in(0,0)
      ALLOCATE(eqIn%rg_in(0:mt_in,0:ms_in))
      ALLOCATE(eqIn%zg_in(0:mt_in,0:ms_in))
      eqIn%rg_in=rd_in(:,1:ms_in+1)
      eqIn%zg_in=zd_in(:,1:ms_in+1)
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate program.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_miller8b
c-----------------------------------------------------------------------
c     subprogram 7. read_jsolver.
c     reads data from Steve Jardin's JSOLVER inverse equilibrium.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE read_jsolver(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fg_spline
      USE fg_bicube
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      INTEGER, PARAMETER :: eqUnit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44

      INTEGER :: nz1,nz2,nthe,npsi,isym,ma,mtau,i
      INTEGER :: itau,ia,ms_in, mt_in
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: rd_in,zd_in
      REAL(r8), DIMENSION(:), POINTER :: p,ppxx,q,qp,gxx,gpx,
     $     fb,fbp,f,fp,psival,ne,te,ti,avez,zeffa
      REAL(r8) :: dr,dt,xzero,p0,plimpst,pminpst,dz
      REAL(r8) :: rnorm,bnorm,fvac,theta,df,dx,dp,reps,qeps
      REAL(r8), PARAMETER :: psieps=1.E-6
      REAL(r8), DIMENSION(:,:), POINTER :: x,z,aj3,aj
      REAL(r8) :: n_edge,psio
      TYPE(spline_type) :: tmp_dens
c-----------------------------------------------------------------------
c     format statements.
c-----------------------------------------------------------------------
 10   FORMAT(5e16.8)
c-----------------------------------------------------------------------
c     open input file, read scalars, and allocate arrays.
c-----------------------------------------------------------------------
      fileIn%eqType="inverse"
      OPEN(UNIT=eqUnit,FILE=TRIM(fileIn%origFile),STATUS="OLD")
      READ(eqUnit,*)nz1,nz2,nthe,npsi,isym,dr,dt
      READ(eqUnit,*)xzero,p0,plimpst,pminpst
      ALLOCATE(p(nz2),ppxx(nz2),q(nz2),qp(nz2),gxx(nz2),gpx(nz2),
     $     fb(nz2),fbp(nz2),f(nz2),fp(nz2),psival(nz2),
     $     ne(nz2),te(nz2),ti(nz2),avez(nz2),zeffa(nz2),
     $     x(nz1,nz2),z(nz1,nz2),aj3(nz1,nz2),aj(nz1,nz2))
c-----------------------------------------------------------------------
c     read arrays, close files.
c-----------------------------------------------------------------------
      READ(eqUnit,10)p
      READ(eqUnit,10)ppxx
      READ(eqUnit,10)q
      READ(eqUnit,10)qp
      READ(eqUnit,10)gxx
      READ(eqUnit,10)gpx
      READ(eqUnit,10)fb
      READ(eqUnit,10)fbp
      READ(eqUnit,10)f
      READ(eqUnit,10)fp
      READ(eqUnit,10)psival
      READ(eqUnit,10)x
      READ(eqUnit,10)z
      READ(eqUnit,10)aj3
      READ(eqUnit,10)aj
      READ(eqUnit,10)te
      READ(eqUnit,10)ne
      READ(eqUnit,10)ti
      READ(eqUnit,10)avez
      READ(eqUnit,10)zeffa
      CLOSE(UNIT=eqUnit)
c-----------------------------------------------------------------------
c     copy and modify 1D arrays.
c     Note that we put in a point that is psieps away from psi=0 (axis)
c     and use extrapolation for the 1D arrays
c-----------------------------------------------------------------------
      fvac=ts%fnorm_input
      rnorm=ts%rnorm_input
      bnorm=fvac/rnorm
      ms_in=npsi-1;   ma=ms_in
      eqIn%ms_in=ms_in
      CALL spline_alloc(eqIn%sq_in,ms_in,eqIn%nsq_in)
      psio=-psival(1) *bnorm
      eqIn%sq_in%xs(0)=psieps
      eqIn%sq_in%xs(1:ms_in)=1-psival(2:npsi)/psival(1)

      eqIn%sq_in%fs(1:ms_in,1)=(gxx(1:npsi-1)+gxx(2:npsi))*xzero/2*bnorm
      df=eqIn%sq_in%fs(2,1)-eqIn%sq_in%fs(1,1)
      dx=eqIn%sq_in%xs(2)-eqIn%sq_in%xs(1)
      eqIn%sq_in%fs(0,1)=eqIn%sq_in%fs(1,1)
     &                  -df/dx*(eqIn%sq_in%xs(1)-psieps)
      eqIn%sq_in%fs(ma,1)=gxx(npsi)*xzero  *bnorm

      eqIn%sq_in%fs(1:ms_in,2)=(p(1:npsi-1)+p(2:npsi))/2  *bnorm**2
      dp=eqIn%sq_in%fs(2,2)-eqIn%sq_in%fs(1,2)
      eqIn%sq_in%fs(0,2)=eqIn%sq_in%fs(1,2)
     &                   -dp/dx*(eqIn%sq_in%xs(1)-psieps)

c      eqIn%sq_in%fs(ma,2)=0
      eqIn%sq_in%fs(1:ms_in,3)=q(2:npsi)
      eqIn%sq_in%fs(0,3)=q(1)

      eqIn%sq_in%fs(:,4)=0.
c-----------------------------------------------------------------------
c     copy 2D arrays.
c-----------------------------------------------------------------------
      eqIn%ro=x(3,1)
      eqIn%zo=z(3,1)
      qeps=eqIn%sq_in%fs(1,1)-(q(2)-q(1))/dx*(eqIn%sq_in%xs(1)-psieps)
      reps=SQRT(eqIn%ro*qeps*psieps/eqIn%sq_in%fs(0,1)/twopi)
      IF(isym == 0)THEN
          mt_in=nthe; mtau=mt_in
          eqIn%mt_in=mt_in;  eqIn%ms_in=ms_in
          ALLOCATE(eqIn%rg_in(0:mt_in,0:ms_in))
          ALLOCATE(eqIn%zg_in(0:mt_in,0:ms_in))
          eqIn%rg_in(:,1:ms_in)=x(3:nthe+2,2:npsi)
          eqIn%zg_in(:,1:ms_in)=z(3:nthe+2,2:npsi)
          DO itau=0,mt_in
             dr=eqIn%rg_in(itau,2)-eqIn%ro
             dz=eqIn%zg_in(itau,2)-eqIn%zo
             theta=ATAN2(dz,dr)
             eqIn%rg_in(itau,0)=eqIn%ro+reps*COS(theta)
             eqIn%zg_in(itau,0)=eqIn%zo+reps*SIN(theta)
          ENDDO
      ELSE
          mt_in=nthe-1; mtau=mt_in
          eqIn%mt_in=mt_in;  eqIn%ms_in=ms_in
          ALLOCATE(eqIn%rg_in(0:2*mt_in,0:ms_in))
          ALLOCATE(eqIn%zg_in(0:2*mt_in,0:ms_in))
          eqIn%rg_in(0:mtau,1:ms_in)=x(3:nthe+2,2:npsi)
          eqIn%zg_in(0:mtau,1:ms_in)=z(3:nthe+2,2:npsi)
          eqIn%rg_in(mtau:2*mtau,1:ms_in)= x(nthe+2:3:-1,2:npsi)
          eqIn%zg_in(mtau:2*mtau,1:ms_in)=-z(nthe+2:3:-1,2:npsi)
          mt_in=2*mt_in
          DO itau=0,mt_in
             dr=eqIn%rg_in(itau,2)-eqIn%ro
             dz=eqIn%zg_in(itau,2)-eqIn%zo
             theta=ATAN2(dz,dr)
             eqIn%rg_in(itau,0)=eqIn%ro+reps*COS(theta)
             eqIn%zg_in(itau,0)=eqIn%zo+reps*SIN(theta)
          ENDDO
      ENDIF
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate program.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_jsolver


c-----------------------------------------------------------------------
c     subprogram 7. read_fluxgrid.
c     reads data from old fluxgrid.dat's
cNEED TO IMPLEMENT
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE read_fluxgrid(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fg_spline
      USE fg_bicube
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn

      INTEGER, PARAMETER :: eqUnit = 16  ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44

      INTEGER(i4) :: i, ms_in, mt_in
      REAL(r8), DIMENSION(:), ALLOCATABLE :: psis_in,fs_in,ps_in,qs_in,
     &                                       mach_in
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: rd_in,zd_in
      REAL(r8) :: n_edge,psio
      TYPE(spline_type) :: tmp_dens
c-----------------------------------------------------------------------
c     the reals and integers are assumed to be 32 bit.
c-----------------------------------------------------------------------
      fileIn%eqType="inverse"
      OPEN(eqUnit,file=TRIM(fileIn%origFile),status="UNKNOWN")
      READ(eqUnit,*)mt_in,ms_in
      mt_in=mt_in-1;   eqIn%mt_in=mt_in
      ms_in=ms_in-1
c-----------------------------------------------------------------------
c     allocate and read arrays, close files.
c-----------------------------------------------------------------------
      ALLOCATE(psis_in(0:ms_in))
      ALLOCATE(fs_in(0:ms_in))
      ALLOCATE(ps_in(0:ms_in))
      ALLOCATE(qs_in(0:ms_in))
      ALLOCATE(rd_in(0:mt_in,0:ms_in))
      ALLOCATE(zd_in(0:mt_in,0:ms_in))
      ALLOCATE(mach_in(0:ms_in))
      READ(eqUnit,*)psis_in
      READ(eqUnit,*)fs_in
      READ(eqUnit,*)ps_in
      READ(eqUnit,*)qs_in
      READ(eqUnit,*)rd_in
      READ(eqUnit,*)zd_in
      mach_in(:) = 0.
      READ(eqUnit,*,END=999) mach_in
      CALL close_bin(eqUnit,TRIM(fileIn%origFile))
 999  CONTINUE
c-----------------------------------------------------------------------
c     copy and modify 1D arrays.
c-----------------------------------------------------------------------
      psio=psis_in(ms_in)-psis_in(0);         eqIn%psio=psio
      ms_in=ms_in-1;   eqIn%ms_in=ms_in
      CALL spline_alloc(eqIn%sq_in,ms_in,eqIn%nsq_in)
      eqIn%sq_in%xs(:)=(psis_in(1:ms_in+1)-psis_in(0))/psio
      eqIn%sq_in%fs(:,1)=fs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,2)=ps_in(1:ms_in+1)*mu0
      eqIn%sq_in%fs(:,3)=qs_in(1:ms_in+1)
      eqIn%sq_in%fs(:,4)=mach_in(1:ms_in+1)
c-----------------------------------------------------------------------
c     copy 2D arrays.
c-----------------------------------------------------------------------
      eqIn%ro=rd_in(0,0)
      eqIn%zo=zd_in(0,0)
      ALLOCATE(eqIn%rg_in(0:mt_in,0:ms_in))
      ALLOCATE(eqIn%zg_in(0:mt_in,0:ms_in))
      eqIn%rg_in=rd_in(:,1:ms_in+1)
      eqIn%zg_in=zd_in(:,1:ms_in+1)
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate program.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_fluxgrid
