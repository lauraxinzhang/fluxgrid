!---------------------------------------------------------------------
!#
!# File:  analyze_type.f.
!#
!# Purpose:  Declare the common derived types for analyzing the
!#           equilibrium
!#                          
!#           
!#
!## $Id: analyze_type.f 1276 2009-08-05 21:54:17Z srinath $
!---------------------------------------------------------------------
!> These are the types containing derived quantities which can be
!! calculated from the mappedEq data structure.
!! Data structures in this module include:
!!    1) TransportGeometry: The metric elements and related geometric
!!          quantities that are used used in a 1D transport code
!!    2) globalEqDiagnose: global scalars characterizing the plasma.  
!!          For example: current, elongation, aspect ratio, etc.
!!    3) globalEqProfiles: Useful profiles characterizing the plasma
!!    4) singularSurfaces: Key values at the rational surfaces
!!    5) neoclassical: Decomposition of the current into the 
!!          various neoclassical terms: Pfirsh-Schluter, bootstrap.
!!    6) stabilityType: Calculations of stability quantities: D_ideal,
!!          D_R, ...
c-----------------------------------------------------------------------
c     subprogram 0. analyze_type.
c     type definitions contained within a module for convenience
c-----------------------------------------------------------------------
      MODULE fgAnalyzeTypes
      USE fg_local
      IMPLICIT NONE

!=======================================================================
!       TransportGeometry
!
!-----------------------------------------------------------------------       

!> Transport geometry type
!! @note \f$\rho =\frac{\Phi}{\Phi_{total}}\f$ is unitless.
!!        All gradients are with respect to \f$\rho\f$
!! \f$arho=\sqrt{\frac{\Phi_{total}}{\pi B_T}}\f$ 
!! and \f$\tilde{\rho}=\sqrt{\frac{\Phi}{\pi B_T}}\f$ in GLF23.
!! Thus our \f$\rho=\frac{\tilde{\rho}}{arho}\f$.  
      TYPE :: TransportGeometry !#SETGET !#SIDL_ST
       REAL(r8) :: anorm            !< Minor radius used to normalize equations
       REAL(r8) :: shafAxis         !< Shafranov shift for axis
       REAL(r8) :: shafEdge         !< Shafranov shift for last closed surface
       REAL(r8), DIMENSION(:), ALLOCATABLE :: rho      !< Normalized radial coord
       REAL(r8), DIMENSION(:), ALLOCATABLE :: rhodim   !< rho*anorm
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dpsidchi !< d(PolFlux)/d(TorFlux)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dpsidrho !< d(PolFlux)/d(rho)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dvdrho   !< d(Volume)/d(rho)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: grdrhosq !< FluxAv(g^{rhorho})
       REAL(r8), DIMENSION(:), ALLOCATABLE :: grdrho   !< FluxAv(grad{rho})
       REAL(r8), DIMENSION(:), ALLOCATABLE :: grdpsisq !< FluxAv(g^{psipsi})
       REAL(r8), DIMENSION(:), ALLOCATABLE :: bigr     !< (Rmax(psi)+Rmin(psi))/2
       REAL(r8), DIMENSION(:), ALLOCATABLE :: bigz     !< (Zmax(psi)+Zmin(psi))/2
       REAL(r8), DIMENSION(:), ALLOCATABLE :: r        !< r=(Rmax(psi)-Rmin(psi))/2
       REAL(r8), DIMENSION(:), ALLOCATABLE :: kappa    !< Elongation=(Zmax(psi)-Zmin(psi))/2
       REAL(r8), DIMENSION(:), ALLOCATABLE :: shafShift    !< Shafranov shift
       REAL(r8), DIMENSION(:), ALLOCATABLE :: delta        !< Triangularity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: deltamiller  !< Triangularity (Miller def)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dqsfdrho     !< d(safety factor)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dkappadrho   !< d(Elongation)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: ddeltadrho   !< d(Triangularity)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: ddeltamdrho  !< d(Triangularity-miller)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dRmajdrho    !< d(R)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dZmajdrho    !< d(Z)/drho
       REAL(r8), DIMENSION(:), ALLOCATABLE :: drhodr       !< d(rho)/dr
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nu           !< Collision frequency
      END TYPE

!> Global scalar quantities which characterize the plasma
!! @note See website for documentation
      TYPE :: globalEqDiagnose !#SETGET !#SIDL_ST
       REAL(r8) :: rmean      !< rmean=(R_max+R_min)/2 ; geometric major radius
       REAL(r8) :: amean      !< amean=(R_max-R_min)/2 ; geometric minor radius
       REAL(r8) :: aratio     !< aspect ratio: R_geom/amean
       REAL(r8) :: bt0        !< B_toroidal=F_vac/R_geom
       REAL(r8) :: crnt       !< Total toroidal plasma current
       REAL(r8) :: volume     !< Total plasma volume
       REAL(r8) :: area_xsct  !< Total cross-sectional area
       REAL(r8) :: area_surf  !< Total surface area.
       REAL(r8) :: delta1     !< Upper triangularity
       REAL(r8) :: delta2     !< Lower triangularity
       REAL(r8) :: kappa      !< Elongation
       REAL(r8) :: li1        !< Internal inductance
       REAL(r8) :: li2        !< Internal inductance
       REAL(r8) :: li3        !< Internal inductance
       REAL(r8) :: torfluxo   !< Total toroidal flux
       REAL(r8) :: pave       !< Volume averaged pressure
       REAL(r8) :: p0         !< Pressure on axis
       REAL(r8) :: betat      !< Toroidal beta
       REAL(r8) :: betan      !< Beta normal
       REAL(r8) :: betap1     !< Beta poloidal
       REAL(r8) :: betap2     !< Beta poloidal
       REAL(r8) :: betap3     !< Beta poloidal
       REAL(r8) :: pe0        !< Electron pressure on axis
       REAL(r8) :: nd0        !< Electron number density on axis
       REAL(r8) :: ti0        !< Ion temperature on axis
       REAL(r8) :: te0        !< Electron temperature on axis
       REAL(r8) :: q0         !< Safety factor on axis
       REAL(r8) :: qmin       !< Minimum safety factor 
       REAL(r8) :: qmax       !< Maximum safety factor 
       REAL(r8) :: qa         !< Safety factor at psihigh
       REAL(r8) :: vt0e       !< Electron thermal velocity on axis
       REAL(r8) :: vt0i       !< Ion thermal velocity on axis
       REAL(r8) :: omega_te   !< Electron cyclotron frequency on axis
       REAL(r8) :: omega_ti   !< Ion cyclotron frequency on axis
       REAL(r8) :: nu_e       !< Electron collision frequency on axis
       REAL(r8) :: nu_i       !< Ion collision frequency on axis
       REAL(r8) :: omega_star !< Star frequency on axis
       REAL(r8) :: omega_ci   !< Ion cyclotron frequency on axis
       REAL(r8) :: omega_ce   !< Electron cyclotron frequency on axis
       REAL(r8) :: omega_pe   !< Electron plasma frequency on axis
       REAL(r8) :: omega_pi   !< Ion plasma frequency on axis
       REAL(r8) :: l_pll      !< Parallel length scale
       REAL(r8) :: le_debye   !< Electron Debye length
       REAL(r8) :: li_debye   !< Ion Debye length
       REAL(r8) :: re_larmor  !< Electron Larmor Radius
       REAL(r8) :: ri_larmor  !< Ion Larmor Radius
       REAL(r8) :: le_mfp     !< Electron Mean Free Path
       REAL(r8) :: li_mfp     !< Ion Mean Free Path
       REAL(r8) :: tfloor     !< Floor on temperature
       REAL(r8) :: nfloor     !< Floor on density
       REAL(r8) :: tauafac    !< TauAfac
       REAL(r8) :: taurfac    !< TauRfac
       REAL(r8) :: taua       !< Alfven time scale
       REAL(r8) :: taur       !< Resistive time scale
       REAL(r8) ::  v_a       !< Alfven velocity
      END TYPE globalEqDiagnose

!> Type for calculating key quantities on rational surface
!! @note See website for documentation
      TYPE :: singularSurfaces                      !#SIDL_ST
       INTEGER(i4) :: nsing
       INTEGER(i4) :: num_root
       INTEGER(i4), DIMENSION(64) :: msing
       REAL(r8), DIMENSION(64) :: qsing
       REAL(r8), DIMENSION(64) :: q1sing
       REAL(r8), DIMENSION(64) :: psising
      END TYPE singularSurfaces

!> Various global equilibrium profiles
!! @note See website for documentation
      TYPE :: globalEqProfiles                              !#SIDL_ST
       REAL(r8), DIMENSION(:), ALLOCATABLE :: torflux       !< Toroidal flux
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vprime        !< d(Volume)/d(Psi_Normal)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: te            !< electron Temperature (Joules)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: ti            !< ion Temperature (Joules)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: te_ev         !< electron Temperature (eV)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: ti_ev         !< ion Temperature (eV)
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: lambda      !< J.B/B^2           !#SIDL_ST_MEMNAME=lmbda#
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: delstr      !< Del^star(Psi)
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: gsrhs       !< right-side of Grad-Shafranov Eq.
       REAL(r8), DIMENSION(:), ALLOCATABLE :: lambdaprof    !< Fluxav(lambda)
       REAL(r8), DIMENSION(:), ALLOCATABLE :: coulomb_log   !< Coulomb logarithm
       REAL(r8), DIMENSION(:), ALLOCATABLE :: eta_prp       !< Braginskii resistivity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: chi_pll       !< Braginskii parallel conduction
       REAL(r8), DIMENSION(:), ALLOCATABLE :: chi_cross     !< Braginskii cross conduction
       REAL(r8), DIMENSION(:), ALLOCATABLE :: chi_prp       !< Braginskii perp conduction
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nu_pll        !< Braginskii parallel kin. viscosities
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nu_cross      !< Braginskii cross kin. viscosity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nu_prp        !< Braginskii perp kin. viscosities
       REAL(r8), DIMENSION(:), ALLOCATABLE :: tau_e         !< Electron collisional time scale
       REAL(r8), DIMENSION(:), ALLOCATABLE :: tau_i         !< Ion collisional time scale
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vte           !< Electron thermal velocity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vti           !< Ion thermal velocity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vde           !< Electron drift velocity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vdi           !< Ion drift velocity
       REAL(r8), DIMENSION(:), ALLOCATABLE :: area_xsct     !< Cross-sectional area
       REAL(r8), DIMENSION(:), ALLOCATABLE :: area_surf     !< Surface area for each flux surface
       REAL(r8), DIMENSION(:), ALLOCATABLE :: vol           !< Volume for each flux surface
       REAL(r8), DIMENSION(:), ALLOCATABLE :: crnt          !< Toroidal plasma current enclosed by a surface (MA)
      END TYPE globalEqProfiles

!> Type for decomposition of toroidal current into pressure-driven currents
!! @note See website for documentation
!! jbs, etaparnc, L31, L32, k1 all  have different models denoted with the first index
!!   1: Hirshman 88
!!   2: Sauter
!!   3: Callen
!!   4: modified Sauter
!! @note See website for documentation
      TYPE :: neoclassical                        !#SIDL_ST   !#SIDL_ST_NAME=neoclassical#
       REAL(r8), DIMENSION(:), ALLOCATABLE :: fcirc     !< Circulating particle fraction
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nustare   !< e-  effective collisionality
       REAL(r8), DIMENSION(:), ALLOCATABLE :: nustari   !< ion effective collisionality
       REAL(r8), DIMENSION(:), ALLOCATABLE :: omegatre  !< e-  transit frequency
       REAL(r8), DIMENSION(:), ALLOCATABLE :: omegatri  !< ion transit frequency
       REAL(r8), DIMENSION(:), ALLOCATABLE :: jdia      !< Diamagnetic current
       REAL(r8), DIMENSION(:), ALLOCATABLE :: jps       !< Pfirsch-Schluter current
       REAL(r8), DIMENSION(:), ALLOCATABLE :: kcfactor  !< Kagan-Cato factor
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: etaparnc  !< Neoclassical resistivity
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: jbs       !< Bootstrap current using Hirshman-88
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: L31       !< L31 coefficient
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: L32       !< Bootstrap current using Hirshman-88
       REAL(r8), DIMENSION(:,:), ALLOCATABLE :: ki        !< Bootstrap current using Hirshman-88
       REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: neomat    !< Various 2x2 neoclassical matrices
      END TYPE neoclassical

!> MHD stability measures
!! @note See website for documentation
      TYPE :: stabilityType                            !#SIDL_ST
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dideal   !< D_I
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dres     !< D_R
       REAL(r8), DIMENSION(:), ALLOCATABLE :: hfactor  !< H
       REAL(r8), DIMENSION(:), ALLOCATABLE :: dnc      !< D_nc
       REAL(r8), DIMENSION(:), ALLOCATABLE :: fcirc    !< circulating particle fraction
       REAL(r8), DIMENSION(:), ALLOCATABLE :: alphas   !< alpha_s
      END TYPE stabilityType
      END MODULE fgAnalyzeTypes
c-----------------------------------------------------------------------
c     subprogram 1. globalEqProfilesAlloc.
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE globalEqProfilesAlloc(nr,nt,gEP)
      USE fg_local
      USE fgAnalyzeTypes
      IMPLICIT NONE
      INTEGER(i4), INTENT(IN) :: nr,nt
      TYPE(globalEqProfiles), INTENT(INOUT) :: gEP
        ALLOCATE(gEP%torflux(0:nr))
        ALLOCATE(gEP%vprime(0:nr))
        ALLOCATE(gEP%lambdaprof(0:nr))
        ALLOCATE(gEP%lambda(0:nt,0:nr))
        ALLOCATE(gEP%delstr(0:nt,0:nr))
        ALLOCATE(gEP%gsrhs(0:nt,0:nr))
        ALLOCATE(gEP%eta_prp(0:nr))
        ALLOCATE(gEP%chi_pll(0:nr))
        ALLOCATE(gEP%chi_cross(0:nr))
        ALLOCATE(gEP%chi_prp(0:nr))
        ALLOCATE(gEP%nu_pll(0:nr))
        ALLOCATE(gEP%nu_cross(0:nr))
        ALLOCATE(gEP%nu_prp(0:nr))
        ALLOCATE(gEP%tau_e(0:nr))
        ALLOCATE(gEP%tau_i(0:nr))
        ALLOCATE(gEP%te(0:nr))
        ALLOCATE(gEP%ti(0:nr))
        ALLOCATE(gEP%te_ev(0:nr))
        ALLOCATE(gEP%ti_ev(0:nr))
        ALLOCATE(gEP%coulomb_log(0:nr))
        ALLOCATE(gEP%vte(0:nr))
        ALLOCATE(gEP%vti(0:nr))
        ALLOCATE(gEP%vde(0:nr))
        ALLOCATE(gEP%vdi(0:nr))
        ALLOCATE(gEP%crnt(0:nr))
        ALLOCATE(gEP%area_xsct(0:nr))
        ALLOCATE(gEP%area_surf(0:nr))
        ALLOCATE(gEP%vol(0:nr))

      END SUBROUTINE globalEqProfilesAlloc

c-----------------------------------------------------------------------
c     subprogram 1. globalEqProfilesDealloc.
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE globalEqProfilesDealloc(gEP)
      USE fgAnalyzeTypes
      IMPLICIT NONE
      TYPE(globalEqProfiles), INTENT(INOUT) :: gEP
        IF (ALLOCATED(gEP%lambdaprof))    DEALLOCATE(gEP%lambdaprof)
        IF (ALLOCATED(gEP%lambda))        DEALLOCATE(gEP%lambda)
        IF (ALLOCATED(gEP%delstr))        DEALLOCATE(gEP%delstr)
        IF (ALLOCATED(gEP%gsrhs))         DEALLOCATE(gEP%gsrhs)
        IF (ALLOCATED(gEP%eta_prp))       DEALLOCATE(gEP%eta_prp)
        IF (ALLOCATED(gEP%chi_pll))       DEALLOCATE(gEP%chi_pll)
        IF (ALLOCATED(gEP%chi_cross))     DEALLOCATE(gEP%chi_cross)
        IF (ALLOCATED(gEP%chi_prp))       DEALLOCATE(gEP%chi_prp)
        IF (ALLOCATED(gEP%nu_pll))        DEALLOCATE(gEP%nu_pll)
        IF (ALLOCATED(gEP%nu_cross))      DEALLOCATE(gEP%nu_cross)
        IF (ALLOCATED(gEP%nu_prp))        DEALLOCATE(gEP%nu_prp)
        IF (ALLOCATED(gEP%tau_e))         DEALLOCATE(gEP%tau_e)
        IF (ALLOCATED(gEP%tau_i))         DEALLOCATE(gEP%tau_i)
        IF (ALLOCATED(gEP%te))            DEALLOCATE(gEP%te)
        IF (ALLOCATED(gEP%ti))            DEALLOCATE(gEP%ti)
        IF (ALLOCATED(gEP%te_ev))         DEALLOCATE(gEP%te_ev)
        IF (ALLOCATED(gEP%ti_ev))         DEALLOCATE(gEP%ti_ev)
        IF (ALLOCATED(gEP%coulomb_log))   DEALLOCATE(gEP%coulomb_log)
        IF (ALLOCATED(gEP%vte))           DEALLOCATE(gEP%vte)
        IF (ALLOCATED(gEP%vti))           DEALLOCATE(gEP%vti)
        IF (ALLOCATED(gEP%vde))           DEALLOCATE(gEP%vde)
        IF (ALLOCATED(gEP%vdi))           DEALLOCATE(gEP%vdi)
      END SUBROUTINE globalEqProfilesDealloc

c-----------------------------------------------------------------------
c     subprogram 1. stabilityTypeAlloc
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE stabilityTypeAlloc(nr,sT)
      USE fgAnalyzeTypes
      IMPLICIT NONE
      INTEGER(i4), INTENT(IN) :: nr
      TYPE(stabilityType), INTENT(INOUT) :: sT
        ALLOCATE(sT%dideal(0:nr))
        ALLOCATE(sT%dres(0:nr))
        ALLOCATE(sT%hfactor(0:nr))
        ALLOCATE(sT%dnc(0:nr))
        ALLOCATE(sT%alphas(0:nr))
        ALLOCATE(sT%fcirc(0:nr))
      END SUBROUTINE stabilityTypeAlloc

c-----------------------------------------------------------------------
c     subprogram 1. stabilityTypeDealloc
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE stabilityTypeDealloc(sT)
      USE fgAnalyzeTypes
      IMPLICIT NONE
      TYPE(stabilityType), INTENT(INOUT) :: sT
        IF (ALLOCATED(sT%dideal))        DEALLOCATE(sT%dideal)
        IF (ALLOCATED(sT%dres))          DEALLOCATE(sT%dres)
        IF (ALLOCATED(sT%hfactor))       DEALLOCATE(sT%hfactor)
        IF (ALLOCATED(sT%dnc))           DEALLOCATE(sT%dnc)
        IF (ALLOCATED(sT%alphas))        DEALLOCATE(sT%alphas)
        IF (ALLOCATED(sT%fcirc))         DEALLOCATE(sT%fcirc)
      END SUBROUTINE stabilityTypeDealloc

c-----------------------------------------------------------------------
c     subprogram 1. currentAnalysisAlloc.
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE currentAnalysisAlloc(nr,neo)
      USE fgAnalyzeTypes
      IMPLICIT NONE
      INTEGER(i4), INTENT(IN) :: nr
      INTEGER, PARAMETER :: nmodel=4   !Hirshman88,Sauter,Callen,mod-Sauter
      TYPE(neoclassical), INTENT(INOUT) :: neo
        ALLOCATE(neo%nustare(0:nr))
        ALLOCATE(neo%nustari(0:nr))
        ALLOCATE(neo%omegatre(0:nr))
        ALLOCATE(neo%omegatri(0:nr))
        ALLOCATE(neo%fcirc(0:nr))
        ALLOCATE(neo%jdia(0:nr))
        ALLOCATE(neo%jps(0:nr))
        ALLOCATE(neo%etaparnc(nmodel,0:nr))
        ALLOCATE(neo%jbs(nmodel,0:nr))
        ALLOCATE(neo%L31(nmodel,0:nr))
        ALLOCATE(neo%L32(nmodel,0:nr))
        ALLOCATE(neo%ki(nmodel,0:nr))
        ALLOCATE(neo%neomat(2,0:1,0:1,0:nr))
      END SUBROUTINE currentAnalysisAlloc
c-----------------------------------------------------------------------
c     subprogram 1. currentAnalysisDealloc.
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE currentAnalysisDealloc(neo)
      USE fgAnalyzeTypes
      IMPLICIT NONE
      TYPE(neoclassical), INTENT(INOUT) :: neo
        IF (ALLOCATED(neo%fcirc))         DEALLOCATE(neo%fcirc)
        IF (ALLOCATED(neo%nustare))       DEALLOCATE(neo%nustare)
        IF (ALLOCATED(neo%nustari))       DEALLOCATE(neo%nustari)
        IF (ALLOCATED(neo%omegatre))      DEALLOCATE(neo%omegatre)
        IF (ALLOCATED(neo%omegatri))      DEALLOCATE(neo%omegatri)
        IF (ALLOCATED(neo%etaparnc))      DEALLOCATE(neo%etaparnc)
        IF (ALLOCATED(neo%jdia))          DEALLOCATE(neo%jdia)
        IF (ALLOCATED(neo%jps))           DEALLOCATE(neo%jps)
        IF (ALLOCATED(neo%jbs))           DEALLOCATE(neo%jbs)
        IF (ALLOCATED(neo%L31))           DEALLOCATE(neo%L31)
        IF (ALLOCATED(neo%L32))           DEALLOCATE(neo%L32)
        IF (ALLOCATED(neo%ki))            DEALLOCATE(neo%ki)
        IF (ALLOCATED(neo%neomat))        DEALLOCATE(neo%neomat)
      END SUBROUTINE currentAnalysisDealloc
c-----------------------------------------------------------------------
c     subprogram 1. transport_alloc.
c     deallocate the data
c-----------------------------------------------------------------------
      SUBROUTINE transportAlloc(nr,TrGeom)
      USE fg_local
      USE fg_physdat
      USE fgAnalyzeTypes
      IMPLICIT NONE

      INTEGER(i4), INTENT(IN) :: nr
      TYPE(TransportGeometry), INTENT(INOUT) :: TrGeom

      ALLOCATE(TrGeom%rho(0:nr))
      ALLOCATE(TrGeom%rhodim(0:nr))
      ALLOCATE(TrGeom%dpsidchi(0:nr))
      ALLOCATE(TrGeom%dpsidrho(0:nr))
      ALLOCATE(TrGeom%dvdrho(0:nr))
      ALLOCATE(TrGeom%grdrhosq(0:nr))
      ALLOCATE(TrGeom%grdrho(0:nr))
      ALLOCATE(TrGeom%grdpsisq(0:nr))
      ALLOCATE(TrGeom%r(0:nr))
      ALLOCATE(TrGeom%bigr(0:nr))
      ALLOCATE(TrGeom%bigz(0:nr))
      ALLOCATE(TrGeom%kappa(0:nr))
      ALLOCATE(TrGeom%delta(0:nr))
      ALLOCATE(TrGeom%deltamiller(0:nr))
      ALLOCATE(TrGeom%shafShift(0:nr))
      ALLOCATE(TrGeom%dqsfdrho(0:nr))
      ALLOCATE(TrGeom%dkappadrho(0:nr))
      ALLOCATE(TrGeom%ddeltadrho(0:nr))
      ALLOCATE(TrGeom%ddeltamdrho(0:nr))
      ALLOCATE(TrGeom%dRmajdrho(0:nr))
      ALLOCATE(TrGeom%dZmajdrho(0:nr))
      ALLOCATE(TrGeom%drhodr(0:nr))
      END SUBROUTINE transportAlloc



