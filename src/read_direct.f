c-----------------------------------------------------------------------
c     file read_direct.f.
c     Files which read in direct-type equilibria
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. read_efit.
c     2. read_tokamac.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     subprogram 1. read_efit.
c     reads data from General Atomic's EFIT equilibrium code.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE read_efit(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      INTEGER, PARAMETER :: eq_unit = 16              ! equilibrium file
      INTEGER, PARAMETER :: eq2_unit=44
      INTEGER(i4) :: i,j,nw,nh,ia,ma,ios,imaxx,iprev,ma_tmp
      INTEGER(i4), DIMENSION(1) :: itemp
      INTEGER :: kvtor,nmass
      REAL(r8) :: bcentr,cpasma,rgrid,rmaxis,rzero,ssibry1,
     $     ssibry2,ssimag1,ssimag2,xdim,xdum,zdim,zmaxis,zmid,rvtor
      REAL(r8) :: rdnd,rdpsi,smallnum
      INTEGER(i4) :: nbbbs, limitr
      INTEGER(i4) :: mr,mz
      INTEGER(i4), DIMENSION(:), ALLOCATABLE :: iorder
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rbbbs,zbbbs,rho,mass_dens
      REAL(r8), DIMENSION(:), ALLOCATABLE :: xlim,ylim,pressw
      LOGICAL file_exists
c-----------------------------------------------------------------------
c     Do some initialization of the type
c-----------------------------------------------------------------------
      fileIn%eqType="direct"
c-----------------------------------------------------------------------
c     read equilibrium data.
c-----------------------------------------------------------------------
      OPEN(UNIT=eq_unit,FILE=TRIM(fileIn%origFile),STATUS='old')
      READ(eq_unit,'(52x,2i4)')nw,nh
      mr=nw-1;  mz=nh-1;  ma=nw-1
      eqIn%mx=mr; eqIn%my=mz; eqIn%ms_in=ma
      READ(eq_unit,'(5e16.9)')xdim,zdim,rzero,rgrid,zmid
      READ(eq_unit,'(5e16.9)')rmaxis,zmaxis,ssimag1,ssibry1,bcentr
      READ(eq_unit,'(5e16.9)')cpasma,ssimag2,xdum,rmaxis,xdum
      READ(eq_unit,'(5e16.9)')zmaxis,xdum,ssibry2,xdum,xdum
      CALL spline_alloc(eqIn%sq_in,ma,eqIn%nsq_in)
      READ(eq_unit,'(5e16.9)')(eqIn%sq_in%fs(i,1),i=0,ma)
      READ(eq_unit,'(5e16.9)')(eqIn%sq_in%fs(i,2),i=0,ma)
      READ(eq_unit,'(5e16.9)')(eqIn%sq_in%fs(i,3),i=0,ma)
      READ(eq_unit,'(5e16.9)')(eqIn%sq_in%fs(i,3),i=0,ma)
      CALL bicube_alloc(eqIn%psi_in,mr,mz,1_i4)
      READ(eq_unit,'(5e16.9)')((eqIn%psi_in%fs(1,i,j),i=0,mr),j=0,mz)
      READ(eq_unit,'(5e16.9)',iostat=ios)(eqIn%sq_in%fs(i,3),i=0,ma)
c-----------------------------------------------------------------------
c     The above quantities are on a uniform mesh in psi_normal
c-----------------------------------------------------------------------
      eqIn%sq_in%xs=(/(ia,ia=0,ma)/)/dfloat(ma)
c-----------------------------------------------------------------------
c     read and write (if needed) wall data. (controlled by triangle
c      which is not terribly convenient, but sufficient for now)
c     Note: ordering the wall data it to be CCW starting at outboard 
c       side to make developing wall easier.  
c      Assume that it starts at either inboard or outboard
c-----------------------------------------------------------------------
      READ(eq_unit,'(2i5)',iostat=ios)nbbbs,limitr
      IF(ios == 0_i4)then
        ALLOCATE(rbbbs(nbbbs),zbbbs(nbbbs),xlim(limitr),ylim(limitr))
        READ(eq_unit,'(5e16.9)') (rbbbs(i),zbbbs(i),i=1,nbbbs)
        DEALLOCATE(rbbbs,zbbbs)  ! May need this later so keep
        READ(eq_unit,'(5e16.9)') (xlim(i),ylim(i),i=1,limitr)
        DEALLOCATE(xlim,ylim)
      ENDIF
c----------------------------------------------------------------------
c    Read in rotation information if available.  Translate to nimrod's
c     internal quantities (No R_T is not quite R_magnetic_axis):
c        M_s=R_m/R_T SQRT(2/Gamma P_w/P_o)
c        P_nf=P_o exp[P_w/P_o (1-R_m^2/R_T^2)]        !No flow pressure
c      Setting Gamma=2 for simplicity
c----------------------------------------------------------------------
      READ(eq_unit,*,IOSTAT=ios) kvtor,rvtor,nmass
 209  FORMAT(5e16.9)
      IF(ios==0) THEN
         IF(kvtor > 0) THEN
           WRITE(6,*) 'Using rotation profile from eqdsk file'
           ALLOCATE(pressw(0:ma))
           READ(eq_unit,209) (pressw(i),i=0,ma)
           pressw=ABS(pressw)
           READ(eq_unit,209) (eqIn%sq_in%fs(i,4),i=0,ma)         ! Dont need
           smallnum=SQRT(TINY(smallnum))
           DO i=0,ma
              eqIn%sq_in%fs(i,4)=rmaxis/rvtor*
     &                  SQRT(pressw(i)/(smallnum+eqIn%sq_in%fs(i,2)))
              eqIn%sq_in%fs(i,2)=
     &                eqIn%sq_in%fs(i,2)*exp( (1.-(rmaxis/rvtor)**2)*
     &                ABS(pressw(i)/(smallnum+eqIn%sq_in%fs(i,2))))
           ENDDO
           DEALLOCATE(pressw)
         ELSE
              eqIn%sq_in%fs(:,4)=0.
         ENDIF
c----------------------------------------------------------------------
c    Read out ion mass density profile if available                   
c----------------------------------------------------------------------
         IF (nmass > 0) THEN
           ALLOCATE(mass_dens(0:ma))
           READ(eq_unit,209)(mass_dens(i),i=0,ma)
           IF (TRIM(ts%n_profile)=='eqfile') THEN
             WRITE(6,*) 'Using density profile from eqdsk file'
             IF ( ts%ndens/=(mass_dens(0)/ms(2)*ts%zz) )  THEN
              WRITE(6,*) '1 ', mass_dens(0)
              WRITE(6,*) '1 ', ms(2)
              WRITE(6,*) '1 ', ts%zz
              WRITE(6,*) 'Renormalizing density profile from',
     &           mass_dens(0)/ms(2)*ts%zz,' to ndens: ', ts%ndens
             ENDIF
           ELSE
             WRITE(6,*) 'NOT using density profile from eq file'
             WRITE(6,*) '  Eq central value from file: ', 
     &                                       mass_dens(0)/ms(2)*ts%zz
           ENDIF
         ENDIF
      ELSE
         eqIn%sq_in%fs(:,4)=0.
      ENDIF
c----------------------------------------------------------------------
c    Specify density profile.
c    PRE - added option to read in a density file.  Assuming that I'm
c    reading in electron density on same mesh as eqdsk file.
c----------------------------------------------------------------------
      IF(TRIM(ts%n_profile)=='eqfile'.AND.ALLOCATED(mass_dens)) THEN
          eqIn%sq_in%fs(:,5)=mass_dens*(ts%ndens/mass_dens(0))
      ENDIF
      IF (ALLOCATED(mass_dens))  DEALLOCATE(mass_dens)
      IF (ALLOCATED(rho))        DEALLOCATE(rho)
c-----------------------------------------------------------------------
      CLOSE(UNIT=eq_unit)
c-----------------------------------------------------------------------
c     translate to derived type quantities
c-----------------------------------------------------------------------
      eqIn%psi_in%mx=mr
      eqIn%psi_in%my=mz
      eqIn%rmin_in=rgrid
      eqIn%rmax_in=rgrid+xdim
      eqIn%zmin_in=-zdim/2
      eqIn%zmax_in=zdim/2
      eqIn%psio=ssibry1-ssimag1
      eqIn%sq_in%fs(:,1)=ABS(eqIn%sq_in%fs(:,1))
      eqIn%sq_in%fs(:,2)=MAX(eqIn%sq_in%fs(:,2)*mu0,0._r8)  !pressure
      eqIn%sq_in%fs(:,6)=0.   ! electron pressure: Not read in here.
c-----------------------------------------------------------------------
c     copy and convert 2D quantities.
c-----------------------------------------------------------------------
      eqIn%ro=rmaxis
      eqIn%zo=zmaxis
      eqIn%psi_in%fs=ssibry1-eqIn%psi_in%fs
      eqIn%psiin_boundary=ssibry1
c-----------------------------------------------------------------------
c     normalize sign convention.
c-----------------------------------------------------------------------
      IF(eqIn%psio < 0)THEN
         eqIn%psio=-eqIn%psio
         eqIn%psi_in%fs=-eqIn%psi_in%fs
      ENDIF
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_efit
c-----------------------------------------------------------------------
c     subprogram 17. read_tokamac.
c     reads data from Mike Mauel's direct solver.
c-----------------------------------------------------------------------
      SUBROUTINE read_tokamac(io,ts,fileIn,eqIn)
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgInputEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(INOUT) :: fileIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlAux), INTENT(IN) :: ts
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      INTEGER, PARAMETER :: eq_unit = 16  ! equilibrium file
      INTEGER :: ma, mr, mz
      REAL(r8) :: beta_in,betap_in, ro, zo
      REAL(r8) :: rmin_in, rmax_in, zmin_in, zmax_in
      REAL(r8), DIMENSION(:), ALLOCATABLE :: psi,f,p,q
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: psig
c-----------------------------------------------------------------------
c     Do some initialization of the type
c-----------------------------------------------------------------------
      fileIn%eqType="direct"
c-----------------------------------------------------------------------
c     read equilibrium data.
c-----------------------------------------------------------------------
      OPEN(UNIT=eq_unit,FILE=TRIM(fileIn%origFile),STATUS='old')
      READ(eq_unit,'(20(/))')
      READ(eq_unit,*)mr,mz,ma
      ma=ma-1
      CALL spline_alloc(eqIn%sq_in,ma,eqIn%nsq_in)
      ALLOCATE(psi(0:ma),f(0:ma),p(0:ma),q(0:ma),psig(mr,mz))
      READ(eq_unit,*)rmin_in,rmax_in,zmin_in,zmax_in
      READ(eq_unit,*) ro,zo
      READ(eq_unit,*)beta_in
      READ(eq_unit,*)betap_in
      READ(eq_unit,*)psig
      READ(eq_unit,*)psi
      READ(eq_unit,*)f
      READ(eq_unit,*)p
      READ(eq_unit,*)q
      CLOSE(eq_unit)
c-----------------------------------------------------------------------
c     Put data into eqIn derived type
c-----------------------------------------------------------------------
      eqIn%mx=mr-1; eqIn%my=mz-1; eqIn%ms_in=ma
      eqIn%rmin_in=rmin_in
      eqIn%rmax_in=rmax_in
      eqIn%zmin_in=zmin_in
      eqIn%zmax_in=zmax_in
      eqIn%ro=ro;   eqIn%zo=zo
      CALL spline_alloc(eqIn%sq_in,ma,eqIn%nsq_in)
      eqIn%sq_in%fs=0.
      eqIn%psio=psi(ma)-psi(0)
      eqIn%sq_in%xs=(psi-psi(0))/eqIn%psio
      eqIn%sq_in%fs(:,1)=f
      eqIn%sq_in%fs(:,2)=p*mu0
      eqIn%sq_in%fs(:,3)=q
      eqIn%sq_in%fs(:,4)=0.
      eqIn%psio=eqIn%psio/twopi
      CALL bicube_alloc(eqIn%psi_in,mr-1,mz-1,1)
      eqIn%psi_in%fs(1,:,:)=(psi(ma)-psig(:,:))/twopi
      eqIn%psiin_boundary=psi(ma)/twopi
c-----------------------------------------------------------------------
c     process equilibrium.
c-----------------------------------------------------------------------
      DEALLOCATE(psi,f,p,q,psig)
c-----------------------------------------------------------------------
c     Revise profiles if needed and diagnose
c-----------------------------------------------------------------------
      CALL finishEqIn(io,ts,fileIn,eqIn)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE read_tokamac

