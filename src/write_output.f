c-----------------------------------------------------------------------
c     file output.f.
c     Various output files.
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. write_out.
c     2. write_stability.
c     3. write_neoclassical.
c-----------------------------------------------------------------------

c-----------------------------------------------------------------------
c     subprogram 1. write_out.
c     Writes standard ascii and binary output:
c     fluxgrid.txt, fluxgrid.bin, stability.bin, gs2d.bin
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     declarations.
c-----------------------------------------------------------------------
      SUBROUTINE write_out(io,fIn,gIn,mEq,gEV,gEP,sS) !#SIDL_SUB !#SIDL_SUB_NAME=writeOut#
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(fileData),INTENT(IN) :: fIn
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(singularSurfaces), INTENT(IN) :: sS
      TYPE(globalEqDiagnose), INTENT(IN) :: gEV
      TYPE(globalEqProfiles), INTENT(IN) :: gEP
      
      INTEGER(i4) :: ipsi,ising,i
      REAL(r8) :: gsnorm,c0,c1,c2
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: diff
      REAL(r8), PARAMETER :: k_boltzmann=1.3807e-23
      CHARACTER(30) :: format2,foutname
      CHARACTER(128) :: title
      INTEGER, PARAMETER :: out_unit = 2  ! output file unit
      INTEGER, PARAMETER :: iua = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: iub = 42    ! binary diagnostic file
      INTEGER(i4) :: mpsi,mtheta
      mpsi=gIn%mpsi; mtheta=gIn%mtheta
c-----------------------------------------------------------------------
c     write formats.
c     Note I'm the es format requires es(N+7).N b/c 
c      -?.***E+?? gives 7 non* characters (even though most numbers are 
c      positive 7 is better than 6 so that one can catch errors).  
c     If you want spaces use es(N+8).N or es(N+9).N
c-----------------------------------------------------------------------
 10   FORMAT(/3x,"mpsi",1x,"mtheta",2x,"psilow",4x,"psihigh",6x,"tol0",
     &     6x,"ipb",3x,"ipr"//2i6,1p,3es11.3,2i6/)
 20   FORMAT( 4x,"R_m = ",es10.3," m",/4x,"Z_m = ",es10.3," m")
 21   FORMAT( 4x,"R_o = ",es10.3," m",/4x,"a   = ",es10.3," m")
 22   FORMAT( 4x,"A  = R_o/a   = ",es10.3)
 30   FORMAT( 4x,"kappa        = ",es10.3,
     &       /4x,"delta_top    = ",es10.3,/4x,"delta_bottom = ",es10.3)
 40   FORMAT( 4x,"B_o = ",es10.3," T",/4x,"I   = ",es10.3," MA ",
     &       /4x,"I_N = I/aB = ",es10.3,
     &       /4x,"Volume     = ",es10.3," m^3")
 41   FORMAT( 4x,"Total poloidal flux = ",es10.3," W",/
     &        4x,"Total toroidal flux = ",es10.3," W")
 42   FORMAT( 4x,"Total cross-sectional area = ",es10.3," m^2",/
     &        4x,"Total flux surface area = ",es10.3," m^2")
 50   FORMAT( 4x,"q0   = ",es10.3,/4x,"qmin = ",es10.3,/4x,
     &           "qmax = ",es10.3,/4x,"qa   = ",es10.3)
 60   FORMAT( 4x,"li1 = ",es10.3,/4x,"li2 = ",es10.3,/
     &        4x,"li3 = ",es10.3)
 70   FORMAT( 4x,"p_0    = ",es10.3," Pa",/4x,"<p>    = ",es10.3," Pa",
     &       /4x,"betap1 = ",es10.3,/4x,"betap2 = ",es10.3,
     &       /4x,"betap3 = ",es10.3,/4x,"betat  = ",es10.3,
     &       /4x,"betan  = ",es10.3)
 75   FORMAT( 4x,"n_0    = ",es10.3)
 76   FORMAT( 4x,"T_i_0    = ",es10.3," K",4x," = ",es10.3," keV")
 77   FORMAT( 4x,"T_e_0    = ",es10.3," K",4x," = ",es10.3," keV")
 78   FORMAT( 4x,"T_i_a    = ",es10.3," K",4x," = ",es10.3," keV")
 79   FORMAT( 4x,"T_e_a    = ",es10.3," K",4x," = ",es10.3," keV")
 71   FORMAT( 4x,"Z used for quasineutrality: ",es10.3)
 80   FORMAT( 4x,"At Z_m:    R,Z = ",2es11.3,
     &        "      and     R,Z = ",2es11.3)
 81   FORMAT( 4x,"At max. R: R,Z = ",2es11.3,
     &        "   at min. R: R,Z = ",2es11.3)
 82   FORMAT( 4x,"At max. Z: R,Z = ",2es11.3,
     &        "   at min. Z: R,Z = ",2es11.3)
 160  FORMAT( 4x,"V_te    = ",es10.3,"m/s",
     &       /4x,"V_ti    = ",es10.3," m/s")
 161  FORMAT( 4x,"V_A     = ",es10.3," m/s")
 162  FORMAT( 4x,"V_de_max= ",es10.3,"m/s",
     &       /4x,"V_di_max= ",es10.3," m/s")
 170  FORMAT( 4x,"Tau_ce  = ",es10.3," s",
     &        7x,"omega_ce  = ",es10.3," 1/s")
 171  FORMAT( 4x,"Tau_ci  = ",es10.3," s",
     &        7x,"omega_ci  = ",es10.3," 1/s")
 172  FORMAT( 4x,"Tau_pe  = ",es10.3," s",
     &        7x,"omega_pe  = ",es10.3," 1/s")
 173  FORMAT( 4x,"Tau_pi  = ",es10.3," s",
     &        7x,"omega_pi  = ",es10.3," 1/s")
 174  FORMAT( 4x,"Tau_e   = ",es10.3," s",
     &        7x,"nu_e      = ",es10.3," 1/s")
 175  FORMAT( 4x,"Tau_i   = ",es10.3," s",
     &        7x,"nu_i      = ",es10.3," 1/s")
 176  FORMAT( 4x,"Tau_tre = ",es10.3," s",
     &        7x,"omega_tre = ",es10.3," 1/s")
 177  FORMAT( 4x,"Tau_tri = ",es10.3," s",
     &        7x,"omega_tri = ",es10.3," 1/s")
 178  FORMAT( 4x,"Tau_E   = ",es10.3," s")
 180  FORMAT( 4x,"Tau_A   = ",es10.3," s",
     &        7x,"nu_A      = ",es10.3," 1/s")
 181  FORMAT( 4x,"Tau_R   = ",es10.3," s",
     &        7x,"nu_R      = ",es10.3," 1/s")
 182  FORMAT( 4x,"Tau_str = ",es10.3," s",
     &        7x,"omega_str = ",es10.3," 1/s")
 190  FORMAT( 4x,"r_cyce  = ",es10.3," m")
 191  FORMAT( 4x,"r_cyci  = ",es10.3," m")
 192  FORMAT( 4x,"l_de    = ",es10.3," m")
 193  FORMAT( 4x,"l_di    = ",es10.3," m")
 194  FORMAT( 4x,"l_pll   = ",es10.3," m")
 195  FORMAT( 4x,"l_mfpe  = ",es10.3," m")
 196  FORMAT( 4x,"l_mfpi  = ",es10.3," m")
 200  FORMAT(/5x,"i",5x,"m",6x,"q",7x,"dq/dpsi",6x,"psi",8x,"rho"/)
 210  FORMAT(2i6,1p,4es11.3)
 250  FORMAT(/21x,"S = ",es10.3,"   and  ndens = ",es10.3," m^-3")
 251  FORMAT(/21x,"S = ",es10.3,"   and  Tau_A = ",es10.3," s")
 252  FORMAT(/21x,"ndens = ",es10.3," m^-3")
 260  FORMAT( 21x,"elecd = ",es10.3," m^2/s",
     &       /21x,"Tau_A = ",es10.3," s",/21x,"Tau_R = ",es10.3," s")
 261  FORMAT( 21x,"elecd = ",es10.3," m^2/s",
     &       /21x,"ndens = ",es10.3," m^-3",/21x,"Tau_R = ",es10.3," s")
 262  FORMAT( 21x,"elecd = ",es10.3," m^2/s",/21x,"S     = ",es10.3,
     &       /21x,"Tau_A = ",es10.3," s",/21x,"Tau_R = ",es10.3," s")
 301  FORMAT( 4x,"Chi_pll_e (axis) = ",es10.3," m^2/s",
     &        7x,"Chi_pll_e (edge) = ",es10.3," m^2/s")
 309  FORMAT( 4x,"Chi_cross_e (axis) = ",es10.3," m^2/s",
     &        7x,"Chi_cross_e (edge) = ",es10.3," m^2/s")
 302  FORMAT( 4x,"Chi_prp_e (axis) = ",es10.3," m^2/s",
     &        7x,"Chi_prp_e (edge) = ",es10.3," m^2/s")
 303  FORMAT( 4x,"Chi_pll_i (axis) = ",es10.3," m^2/s",
     &        7x,"Chi_pll_i (edge) = ",es10.3," m^2/s")
 304  FORMAT( 4x,"Chi_prp_i (axis) = ",es10.3," m^2/s",
     &        7x,"Chi_prp_i (edge) = ",es10.3," m^2/s")
 305  FORMAT( 4x,"eta_prp (axis) = ",es10.3," m^2/s",
     &        7x,"eta_prp (edge) = ",es10.3," m^2/s")
 306  FORMAT( 4x,"Nu_pll_i (axis) = ",es10.3," m^2/s",
     &        7x,"Nu_pll_i (edge) = ",es10.3," m^2/s")
 307  FORMAT( 4x,"Nu_cross_i (axis) = ",es10.3," m^2/s",
     &        7x,"Nu_cross_i (edge) = ",es10.3," m^2/s")
 308  FORMAT( 4x,"Nu_prp_i (axis) = ",es10.3," m^2/s",
     &        7x,"Nu_prp_i (edge) = ",es10.3," m^2/s")
c-----------------------------------------------------------------------
c     open ascii output file and write input data.
c-----------------------------------------------------------------------
      ALLOCATE(diff(0:mtheta,0:mpsi))
      IF(LEN_TRIM(fIn%runlabel)==0) THEN
        foutname="fluxgrid.txt"
      ELSE
        foutname="fg"//TRIM(fIn%runlabel)//".txt"
      ENDIF
      OPEN(UNIT=iua,FILE=foutname,STATUS='UNKNOWN')
      WRITE(iua,*) "EQ. FILE:  ",TRIM(fIn%origFile)
      WRITE(iua,*) "TYPE:      ",TRIM(fIn%eqType)
      WRITE(iua,*) "FILE TYPE: ",TRIM(fIn%fileType)
c-----------------------------------------------------------------------
c     Print info on global data.
c     NOTE: In future, need to add qcyl, pressure peaking, GA's S, etc.
c-----------------------------------------------------------------------
      WRITE(iua,fmt='(/1x,"GEOMETRIC QUANTITIES:")')
      WRITE(iua,20) mEq%ro,mEq%zo
      WRITE(iua,21) gEV%rmean,gEV%amean
      WRITE(iua,22) gEV%aratio
      WRITE(iua,30) gEV%kappa,gEV%delta1,gEV%delta2
      WRITE(iua,fmt='(/1x,"GLOBAL PLASMA QUANTITIES:")')
      WRITE(iua,40) gEV%bt0,gEV%crnt,
     &              gEV%crnt/(gEV%amean*gEV%bt0),gEV%volume
      WRITE(iua,42) gEV%area_xsct,gEV%area_surf
      WRITE(iua,41) mEq%psio*twopi,gEV%torfluxo*twopi
      WRITE(iua,fmt='(/1x,"SAFETY FACTOR QUANTITIES:")')
      WRITE(iua,50) gEV%q0,gEV%qmin,gEV%qmax,gEV%qa
      WRITE(iua,fmt='(/1x,"CURRENT QUANTITIES:")')
      WRITE(iua,60) gEV%li1,gEV%li2,gEV%li3
      WRITE(iua,fmt='(/1x,"PRESSURE QUANTITIES:")')
      WRITE(iua,70) gEV%p0,gEV%pave,
     &              gEV%betap1,gEV%betap2,gEV%betap3,gEV%betat,gEV%betan
      WRITE(iua,fmt='(/1x,"TEMPERATURE & DENSITY:")')
      WRITE(iua,75) gEV%nd0
                             c1=k_boltzmann/elementary_q*1.e-3
      WRITE(iua,76) gEV%ti0, gEV%ti0*c1
      WRITE(iua,77) gEV%te0, gEV%te0*c1
      WRITE(iua,78) gEP%ti(mpsi)/k_boltzmann,gEP%ti(mpsi)/k_boltzmann*c1
      WRITE(iua,79) gEP%te(mpsi)/k_boltzmann,gEP%te(mpsi)/k_boltzmann*c1
      WRITE(iua,71) mEq%zz
      WRITE(iua,fmt='(/1x,"SEPARATRIX LOCATIONS:")')
      WRITE(iua,80) mEQ%rs1,mEQ%zo, mEQ%rs2,mEQ%zo
      WRITE(iua,81) mEQ%rsn,mEQ%zsn,mEQ%rsx,mEQ%zsx
      WRITE(iua,82) mEQ%rsb,mEQ%zsb,mEQ%rst,mEQ%zst
      WRITE(iua,fmt='(/1x,"VELOCITIES:")')
      WRITE(iua,160) gEV%vt0e, gEV%vt0i
      WRITE(iua,161) gEV%v_a
      WRITE(iua,162) maxval(gEP%vde), maxval(gEP%vdi)
      WRITE(iua,fmt='(/1x,"TIME SCALES:")')
      WRITE(iua,172) 2.*pi/gEV%omega_pe,gEV%omega_pe
      WRITE(iua,170) 2.*pi/gEV%omega_ce,gEV%omega_ce
      WRITE(iua,173) 2.*pi/gEV%omega_pi,gEV%omega_pi
      WRITE(iua,171) 2.*pi/gEV%omega_ci,gEV%omega_ci
      WRITE(iua,180) gEV%taua,1./gEV%taua
      WRITE(iua,176) 2.*pi/gEV%omega_te,gEV%omega_te                ! e- transit
      WRITE(iua,177) 2.*pi/gEV%omega_ti,gEV%omega_ti                ! e- transit
      WRITE(iua,174) 1./gEV%nu_e,gEV%nu_e
      WRITE(iua,175) 1./gEV%nu_i,gEV%nu_i
      WRITE(iua,182) 2.*pi/gEV%omega_star,gEV%omega_star
      WRITE(iua,178) 1./gEV%nu_i*(ms(2)/2./ms(1))
      WRITE(iua,181) gEV%taur,1./gEV%taur
      WRITE(iua,fmt='(/1x,"LENGTH SCALES:")')
      WRITE(iua,192) gEV%le_debye
      WRITE(iua,190) gEV%re_larmor
      WRITE(iua,193) gEV%li_debye
      WRITE(iua,191) gEV%ri_larmor
      WRITE(iua,194) gEV%l_pll
      WRITE(iua,195) gEV%le_mfp
      WRITE(iua,196) gEV%li_mfp

      WRITE(iua,fmt='(/1x,"BRAGINSKII TRANSPORT COEFFICIENTS:")')
         !Note: chi_prp_e/chi_prp_i = 1.65 Z^-2 SQRT(m_e/m_i) (T_i/T_e)^0.5 
         c0=1.65/mEq%zz**2 * SQRT(ms(1)/ms(2))
         c1=c0*(gEV%ti0/gEV%te0)**.5
         c2=c0*(gEP%ti(mpsi)/gEP%te(mpsi))**.5
      WRITE(iua,301) gEP%chi_pll(0),gEP%chi_pll(mpsi)        ! Electron
      WRITE(iua,309) gEP%chi_cross(0),gEP%chi_cross(mpsi)        ! Electron
      WRITE(iua,302) gEP%chi_prp(0)*c1,gEP%chi_prp(mpsi)*c2  ! Electron
         !Note: chi_pll_i/chi_pll_e = 3.3 Z^-4 SQRT(m_e/m_i) (T_i/T_e)^2.5 
         c0=3.3/mEq%zz**4 * SQRT(ms(1)/ms(2))
         c1=c0*(gEP%ti(0)/gEP%te(0))**2.5
         c2=c0*(gEP%ti(mpsi)/gEP%te(mpsi))**2.5
      WRITE(iua,303) gEP%chi_pll(0)*c1,gEP%chi_pll(mpsi)*c2  ! Ion
      WRITE(iua,304) gEP%chi_prp(0),gEP%chi_prp(mpsi)        ! Ion
      WRITE(iua,305) gEP%eta_prp(0)/mu0,gEP%eta_prp(mpsi)/mu0
      WRITE(iua,306) gEP%nu_pll(0),gEP%nu_pll(mpsi) 
      WRITE(iua,307) gEP%nu_cross(0),gEP%nu_cross(mpsi)
      WRITE(iua,308) gEP%nu_prp(0),gEP%nu_prp(mpsi)
      WRITE(iua,fmt='(/1x,"MHD QUANTITIES:")')
      WRITE(iua,fmt='(/2x,"IF YOU WANT:")')
      WRITE(iua,250) io%sfac,gEV%nd0
      WRITE(iua,fmt='(/2x,"THEN USE:")')
      WRITE(iua,260) gEV%taurfac/(io%sfac*gEV%taua),gEV%taua,
     &               io%sfac*gEV%taua
      WRITE(iua,fmt='(/1x,"OR")')
      WRITE(iua,fmt='(/2x,"IF YOU WANT:")')
      WRITE(iua,251) io%sfac,1.e-7
      WRITE(iua,fmt='(/2x,"THEN USE:")')
      WRITE(iua,261) gEV%taurfac/(io%sfac*1.e-7),(1.e-7)**2/gEV%tauafac,
     &               io%sfac*1.e-7
      WRITE(iua,fmt='(/1x,"OR")')
      WRITE(iua,fmt='(/2x,"IF YOU WANT SPITZER RESISTIVITIY USING:")')
      WRITE(iua,252) gEV%nd0
      WRITE(iua,fmt='(/2x,"THEN USE:")')
      WRITE(iua,262) gEP%eta_prp(0)/mu0, 
     $               gEV%taurfac*mu0/gEP%eta_prp(0)/gEV%taua,
     $               gEV%taua, gEV%taurfac*mu0/gEP%eta_prp(0)
c-----------------------------------------------------------------------
c     Print info on singular surfaces.
c-----------------------------------------------------------------------
      WRITE(iua,'(//1x,a,i2,a)')"Singular surfaces for n = ",gIn%nn,":"
      WRITE(iua,200)
      WRITE(iua,210)(ising,sS%msing(ising),sS%qsing(ising),
     $     sS%q1sing(ising),sS%psising(ising),SQRT(sS%psising(ising)),
     $     ising=1,sS%nsing)
      WRITE(iua,200)
c-----------------------------------------------------------------------
c     Write surface quantity information
c-----------------------------------------------------------------------
 310  FORMAT('(i5,1p,',i2.2,'es11.3)')
      WRITE(format2,310)mEq%sq%nqty+1
      WRITE(iua,fmt='(//"SURFACE QUANTITIES")')
      title='i     psi         f          p          q         mach'
      title=TRIM(title)//"       ndens       pe"
      WRITE(iua,fmt='(/a)') "    "//TRIM(title)
      WRITE(iua,*)
      WRITE(iua,format2)
     &  (i,mEq%sq%xs(i),mEq%sq%fs(i,1:mEq%sq%nqty),i=0,mEq%sq%nodes)
      WRITE(iua,fmt='(/a/)') "    "//TRIM(title)
c-----------------------------------------------------------------------
c     Close fluxgrid.txt
c-----------------------------------------------------------------------
      CLOSE(iua)
c-----------------------------------------------------------------------
c     Write useful surface quantities to fluxgrid.bin.
c-----------------------------------------------------------------------
      diff=ABS(gEP%delstr-gEP%gsrhs)                         ! Check GS Eq.
      gsnorm=MAXVAL(gEP%delstr)

      IF(io%bin_1d) THEN
      CALL open_bin(iub,"fluxgrid.bin","UNKNOWN","REWIND",32_i4)
      DO ipsi=0,mEq%sq%nodes
        WRITE(iub)(/REAL(mEq%sq%xs(ipsi),4),
     &            REAL(mEq%sq%fs(ipsi,1:mEq%sq%nqty),4),
     &            REAL(SQRT(mEq%sq%xs(ipsi)),4),
     &            REAL(gEP%lambdaprof(ipsi),4),
     &            REAL(LOG(MAXVAL(diff(:,ipsi)/gsnorm)),4)/)
      ENDDO
      CALL close_bin(iub,"fluxgrid.bin")
      ENDIF
c-----------------------------------------------------------------------
c     Show 2d difference of GS Equation
c-----------------------------------------------------------------------
      IF(io%bin_2d) THEN
      CALL open_bin(iub,"gs2d.bin","UNKNOWN","REWIND",32_i4)
      WRITE(iub) INT(1,4),INT(0,4), INT(2,4)
      WRITE(iub)  INT(mEq%mtheta,4), INT(mEq%mpsi,4)
      WRITE(iub)  REAL(mEq%twod%fs(1,:,:),4)
      WRITE(iub)  REAL(mEq%twod%fs(2,:,:),4)
      WRITE(iub)  REAL(LOG(diff(:,:)),4)
      WRITE(iub)  REAL(LOG(diff(:,:)/gsnorm),4)
      CALL close_bin(iub,"gs2d.bin")
      ENDIF
c-----------------------------------------------------------------------
      DEALLOCATE(diff)
      RETURN
      END SUBROUTINE write_out
c-----------------------------------------------------------------------
c     subprogram 3. write_stability
c     Stability parameters from Glasser, Greene, and Johnson
c      and C.C. Hegna, Phys. Plasmas 6 (1999)
c-----------------------------------------------------------------------
      SUBROUTINE write_stability(sT,mEq)     !#SIDL_SUB !#SIDL_SUB_NAME=writeStability#
      USE fg_local
      USE fg_physdat
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(stabilityType), INTENT(IN) :: sT
      TYPE(mappedEq), INTENT(IN) :: mEq
      INTEGER(i4) :: ipsi
      INTEGER, PARAMETER :: iub = 42    ! binary diagnostic file
c-----------------------------------------------------------------------
       CALL open_bin(iub,"stability.bin","UNKNOWN","REWIND",32_i4)
       DO ipsi=0,mEq%mpsi
        WRITE(iub)(/REAL(SQRT(mEq%sq%xs(ipsi)),4),
     &     REAL(sT%dideal(ipsi),4),
     &     REAL(sT%dres(ipsi),4),
     &     REAL(sT%dnc(ipsi),4),
     &     REAL(sT%dres(ipsi)/(sT%alphas(ipsi)-sT%hfactor(ipsi)),4),
     &     REAL(sT%dres(ipsi)
     &          /(sT%alphas(ipsi)-sT%hfactor(ipsi))+sT%dnc(ipsi),4)/)
       ENDDO
       CALL close_bin(iub,"stability.bin")
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE write_stability
c-----------------------------------------------------------------------
c     subprogram 4. write_neoclassical
c     Write out the data from the neoclassical calculatoin
c-----------------------------------------------------------------------
      SUBROUTINE write_neoclassical(runlabel,TG,gEP,mEq,pC)      !#SIDL_SUB !#SIDL_SUB_NAME=writeNeoclassical#
      USE fg_local
      USE fg_physdat
      USE fcirc_mod
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      USE hdf5_api
      IMPLICIT NONE
      CHARACTER(*), INTENT(IN) :: runlabel
      TYPE(mappedEq), INTENT(IN) :: mEq
      TYPE(globalEqProfiles), INTENT(IN) :: gEP
      TYPE(TransportGeometry), INTENT(IN) :: TG
      TYPE(neoclassical), INTENT(INOUT) :: pC
      REAL(r8) :: psio
      INTEGER, PARAMETER :: iub = 42    ! binary diagnostic file
      INTEGER(i4) :: mpsi,ipsi
      CHARACTER(20) :: meshl="/mappedGrid"
      CHARACTER(40) :: file_name
      CHARACTER(128) :: description
      REAL(r8), DIMENSION(:), ALLOCATABLE :: zeff,zstar,zzi
      REAL(r8), DIMENSION(:), ALLOCATABLE :: nd,f
      REAL(r8), DIMENSION(:), ALLOCATABLE :: dnd,pp,dpp,pe,dpe,ti,dti
      REAL(r8), DIMENSION(:), ALLOCATABLE :: te,dte,bnorm
!#ifdef HAVE_FCIOWRAPPERS
      INTEGER(HID_T) :: rootGid,fid,step_gid,grid_gid
      TYPE(hdf5ErrorType) :: errval
      TYPE(hdf5ErrorType) :: h5err
      TYPE(hdf5InOpts) :: h5in
!#endif
c-----------------------------------------------------------------------
c     Calculate electron and ion temperature  + derivatives
c     This allows a calculation on the fly
c-----------------------------------------------------------------------
      psio=mEq%psio; mpsi=mEq%mpsi
      ALLOCATE(pp(0:mpsi),dpp(0:mpsi),pe(0:mpsi),dpe(0:mpsi))
      ALLOCATE(ti(0:mpsi),dti(0:mpsi),te(0:mpsi),dte(0:mpsi))
      ALLOCATE(dnd(0:mpsi),bnorm(0:mpsi))
      ALLOCATE(zeff(0:mpsi),zzi(0:mpsi),zstar(0:mpsi))
      ALLOCATE(nd(0:mpsi),f(0:mpsi))
      zzi=mEq%zz
      zstar=mEq%sq%fs(:,7)
      zeff=mEq%sq%fs(:,8)
      f =mEq%sq%fs(:,1)
      nd = mEq%sq%fs(:,5);          dnd = mEq%sq%fs1(:,5)/psio
      pe = mEq%sq%fs(:,6)/mu0;      dpe = mEq%sq%fs1(:,6)/mu0/psio
      pp = mEq%sq%fs(:,2)/mu0-pe;   dpp = mEq%sq%fs1(:,2)/mu0/psio
      ti = zstar*pp/nd;              te = pe/nd
      dti= zstar*(dpp-dpe -ti*dnd/zstar)/nd
      dte= (dpe-te*dnd)/nd
      bnorm=f/TG%bigr
c-----------------------------------------------------------------------
!       CALL open_bin(iub,"neoclassical.bin","UNKNOWN","REWIND",32_i4)
!       DO ipsi=0,mEq%mpsi
!        WRITE(iub)(/REAL(SQRT(mEq%sq%xs(ipsi)),4),
!     &     REAL(sT%dideal(ipsi),4),
!     &     REAL(sT%dres(ipsi),4),
!     &     REAL(sT%dnc(ipsi),4),
!     &     REAL(sT%dres(ipsi)/(sT%alphas(ipsi)-sT%hfactor(ipsi)),4),
!     &     REAL(sT%dres(ipsi)
!     &          /(sT%alphas(ipsi)-sT%hfactor(ipsi))+sT%dnc(ipsi),4)/)
!       ENDDO
!       CALL close_bin(iub,"stability.bin")
c-----------------------------------------------------------------------
c     HDF5 file
c-----------------------------------------------------------------------
!#ifdef HAVE_FCIOWRAPPERS
      CALL vshdf5_fcinit()  ! IMPORTANT
      CALL vshdf5_inith5vars(h5in,h5err)  ! IMPORTANT
      h5in%wrd_type=H5T_NATIVE_DOUBLE
      h5in%verbose=.true.
      description="Neoclassical data"
      file_name="fgNeoclassical"//TRIM(runlabel)//".h5"
      CALL open_newh5file(file_name,fid,description,rootGid,h5in,h5err)
      CALL write_attribute(rootGid,"SourceMapper","fluxgrid",h5err)
 
      ! Grid
      CALL make_mesh_group(rootGid,grid_gid,h5in,meshl,"rectilinear"
     &                     ,"r","","","","",h5err)
      call dump_h5(grid_gid,"rho",TG%rho,h5in,h5err)
      h5in%units="m"
      call dump_h5(grid_gid,"r",TG%r,h5in,h5err)
      CALL close_group("GRID",grid_gid,h5err)

      !Data
      h5in%mesh=meshl
      h5in%units="None"
      call dump_h5(rootGid,"bnorm",bnorm,h5in,h5err)
      call dump_h5(rootGid,"fcirc",pC%fcirc,h5in,h5err)
      call dump_h5(rootGid,"nu_star_e",pC%nustare,h5in,h5err)
      call dump_h5(rootGid,"nu_star_i",pC%nustari,h5in,h5err)
      h5in%units="s^-1"
      call dump_h5(rootGid,"omega_tr_e",pC%omegatre,h5in,h5err)
      call dump_h5(rootGid,"omega_tr_i",pC%omegatri,h5in,h5err)
      h5in%units="m^2 s^-1"
      call dump_h5(rootGid,"eta_par_nc_H88",pC%etaparnc(1,:),
     &             h5in,h5err)
      call dump_h5(rootGid,"eta_par_nc_Sauter",pC%etaparnc(2,:),
     &             h5in,h5err)
      call dump_h5(rootGid,"eta_par_nc_Callen",pC%etaparnc(3,:),
     &             h5in,h5err)
      call dump_h5(rootGid,"eta_par_nc_mSauter",pC%etaparnc(4,:),
     &             h5in,h5err)

      call dump_h5(rootGid,"jbs_H88",pC%jbs(1,:),h5in,h5err)
      call dump_h5(rootGid,"jbs_Sauter",pC%jbs(2,:),h5in,h5err)
      call dump_h5(rootGid,"jbs_Callen",pC%jbs(3,:),h5in,h5err)
      call dump_h5(rootGid,"jbs_mSauter",pC%jbs(4,:),h5in,h5err)

      call dump_h5(rootGid,"L31_H88",   pC%L31(1,:),h5in,h5err)
      call dump_h5(rootGid,"L31_Sauter",pC%L31(2,:),h5in,h5err)
      call dump_h5(rootGid,"L31_Callen",pC%L31(3,:),h5in,h5err)
      call dump_h5(rootGid,"L31_mSauter",pC%L31(4,:),h5in,h5err)

      call dump_h5(rootGid,"L32_H88",   pC%L32(1,:),h5in,h5err)
      call dump_h5(rootGid,"L32_Sauter",pC%L32(2,:),h5in,h5err)
      call dump_h5(rootGid,"L32_Callen",pC%L32(3,:),h5in,h5err)
      call dump_h5(rootGid,"L32_mSauter",pC%L32(4,:),h5in,h5err)

      call dump_h5(rootGid,"ki_H88",   pC%ki(1,:),h5in,h5err)
      call dump_h5(rootGid,"ki_Sauter",pC%ki(2,:),h5in,h5err)
      call dump_h5(rootGid,"ki_Callen",pC%ki(3,:),h5in,h5err)
      call dump_h5(rootGid,"ki_mSauter",pC%ki(4,:),h5in,h5err)
 
      call dump_h5(rootGid,"K_matrix",pC%neomat(1,:,:,:),h5in,h5err)
      call dump_h5(rootGid,"M_e",pC%neomat(2,:,:,:),h5in,h5err)

      call dump_h5(rootGid,"RBtoroidal",f,h5in,h5err)
      call dump_h5(rootGid,"Pressure",pp,h5in,h5err)
      call dump_h5(rootGid,"dPdpsi",dpp,h5in,h5err)
      call dump_h5(rootGid,"ElectronPressure",pe,h5in,h5err)
      call dump_h5(rootGid,"dPeDpsi",dpe,h5in,h5err)
      call dump_h5(rootGid,"IonTemperature",ti,h5in,h5err)
      call dump_h5(rootGid,"dTiDpsi",dti,h5in,h5err)
      call dump_h5(rootGid,"ElectronDensity",nd,h5in,h5err)
      call dump_h5(rootGid,"dneDpsi",dnd,h5in,h5err)
      call dump_h5(rootGid,"ElectronTemperature",te,h5in,h5err)
      call dump_h5(rootGid,"dTeDpsi",dte,h5in,h5err)
      call dump_h5(rootGid,"zeff",zeff,h5in,h5err)
      call dump_h5(rootGid,"zstar",zstar,h5in,h5err)
      call dump_h5(rootGid,"zzi",zzi,h5in,h5err)
      CALL close_h5file(fid,rootGid,errval)
!#endif
      DEALLOCATE(pp,dpp,pe,dpe,ti,dti,te,dnd,dte)
      DEALLOCATE(nd,f)
      DEALLOCATE(zeff,zzi,zstar)
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE write_neoclassical
